import { useEffect } from 'react';

import { PAGES } from '../constants';
import { isFilledArray, isFilledString, getPageName } from '../utils';
import ENTITIES from '../domain/model/entities';

function useGetParams(params, pageName) {
  let newParams = {};
  if (isFilledString(pageName) && PAGES[pageName]) {
    const converters = PAGES[pageName].params || {};
    params = params || {};
    Object.keys(converters)
      .forEach((name) => {
        newParams[name] = converters[name] ? converters[name](params[name]) : params[name];
      });
  }
  return newParams;
}

function useModePath(props) {
  const { setModePath } = props;
  const { pathname } = props.location;

  // console.log('useModePath: pathname: ', pathname);

  const pageName = getPageName(pathname);
  const params = useGetParams(props.computedMatch.params, pageName);

  // console.log('useModePath: pageName: ', pageName);
  // console.log('useModePath: params: ', params);

  useEffect(() => {
    if (isFilledString(pageName)) {
      const { entityId } = PAGES[pageName];
      if (entityId) {
        const { modePath } = ENTITIES[entityId];
        const modePathValue = props[modePath];
        // console.log('modePathValue: ', modePathValue);
        if (isFilledArray(modePathValue)) {
          const pathIndex = modePathValue.findIndex(item => item.id === entityId);
          // console.log('pathIndex: ', pathIndex);
          if (pathIndex < 0) {
            const newModePathValue = modePathValue.concat({ id: entityId, activeId: -1, ...params });
            setModePath(modePath, newModePathValue);
          } else {
            let changed = false;
            const paramNames = Object.keys(params);

            for (let i = 0; i < paramNames.length; i++) {
              const name = paramNames[i];
              changed = params[name] !== modePathValue[pathIndex][name];
              if (changed) break;
            }
            if ((pathIndex < modePathValue.length - 1) || changed) {
              let newModePathValue = modePathValue.slice(0, pathIndex + 1);
              newModePathValue[pathIndex] = { ...newModePathValue[pathIndex], ...params };
              setModePath(modePath, newModePathValue);
            }
          }
        }
      }
    }
  });
}

export {
  useGetParams,
  useModePath,
};
