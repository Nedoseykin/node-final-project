import {
  HOME_NEWS,
  HOME_ROOT,
  PERSONAL_ACCOUNT,
  PERSONAL_GALLERY, PERSONAL_NEWS, PERSONAL_ROOT, ROLES, ROOT, USERS,
} from '../domain/model/entityId';

export const PAGES = {
  HOME: {
    title: 'Fast news',
    pathName: '/',
    withLoginButton: true,
    withNavBar: true,
    entityId: HOME_ROOT,
    navLinkLabel: 'Home',
    isNavLink: true,
  },

  HOME_NEWS: {
    title: 'Fast news - news',
    pathName: '/news',
    withLoginButton: true,
    withNavBar: true,
    entityId: HOME_NEWS,
    privilegesList: ['n'],
    params: {
      activeId: v => v ? parseInt(v, 10) : -1,
    },
  },

  PERSONAL: {
    title: 'Fast news - personal',
    pathName: '/personal',
    withLoginButton: true,
    withNavBar: true,
    entityId: PERSONAL_ROOT,
    navLinkLabel: 'Personal',
    isNavLink: true,
    privilegesList: ['a', 'i', 'n'],
  },

  PERSONAL_ACCOUNT: {
    title: 'Fast news - personal account',
    pathName: '/personal/account',
    withLoginButton: true,
    withNavBar: true,
    entityId: PERSONAL_ACCOUNT,
    privilegesList: ['a'],
  },

  PERSONAL_GALLERY: {
    title: 'Fast news - personal gallery',
    pathName: '/personal/gallery',
    withLoginButton: true,
    withNavBar: true,
    entityId: PERSONAL_GALLERY,
    privilegesList: ['i'],
  },

  PERSONAL_NEWS: {
    title: 'Fast news - personal publications',
    pathName: '/personal/publications',
    withLoginButton: true,
    withNavBar: true,
    entityId: PERSONAL_NEWS,
    privilegesList: ['n'],
  },

  ADMIN: {
    title: 'Fast news - admin',
    pathName: '/admin',
    withLoginButton: true,
    withNavBar: true,
    navLinkLabel: 'Admin',
    isNavLink: true,
    entityId: ROOT,
    privilegesList: ['u', 'r'],
  },

  ADMIN_ROLES: {
    title: 'Fast news - roles administration',
    pathName: '/admin/roles',
    withLoginButton: true,
    withNavBar: true,
    entityId: ROLES,
    privilegesList: ['r'],
  },

  ADMIN_USERS: {
    title: 'Fast news - users administration',
    pathName: '/admin/users',
    withLoginButton: true,
    withNavBar: true,
    entityId: USERS,
    privilegesList: ['u'],
  },

  LOGIN: {
    title: 'Fast news - login',
    pathName: '/login',
  },

  REGISTRATION: {
    title: 'Fast news - registration',
    pathName: '/registration',
  },

  DEFAULT: {
    title: 'Fast news - 404',
  }
};
