import { isFilledArray } from './common-utils';

function hasAnyPrivilegesFor(userPrivileges, letterList) {
  if (!isFilledArray(letterList)) {
    return true;
  } else if (!userPrivileges) {
    return false
  }
  const re = new RegExp('[a-z](?=[1-9]+)', 'ig');
  const letters = userPrivileges.match(re);
  if (!isFilledArray(letters)) {
    return false;
  }
  const union = letterList.filter(p => letters.includes(p));
  return isFilledArray(union);
}

export {
  hasAnyPrivilegesFor,
}
