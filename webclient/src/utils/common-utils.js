function isExists(v) {
  return (v !== undefined) && (v !== null);
}

function isFilledString(v) {
  return (typeof v === 'string') && (v.length > 0);
}

function isFilledArray(v) {
  return Array.isArray(v) && (v.length > 0);
}

// returns value of key by filter: { key: { ...filter } }
// returns null if find nothing
function findItemObj(obj, filter) {
  let result = null;
  if (!(obj && filter)) { return result; }
  const keys = Object.keys(obj);
  let item;
  for (let i = 0; i < keys.length; i++) {
    item = obj[keys[i]];
    const filterKeys = Object.keys(filter);
    let test = true;
    for (let j = 0; j < filterKeys.length; j++) {
      const filterKey = filterKeys[j];
      const filterValue = filter[filterKey];
      test = (filterKey in item) && (item[filterKey] === filterValue);
      if (!test) {
        break;
      }
    }
    if (test) {
      result = { ...item };
      break;
    }
  }
  return result;
}

function addCharToLength(
  str,
  length = 0,
  char = '0',
  fromStart = true,
) {
  let chars = '';
  const l = isFilledString(str) ? str.length : 0;
  if (l < length) {
    chars = new Array(length - l)
      .fill(char).join('');
  }
  return fromStart ? `${chars}${str}` : `${str}${chars}`;
}

export {
  isExists,
  isFilledString,
  isFilledArray,
  findItemObj,
  addCharToLength,
};
