import { get } from 'lodash';
import { PAGES } from '../constants';

function getErrorObject(error, operation = 'Networking') {
  const message = get(error, 'message', `${operation}: unknown error`);
  const status = get(error, 'status', 500);
  const dateTime = get(error, 'dateTime', new Date().getTime());
  return { message, status, dateTime };
}

function getPageName(pathname) {
  let pageName = '';
  let i = 0;
  Object.keys(PAGES).forEach((pageId) => {
    const { pathName } = PAGES[pageId];
    if ((pathname.indexOf(pathName) === 0) && (pathName.length > i)) {
      i = pathName.length;
      pageName = pageId;
    }
  });
  return pageName;
}

function getPage(pathname) {
  const pageName = getPageName(pathname);
  return PAGES[pageName];
}

export {
  getErrorObject,
  getPageName,
  getPage,
};
