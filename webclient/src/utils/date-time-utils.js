import { isNumber } from 'lodash';

import { addCharToLength, isExists } from './common-utils';

function isDate(value) {
  const date = new Date(value).getTime();
  return isExists(value) && !isNaN(date);
}

function toDateObjectWithFields(value) {
  let result = null;
  if (isDate(value)) {
    let d = new Date(value);
    const year = d.getFullYear();
    const month = d.getMonth();
    const date = d.getDate();
    result = {
      year,
      month,
      date,
    };
  }
  return result;
}

function toTimeObjectWithFields(value) {
  let result = null;
  if (isDate(value)) {
    let d = new Date(value);
    const hours = d.getHours();
    const minutes = d.getMinutes();
    const seconds = d.getSeconds();
    result = {
      hours,
      minutes,
      seconds,
    };
  }
  return result;
}

function toDDMMYYYY(value, divider = '-', isObj = false) {
  const d = !isObj ? toDateObjectWithFields(value) : value;
  return d
    ? [
        addCharToLength(`${d.date}`, 2),
        addCharToLength(`${d.month + 1}`, 2),
        addCharToLength(`${d.year}`, 4)
      ].join(divider)
    : null;
}

function toUTCDDMMYYYY(value, divider = '-', isObj = false) {
  let d = !isObj ? toDateObjectWithFields(value) : value;
  d = d ? new Date(Date.UTC(d.year, d.month, d.date)) : d;
  return d
    ? [
      addCharToLength(`${d.getUTCDate()}`, 2),
      addCharToLength(`${d.getUTCMonth() + 1}`, 2),
      addCharToLength(`${d.getUTCFullYear()}`, 4)
    ].join(divider)
    : null;
}

function toHHMMSS(value, divider = ':') {
  const d = toTimeObjectWithFields(value);
  return d
    ? [
        addCharToLength(`${d.hours}`, 2),
        addCharToLength(`${d.minutes}`, 2),
        addCharToLength(`${d.seconds}`, 2),
      ].join(divider)
    : null;
}

function toCompatibleTimeStamp(value) {
  let date = new Date(value);
  if (!isNaN(date.getTime())) {
    date = date.toISOString();
    date = date.split(/[TZ]/).filter(Boolean).join(' ');
  } else {
    date = '';
  }
  return date;
}

function convertDateToTimeStamp(date) {
  return date
    ? date.toISOString().split(/[TZ]/).filter(Boolean).join(' ')
    : '';
}

function getLastDateInMonth(month, year = new Date().getFullYear()) {
  return  new Date(year, month + 1, 0).getDate();
}

function getFirstDayFromMonday(month, year = new Date().getFullYear()) {
  let i = new Date(year, month, 1).getDay();
  return i === 0 ? 7 : i;
}

function getLastDayFromMonday(month, year = new Date().getFullYear()) {
  let ld = getLastDateInMonth(month, year);
  const i = new Date(year, month, ld).getDay();
  return i === 0 ? 7 : i;
}

function getMonthNameEng(m) {
  return isNumber(m) && !isNaN(m) && (m >= 0) && (m < 12)
    ? [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
      ][m]
    : '';
}

export {
  isDate,
  toDateObjectWithFields,
  toTimeObjectWithFields,
  toDDMMYYYY,
  toUTCDDMMYYYY,
  toHHMMSS,
  toCompatibleTimeStamp,
  convertDateToTimeStamp,
  getLastDateInMonth,
  getFirstDayFromMonday,
  getLastDayFromMonday,
  getMonthNameEng,
};
