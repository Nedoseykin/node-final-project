export function focusById(id) {
  const elm = document.getElementById(id);
  if (elm) { elm.focus(); }
}
