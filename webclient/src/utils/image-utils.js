function handleOnLoadAndResize(e, maxW = '100%', maxH = '100%') {
  const { currentTarget } = e;
  const { naturalWidth: w, naturalHeight: h } = currentTarget;
  const parent = currentTarget.parentElement;
  const { offsetWidth: pW, offsetHeight: pH } = parent;
  if (w && h && pW && pH) {
    const k = w / h;
    const pK = pW / pH;
    if (k > pK) {
      currentTarget.style.width = maxW;
      currentTarget.style.height = 'auto';
    } else {
      currentTarget.style.width = 'auto';
      currentTarget.style.height = maxH;
    }
  }
}

export {
  handleOnLoadAndResize,
};
