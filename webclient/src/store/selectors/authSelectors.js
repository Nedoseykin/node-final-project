import { get } from 'lodash';

import { createSelector } from 'reselect';

const authTokenSelector = ({ auth }) => auth.authToken || 'someToken';

const isLoggedSelector = ({ auth }) => auth.isLogged || false;

const roleIdSelector = ({ auth }) => auth.roleId || 3;

const privilegesSelector = ({ auth }) => auth.privileges || null;

const authLoginSelector = ({ auth }) => get(auth, 'login', '');

const registerStatusSelector = ({ auth }) => auth.registerStatus || 2;

const isRegisteringSelector = createSelector(
  [registerStatusSelector],
  (registerStatus) => {
    return registerStatus < 2;
  },
);

const loggingStatusSelector = ({ auth }) => auth.loggingStatus || 2;

const isLoggingSelector = createSelector(
  [loggingStatusSelector],
  (loggingStatus) => {
    return loggingStatus < 2;
  },
);

export {
  isLoggingSelector,
  authTokenSelector,
  authLoginSelector,
  isLoggedSelector,
  roleIdSelector,
  privilegesSelector,
  isRegisteringSelector,
};
