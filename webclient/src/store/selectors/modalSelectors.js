import { get } from 'lodash';
import { createSelector } from 'reselect';
import { isExists, isFilledArray, isFilledString, toDDMMYYYY, toHHMMSS } from '../../utils';
import {
  ADD_IMAGE_DIALOG,
  CHANGE_PASSWORD_DIALOG, DELETE_CONFIRMATION_DIALOG, EDIT_CONTENT_DIALOG,
  EDIT_IMAGE_DIALOG,
  PREVIEW_IMAGE_DIALOG,
} from '../../domain/modals/modalsId';
import { errorsSelector, userSelector } from './uiSelectors';

const modalsSelector = ({ modal }) => get(modal, 'modals', []);

const modalIsVisibleSelectorCreator = modalId => createSelector(
  [modalsSelector],
  (modals) => {
    return isFilledArray(modals) && (modals.findIndex(item => item.id === modalId) !== -1);
  },
);

const isVisibleChangePasswordDialogSelector = modalIsVisibleSelectorCreator(
  CHANGE_PASSWORD_DIALOG
);

const isVisibleAddImageDialogSelector = modalIsVisibleSelectorCreator(
  ADD_IMAGE_DIALOG,
);

const isVisibleEditImageDialogSelector = modalIsVisibleSelectorCreator(
  EDIT_IMAGE_DIALOG,
);

const isVisibleEditContentDialogSelector = modalIsVisibleSelectorCreator(
  EDIT_CONTENT_DIALOG
);

const modalValueSelectorCreator = (modalId, key, defaultValue = '') => createSelector(
  [modalsSelector],
  (modals) => {
    const modal = isFilledArray(modals)
      ? modals.find(item => item.id === modalId)
      : null;
    return modal ? modal[key] : defaultValue;
  },
);

const currentPasswordSelector = modalValueSelectorCreator(
  CHANGE_PASSWORD_DIALOG, 'currentPassword',
);

const newPasswordSelector = modalValueSelectorCreator(
  CHANGE_PASSWORD_DIALOG, 'newPassword',
);

const confirmPasswordSelector = modalValueSelectorCreator(
  CHANGE_PASSWORD_DIALOG, 'confirmPassword',
);

const validationChangePasswordSelector = modalValueSelectorCreator(
  CHANGE_PASSWORD_DIALOG, 'validation', {},
);

const imageTitleSelector = modalValueSelectorCreator(
  ADD_IMAGE_DIALOG, 'title',
);

const imageDescriptionSelector = modalValueSelectorCreator(
  ADD_IMAGE_DIALOG, 'description',
);

const imageFileNameSelector = modalValueSelectorCreator(
  ADD_IMAGE_DIALOG, 'fileName',
);

const imageValidationSelector = modalValueSelectorCreator(
  ADD_IMAGE_DIALOG, 'validation', {},
);

const editImageTitleSelector = modalValueSelectorCreator(
  EDIT_IMAGE_DIALOG, 'title', '',
);

const editImageDescriptionSelector = modalValueSelectorCreator(
  EDIT_IMAGE_DIALOG, 'description', '',
);

const editImageSizeSelector = modalValueSelectorCreator(
  EDIT_IMAGE_DIALOG, 'size', 0,
);

const editImageIdSelector = modalValueSelectorCreator(
  EDIT_IMAGE_DIALOG, 'imageId', -1,
);

const editImageNameSelector = modalValueSelectorCreator(
  EDIT_IMAGE_DIALOG, 'curr_name', '',
);

const editImageValidationSelector = modalValueSelectorCreator(
  EDIT_IMAGE_DIALOG, 'validation', {},
);

const editImageNeedThumbSelector = modalValueSelectorCreator(
  EDIT_IMAGE_DIALOG, 'needThumb', false,
);

const userIdPasswordChangeSelector = createSelector(
  [userSelector],
  (user) => {
    return user && isExists(user.id) ? user.id : -1;
  },
);

const userLoginAdminPasswordChangeSelector = createSelector(
  [userSelector],
  (user) => {
    return user && isFilledString(user.login) ? user.login : '';
  },
);

const isVisiblePreviewImageDialogSelector = modalIsVisibleSelectorCreator(
  PREVIEW_IMAGE_DIALOG,
);

const previewImageTitleSelector = modalValueSelectorCreator(
  PREVIEW_IMAGE_DIALOG, 'title', '',
);

const previewImageDescriptionSelector = modalValueSelectorCreator(
  PREVIEW_IMAGE_DIALOG, 'description', '',
);

const previewImageCurrNameSelector = modalValueSelectorCreator(
  PREVIEW_IMAGE_DIALOG, 'curr_name', '',
);

const previewImageSrcSelector = createSelector(
  [previewImageCurrNameSelector],
  (currName) => {
    return isFilledString(currName) ? `/images/${currName}` : '';
  },
);

const isVisibleDeleteConfirmationDialogSelector = modalIsVisibleSelectorCreator(
  DELETE_CONFIRMATION_DIALOG,
);

const deleteConfirmationDialogMessageSelector = modalValueSelectorCreator(
  DELETE_CONFIRMATION_DIALOG, 'message', null,
);

const deleteConfirmationDialogOnDeleteSelector = modalValueSelectorCreator(
  DELETE_CONFIRMATION_DIALOG, 'onDelete', () => {},
);

const isVisibleErrorMessageDialogSelector = createSelector(
  [errorsSelector],
  (errors) => {
    return isFilledArray(errors);
  },
);

const itemErrorMessageDialogSelector = createSelector(
  [errorsSelector],
  (errors) => {
    let item = isFilledArray(errors) ? errors[0] : null;
    const message = get(item, 'message', '');
    const status = get(item, 'status', 500);
    let dateTime = get(item, 'dateTime', '');
    if (dateTime !== '') {
      dateTime = `${toDDMMYYYY(dateTime)} ${toHHMMSS(dateTime)}`;
    }
    return { message, status, dateTime };
  },
);

// edit content
const contentIdEditContentDialogSelector = modalValueSelectorCreator(
  EDIT_CONTENT_DIALOG, 'contentId', -1,
);

const contentSrcEditContentDialogSelector = modalValueSelectorCreator(
  EDIT_CONTENT_DIALOG, 'contentSrc', null,
);

const componentIdEditContentDialogSelector = modalValueSelectorCreator(
  EDIT_CONTENT_DIALOG, 'componentId', '',
);

const propIdEditContentDialogSelector = modalValueSelectorCreator(
  EDIT_CONTENT_DIALOG, 'propId', '',
);

const propValuesEditContentDialogSelector = modalValueSelectorCreator(
  EDIT_CONTENT_DIALOG, 'propValues', {},
);

export {
  isVisibleChangePasswordDialogSelector,
  isVisibleAddImageDialogSelector,
  isVisibleEditImageDialogSelector,
  isVisibleEditContentDialogSelector,
  currentPasswordSelector,
  newPasswordSelector,
  confirmPasswordSelector,
  validationChangePasswordSelector,
  imageTitleSelector,
  imageDescriptionSelector,
  imageFileNameSelector,
  imageValidationSelector,
  editImageTitleSelector,
  editImageDescriptionSelector,
  editImageSizeSelector,
  editImageIdSelector,
  editImageNameSelector,
  editImageValidationSelector,
  editImageNeedThumbSelector,
  userIdPasswordChangeSelector,
  userLoginAdminPasswordChangeSelector,
  isVisiblePreviewImageDialogSelector,
  previewImageTitleSelector,
  previewImageDescriptionSelector,
  previewImageSrcSelector,
  isVisibleDeleteConfirmationDialogSelector,
  deleteConfirmationDialogMessageSelector,
  deleteConfirmationDialogOnDeleteSelector,
  isVisibleErrorMessageDialogSelector,
  itemErrorMessageDialogSelector,
  contentIdEditContentDialogSelector,
  contentSrcEditContentDialogSelector,
  componentIdEditContentDialogSelector,
  propIdEditContentDialogSelector,
  propValuesEditContentDialogSelector,
};
