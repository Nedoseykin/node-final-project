import { createSelector } from 'reselect';
import { get } from 'lodash';

const locationSelector = ({ router }) => router.location;

const pathNameSelector = createSelector(
  [locationSelector],
  (location) => {
    let pathname = get(location, 'pathname', '/');
    if (pathname.length > 1) { pathname = pathname.replace(/\/$/, ''); }
    return pathname;
  },
);

export {
  locationSelector,
  pathNameSelector,
};
