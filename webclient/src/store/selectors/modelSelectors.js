const usersLoadStatusSelector = ({ model }) => model.usersLoadStatus || 0;

const usersSelector = ({ model }) => model.users || null;

const rolesLoadStatusSelector = ({ model }) => model.rolesLoadStatus || 0;

const rolesSelector = ({ model }) => model.roles || null;

const imagesSelector = ({ model }) => model.images || null;

const imagesLoadStatusSelector = ({ model }) => model.imagesLoadStatus || 0;

const accountSelector = ({ model }) => model.account || null;

const accountLoadStatusSelector = ({ model }) => model.accountLoadStatus || 0;

const newsSelector = ({ model }) => model.news || null;

const newsLoadStatusSelector = ({ model }) => model.newsLoadStatus || 0;

export {
  usersLoadStatusSelector,
  usersSelector,
  rolesLoadStatusSelector,
  rolesSelector,
  imagesSelector,
  imagesLoadStatusSelector,
  accountSelector,
  accountLoadStatusSelector,
  newsSelector,
  newsLoadStatusSelector,
};
