import { createSelector } from 'reselect';
import { get } from 'lodash';
import { isExists } from '../../utils';
import PUBLICATIONS from '../../domain/publications/publications';

const getLoadStatus = (dataItem) => {
  if (!isExists(dataItem)) return 0;
  if (dataItem && (dataItem instanceof Promise)) return 1;
  if (dataItem && ('component' in dataItem) && ('props' in dataItem)) return 2;
};

const dataSelector = ({ publications }) => publications.data;

const dataIdSelector = ({ publications }) => publications.dataId;

const dataItemRendererSelector = createSelector(
  [dataSelector, dataIdSelector],
  (data, id) => {
    const dataItem = id > 0 ? data[id] : { component: null, props: {} };
    const loadStatus = getLoadStatus(dataItem);
    const componentId = loadStatus === 2 ? dataItem.component : null;
    const component = get(PUBLICATIONS, `${componentId}.component`, null);
    const props = loadStatus === 2 ? dataItem.props : {};

    return {
      loadStatus,
      component,
      props,
    };
  },
);

export {
  dataSelector,
  dataItemRendererSelector,
};
