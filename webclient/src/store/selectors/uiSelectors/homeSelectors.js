import {
  // uiModelValueSelectorCreator,
  modePathSelectorCreator,
  modeSelectorCreator,
  contentSelectorCreator,
  rootEntitiesSelectorCreator,
  activeEntityItemIdSelectorCreator,
  selectedEntityItemSelectorCreator,
  modeModelSelectorCreator,
  // entityItemSelectorCreator,
} from './selectorCreators';
import { HOME_MODE_PATH } from '../../../domain/model/entityModePathId';
import { HOME_ROOT } from '../../../domain/model/entityId';
import { createSelector } from 'reselect';
import { isExists } from '../../../utils';
import { PAGES } from '../../../constants';
import { newsSelector } from '../modelSelectors';

const homeModePathSelector = modePathSelectorCreator(
  HOME_MODE_PATH, [{ id: HOME_ROOT }]
);

const homeModeSelector = modeSelectorCreator(
  homeModePathSelector, HOME_ROOT
);

const homeContentSelector = contentSelectorCreator(homeModeSelector);

const homeRootEntitiesSelector = rootEntitiesSelectorCreator(HOME_MODE_PATH);

const homeActiveEntityItemIdSelector = activeEntityItemIdSelectorCreator(homeModePathSelector);

const homeActiveNewsSelector = selectedEntityItemSelectorCreator(homeActiveEntityItemIdSelector, newsSelector);

const homePreviousPathNameSelector = createSelector(
  [homeActiveEntityItemIdSelector],
  (newsId) => {
    return isExists(newsId) && (newsId > 0) ? PAGES.HOME_NEWS.pathName : PAGES.HOME.pathName;
  },
);

const homeSearchTextSelector = modeModelSelectorCreator(
  homeModePathSelector, 'searchText', '',
);

// const personalAccountSelector = entityItemSelectorCreator(accountSelector);

// const imagesEditModeSelector = uiModelValueSelectorCreator(
//   'imagesEditMode', false,
// );

export {
  homeModePathSelector,
  homeModeSelector,
  homeContentSelector,
  homeRootEntitiesSelector,
  homeActiveEntityItemIdSelector,
  homeActiveNewsSelector,
  homePreviousPathNameSelector,
  homeSearchTextSelector,

  // personalAccountSelector,
  // imagesEditModeSelector,
};
