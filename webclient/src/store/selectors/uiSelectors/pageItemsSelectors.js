import { createSelector } from 'reselect';
import { pathNameSelector } from '../routerSelectors';
import { hasAnyPrivilegesFor, isFilledArray, getPage } from '../../../utils';
import { PAGES } from '../../../constants';
import { get } from 'lodash';
import { privilegesSelector } from '../authSelectors';

const appTitleSelector = createSelector(
  [pathNameSelector],
  (pathName) => {
    const page = getPage(pathName);
    return page ? page.title : PAGES.DEFAULT.title;
  },
);

const withLoginButtonSelector = createSelector(
  [pathNameSelector],
  (pathName) => {
    const page = getPage(pathName);
    return page ? page.withLoginButton : false;
  },
);

const navBarIsVisibleSelector = createSelector(
  [pathNameSelector],
  (pathName) => {
    const page = getPage(pathName);
    return get(page, 'withNavBar', false);
  },
);

const navBarItemsSelector = createSelector(
  [privilegesSelector],
  (privileges) => {
    const pages = Object.keys(PAGES)
      .filter((key) => {
        const { isNavLink, privilegesList } = PAGES[key];
        return isNavLink && hasAnyPrivilegesFor(privileges, privilegesList);
      })
      .map(key => ({ ...PAGES[key], id: key }));
    return isFilledArray(pages) ? pages : [];
  },
);

export {
  appTitleSelector,
  withLoginButtonSelector,
  navBarIsVisibleSelector,
  navBarItemsSelector,
};