import {
  modePathSelectorCreator,
  modeSelectorCreator,
  contentSelectorCreator,
  rootEntitiesSelectorCreator,
  activeEntityItemIdSelectorCreator,
  selectedEntityItemSelectorCreator,
  entityItemSelectorCreator,
} from './selectorCreators';
import { ADMIN_MODE_PATH } from '../../../domain/model/entityModePathId';
import { ROOT } from '../../../domain/model/entityId';
import { rolesSelector, usersSelector } from '../modelSelectors';

const adminModePathSelector = modePathSelectorCreator(
  ADMIN_MODE_PATH, [{ id: ROOT }]
);

const adminModeSelector = modeSelectorCreator(
  adminModePathSelector, ROOT
);

const adminContentSelector = contentSelectorCreator(adminModeSelector);

const adminRootEntitiesSelector = rootEntitiesSelectorCreator(ADMIN_MODE_PATH);

const adminActiveEntityItemIdSelector = activeEntityItemIdSelectorCreator(adminModePathSelector);

// roles
const selectedRoleSelector = selectedEntityItemSelectorCreator(
  adminActiveEntityItemIdSelector, rolesSelector,
);

const roleSelector = entityItemSelectorCreator(selectedRoleSelector);

// users
const selectedUserSelector = selectedEntityItemSelectorCreator(
  adminActiveEntityItemIdSelector, usersSelector,
);

const userSelector = entityItemSelectorCreator(selectedUserSelector);

export {
  adminModePathSelector,
  adminModeSelector,
  adminContentSelector,
  adminRootEntitiesSelector,
  adminActiveEntityItemIdSelector,

  roleSelector,
  userSelector,
};
