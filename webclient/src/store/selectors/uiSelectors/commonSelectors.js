import { get } from 'lodash';

const editModeSelector = ({ ui }) => get(ui, 'editMode', false);

const recordSelector = ({ ui }) => get(ui, 'record', {});

const errorsSelector = ({ ui }) => get(ui, 'errors', []);

export {
  editModeSelector,
  recordSelector,
  errorsSelector,
};
