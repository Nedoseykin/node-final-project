const loginSelector = ({ ui }) => ui.login || '';

const passwordSelector = ({ ui }) => ui.password || '';

export {
  loginSelector,
  passwordSelector,
};
