import {
  uiModelValueSelectorCreator,
  modePathSelectorCreator,
  modeSelectorCreator,
  contentSelectorCreator,
  rootEntitiesSelectorCreator,
  activeEntityItemIdSelectorCreator,
  selectedEntityItemSelectorCreator,
  entityItemSelectorCreator,
} from './selectorCreators';
import { PERSONAL_MODE_PATH } from '../../../domain/model/entityModePathId';
import { PERSONAL_ROOT } from '../../../domain/model/entityId';
import { accountSelector, newsSelector } from '../modelSelectors';

const personalModePathSelector = modePathSelectorCreator(
  PERSONAL_MODE_PATH, [{ id: PERSONAL_ROOT }]
);

const personalModeSelector = modeSelectorCreator(
  personalModePathSelector, PERSONAL_ROOT
);

const personalContentSelector = contentSelectorCreator(personalModeSelector);

const personalRootEntitiesSelector = rootEntitiesSelectorCreator(PERSONAL_MODE_PATH);

const personalActiveEntityItemIdSelector = activeEntityItemIdSelectorCreator(personalModePathSelector);

const personalAccountSelector = entityItemSelectorCreator(accountSelector);

const imagesEditModeSelector = uiModelValueSelectorCreator(
  'imagesEditMode', false,
);

const selectedPublicationSelector = selectedEntityItemSelectorCreator(
  personalActiveEntityItemIdSelector, newsSelector,
);

const publicationSelector = entityItemSelectorCreator(selectedPublicationSelector);

export {
  personalModePathSelector,
  personalModeSelector,
  personalContentSelector,
  personalRootEntitiesSelector,
  personalActiveEntityItemIdSelector,

  personalAccountSelector,
  imagesEditModeSelector,
  publicationSelector,
};
