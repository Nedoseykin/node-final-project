import { get } from 'lodash';
import { createSelector } from 'reselect';
import { isExists, isFilledArray } from '../../../utils';
import ENTITIES from '../../../domain/model/entities';
import { editModeSelector, recordSelector } from './commonSelectors';

const uiModelValueSelectorCreator = (model, defaultValue) => (
  ({ ui }) => get(ui, model, defaultValue)
);

const modePathSelectorCreator = (modePathName, defaultValue) => ({ ui }) => get(ui, modePathName, defaultValue);

const modeSelectorCreator = (modePathSelector, defaultModePathId) => createSelector(
  [modePathSelector],
  (modePath) => {
    return isFilledArray(modePath)
      ? modePath[modePath.length - 1].id
      : defaultModePathId;
  },
);

const modeModelSelectorCreator = (modePathSelector, model, defaultValue) => createSelector(
  [modePathSelector],
  (modePath) => {
    return isFilledArray(modePath)
      ? modePath[modePath.length - 1][model]
      : defaultValue;
  },
);

const contentSelectorCreator = (modeSelector) => createSelector(
  [modeSelector],
  (mode) => {
    const entity = ENTITIES[mode];
    return entity ? entity.component : null;
  },
);

const rootEntitiesSelectorCreator = modePathName => createSelector(
  [() => ENTITIES],
  (itemsObj) => Object.keys(itemsObj)
      .filter(key => (itemsObj[key].modePath === modePathName) && (itemsObj[key].root !== true))
      .map(key => ({ ...itemsObj[key], id: key })),
);

const activeEntityItemIdSelectorCreator = modePathSelector => createSelector(
  [modePathSelector],
  (modePath) => {
    const { activeId } = modePath[modePath.length - 1];

    return isExists(activeId) ? activeId : -1;
  },
);

const selectedEntityItemSelectorCreator = (
  activeEntityItemIdSelector,
  entityDataSelector,
) => createSelector(
  [activeEntityItemIdSelector, entityDataSelector],
  (activeEntityItemId, entityData) => {
    return isFilledArray(entityData)
      ? entityData.find(item => item.id === activeEntityItemId)
      : null;
  },
);

const entityItemSelectorCreator = (selectedEntityItemSelector) => createSelector(
  [editModeSelector, selectedEntityItemSelector, recordSelector],
  (editMode, selectedEntityItem, record) => {
    return editMode ? record : selectedEntityItem;
  },
);

export {
  uiModelValueSelectorCreator,
  modePathSelectorCreator,
  modeSelectorCreator,
  contentSelectorCreator,
  rootEntitiesSelectorCreator,
  activeEntityItemIdSelectorCreator,
  selectedEntityItemSelectorCreator,
  entityItemSelectorCreator,
  modeModelSelectorCreator,
};
