import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import auth from './authReducer';
import ui from './uiReducer';
import model from './modelReducer';
import modal from './modalReducer';
import publications from './publicationsReducer';

export default (history) => combineReducers({
  router: connectRouter(history),
  auth,
  ui,
  model,
  modal,
  publications,
});
