import { handleActions } from 'redux-actions';
import {
  acLogInStart,
  acLogInSuccess,
  acLogInError,
  acLogOut,
  acRegisterStart,
  acRegisterSuccess,
  acRegisterError,
  acResetApp,
} from '../actions';
import { isExists } from '../../utils';

const getInitialState = () => {
  let roleId = parseInt(sessionStorage.getItem('roleId'), 10);

  return {
    isLogged: sessionStorage.getItem('isLogged') === 'true',
    authToken: sessionStorage.getItem('authToken'),
    login: sessionStorage.getItem('login'),
    roleId: !isExists(roleId) || isNaN(roleId) ? -1 : roleId,
    privileges: sessionStorage.getItem('privileges'),
    loggingStatus: 2,
    registerStatus: 2,
  };
};

export default handleActions({
  [acRegisterStart]: (state) => ({
    ...state,
    registerStatus: 1,
  }),

  [acRegisterSuccess]: (state, { payload }) => {
    const { authToken, roleId, login, privileges } = payload;
    sessionStorage.setItem('isLogged', true);
    sessionStorage.setItem('authToken', authToken);
    sessionStorage.setItem('roleId', `${roleId}`);
    sessionStorage.setItem('login', `${login}`);
    sessionStorage.setItem('privileges', `${privileges}`);
    return {
      ...state,
      isLogged: true,
      registerStatus: 2,
      authToken,
      roleId,
      privileges,
      errorMessage: '',
    };
  },

  [acRegisterError]: (state, { payload }) => {
    sessionStorage.removeItem('isLogged');
    sessionStorage.removeItem('authToken');
    sessionStorage.removeItem('roleId');
    sessionStorage.removeItem('login');
    sessionStorage.removeItem('privileges');
    return {
      ...state,
      registerStatus: 3,
      authToken: null,
      roleId: -1,
      isLogged: false,
      privileges: null,
      errorMessage: payload.message,
    };
  },

  [acLogInStart]: (state) => ({
    ...state,
    loggingStatus: 1,
  }),

  [acLogInSuccess]: (state, { payload }) => {
    const { authToken, roleId, login, privileges } = payload;
    sessionStorage.setItem('isLogged', true);
    sessionStorage.setItem('authToken', authToken);
    sessionStorage.setItem('roleId', `${roleId}`);
    sessionStorage.setItem('login', `${login}`);
    sessionStorage.setItem('privileges', `${privileges}`);
    return {
      ...state,
      isLogged: true,
      loggingStatus: 2,
      authToken,
      roleId,
      login,
      privileges,
      errorMessage: '',
    };
  },

  [acLogInError]: (state, { payload }) => {
    sessionStorage.removeItem('isLogged');
    sessionStorage.removeItem('authToken');
    sessionStorage.removeItem('roleId');
    sessionStorage.removeItem('login');
    sessionStorage.removeItem('privileges');
    return {
      ...state,
      loggingStatus: 3,
      isLogged: false,
      authToken: null,
      roleId: -1,
      privileges: null,
      errorMessage: payload.message,
    };
  },

  [acLogOut]: (state) => {
    sessionStorage.removeItem('isLogged');
    sessionStorage.removeItem('authToken');
    sessionStorage.removeItem('roleId');
    sessionStorage.removeItem('login');
    sessionStorage.removeItem('privileges');
    return {
      ...state,
      loggingStatus: 2,
      isLogged: false,
      authToken: null,
      login: '',
      roleId: -1,
      privileges: null,
    };
  },

  [acResetApp]: () => ({
    ...getInitialState(),
  }),
}, getInitialState());
