import { handleActions } from 'redux-actions';
import {
  acErrorLoading,
  acResetApp,
  acSetLoadedData,
  acShouldBeLoaded,
  acStartLoading,
  acUpdateDataWithItem,
  acInsertNewItemToData,
  acRemoveItemFromData,
  acLogOut,
} from '../actions';
import ENTITIES from '../../domain/model/entities';
import { isFilledArray } from '../../utils';

const initialState = {};

export default handleActions({
  [acShouldBeLoaded]: (state, { payload }) => ({
    ...state,
    [ENTITIES[payload.entityName].loadStatus]: 0,
  }),

  [acStartLoading]: (state, { payload }) => ({
    ...state,
    [ENTITIES[payload.entityName].loadStatus]: 1,
  }),

  [acSetLoadedData]: (state, { payload }) => ({
    ...state,
    [ENTITIES[payload.entityName].loadStatus]: 2,
    [ENTITIES[payload.entityName].data]: payload.loadedData,
  }),

  [acErrorLoading]: (state, { payload }) => ({
    ...state,
    [ENTITIES[payload.entityName].loadStatus]: 3,
    [ENTITIES[payload.entityName].data]: null,
  }),

  [acUpdateDataWithItem]: (state, { payload }) => {
    const { entityName, item } = payload;
    let entity = state[ENTITIES[entityName].data];
    if (isFilledArray(entity)) {
      entity = entity.map((entityItem) => {
        return entityItem.id === item.id
          ? { ...entityItem, ...item }
          : { ...entityItem };
      });
    }

    return {
      ...state,
      [ENTITIES[entityName].data]: entity,
    };
  },

  [acInsertNewItemToData]: (state, { payload }) => {
    const { entityName, item } = payload;
    let entity = state[ENTITIES[entityName].data];
    const { insertToData } = ENTITIES[entityName];
    if (insertToData) {
      entity = insertToData(entity, item);
    } else {
      if (!Array.isArray(entity)) {
        entity = [];
      } else {
        entity = entity.slice();
      }
      entity.push(item);
    }
    return {
      ...state,
      [ENTITIES[entityName].data]: entity,
    }
  },

  [acRemoveItemFromData]: (state, { payload }) => {
    const { entityName, id } = payload;
    const entity = ENTITIES[entityName];
    let { [entity.data]: data } = state;
    data = data.filter(item => item.id !== id);
    return {
      ...state,
      [entity.data]: data,
    };
  },

  [acLogOut]: () => ({
    ...initialState,
  }),

  [acResetApp]: () => ({
    ...initialState,
  }),
}, initialState);
