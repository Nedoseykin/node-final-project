import { handleActions } from 'redux-actions';
import { acAddContentItem, acResetApp, acSetContentItem } from '../actions';

const initialState = {
  data: {},
};

export default handleActions({
  [acAddContentItem]: (state, { payload }) => {
    const data = { ...state.data, [payload.id]: null, };
    return {
      ...state,
      data,
    };
  },

  [acSetContentItem]: (state, { payload }) => ({
    ...state,
    data: {
      ...state.data,
      [payload.id]: payload.value,
    },
  }),

  [acResetApp]: () => ({
    ...initialState,
  }),
}, initialState);
