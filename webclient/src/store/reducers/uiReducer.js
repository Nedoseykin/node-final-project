import { handleActions } from 'redux-actions';
import {
  acChangeValue,
  acChangeRecordValue,
  acLogInSuccess,
  acLogInError,
  acLogOut,
  acNextAdminMode,
  acPrevAdminMode,
  acResetApp,
  acSetEntityItemActiveId,
  acStartEditRecord,
  acCancelEditRecord,
  acNextPersonalMode,
  acPrevPersonalMode,
  acNextHomeMode,
  acPrevHomeMode,
  acSetModePath,
  acAddError,
  acShiftErrors, acChangeModeModelValue,
} from '../actions';
import { HOME_ROOT, PERSONAL_ROOT, ROOT } from '../../domain/model/entityId';
import ENTITIES from '../../domain/model/entities';
import { isFilledArray } from '../../utils';

const initialState = {
  adminModePath: [{ id: ROOT }],
  personalModePath: [{ id: PERSONAL_ROOT }],
  homeModePath: [{ id: HOME_ROOT }],
  editMode: false,
  record: {},
  errors: [],
};

export default handleActions({
  [acNextAdminMode]: (state, { payload }) => {
    const { adminMode } = payload;
    const adminModePath = state.adminModePath
      .filter(item => item.id !== adminMode);
    adminModePath.push({ id: adminMode, activeId: -1 });

    return {
      ...state,
      adminModePath,
      editMode: false,
    };
  },

  [acPrevAdminMode]: (state) => {
    let { adminModePath } = state;
    if (isFilledArray(adminModePath) && adminModePath.length > 1) {
      adminModePath = adminModePath.slice(0, state.adminModePath.length - 1);
    }
    return {
      ...state,
      adminModePath,
      editMode: false,
    };
  },

  [acSetEntityItemActiveId]: (state, { payload }) => {
    const { entityName, activeId } = payload;
    const { modePath } = ENTITIES[entityName];
    const modePathValue = state[modePath]
      .map((modeItem) => {
        return modeItem.id === entityName
          ? { ...modeItem, activeId }
          : { ...modeItem };
      });
    return {
      ...state,
      [modePath]: modePathValue,
    }
  },

  [acChangeValue]: (state, { payload }) => ({
    ...state,
    [payload.name]: payload.value,
  }),

  [acChangeRecordValue]: (state, { payload }) => ({
    ...state,
    record: {
      ...state.record,
      [payload.name]: payload.value,
    },
  }),

  [acChangeModeModelValue]: (state, { payload }) => {
    const { entityId, name, value } = payload;
    const { modePath } = ENTITIES[entityId];
    let { [modePath]: data = [] } = state;
    data = data.map((mode) => {
      return mode.id === entityId
        ? { ...mode, [name]: value }
        : mode;
    });
    return {
      ...state,
      [modePath]: data,
    };
  },

  [acLogInSuccess]: (state) => ({
    ...state,
    password: '',
  }),

  [acLogInError]: (state) => ({
    ...state,
    password: '',
  }),

  [acLogOut]: (state) => ({
    ...initialState,
    login: state.login,
    errors: state.errors,
  }),

  [acStartEditRecord]: (state, { payload }) => ({
    ...state,
    editMode: true,
    record: {
      ...payload.record,
    }
  }),

  [acCancelEditRecord]: (state) => ({
    ...state,
    editMode: false,
    record: {},
  }),

  [acNextPersonalMode]: (state, { payload }) => {
    const { personalMode } = payload;
    const personalModePath = state.personalModePath
      .filter(item => item.id !== personalMode);
    personalModePath.push({ id: personalMode, activeId: -1 });

    return {
      ...state,
      personalModePath,
      imagesEditMode: false,
      editMode: false,
    };
  },

  [acPrevPersonalMode]: (state) => {
    let { personalModePath } = state;
    if (isFilledArray(personalModePath) && personalModePath.length > 1) {
      personalModePath = personalModePath.slice(0, personalModePath.length - 1);
    }
    return {
      ...state,
      personalModePath,
      imagesEditMode: false,
      editMode: false,
    };
  },

  [acNextHomeMode]: (state, { payload }) => {
    const { homeMode } = payload;
    const homeModePath = state.homeModePath
      .filter(item => item.id !== homeMode);
    homeModePath.push({ id: homeMode, activeId: -1 });

    return {
      ...state,
      homeModePath,
      editMode: false,
    };
  },

  [acPrevHomeMode]: (state) => {
    let { homeModePath } = state;
    if (isFilledArray(homeModePath) && homeModePath.length > 1) {
      homeModePath = homeModePath.slice(0, homeModePath.length - 1);
    }
    return {
      ...state,
      homeModePath,
      editMode: false,
    };
  },

  [acSetModePath]: (state, { payload }) => ({
    ...state,
    [payload.modePath]: payload.modePathValue,
    editMode: false,
  }),

  [acAddError]: (state, { payload }) => {
    return {
      ...state,
      errors: [...state.errors, { ...payload }],
    };
  },

  [acShiftErrors]: (state) => {
    state.errors.shift();
    return {
      ...state,
      errors: state.errors.slice(),
    };
  },

  [acResetApp]: () => ({
    ...initialState,
    // errors: state.errors,
  }),
}, initialState);
