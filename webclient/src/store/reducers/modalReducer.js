import { handleActions } from 'redux-actions';
import {
  acHideDialog,
  acLogOut,
  acModalChangeValue,
  acModalClearAllValues, acModalSetState,
  acResetApp,
  acShowDialog,
} from '../actions';

const initialState = {
  modals: [],
};

export default handleActions({
  [acShowDialog]: (state, { payload }) => {
    const { modalId, ...rest } = payload;
    const modals = state.modals.slice();
    if (modals.findIndex(item => item.id === modalId) === -1) {
      modals.push({ id: modalId, ...rest });
    }
    return {
      ...state,
      modals,
    }
  },

  [acHideDialog]: (state, { payload }) => {
    const { modalId } = payload;
    const modals = state.modals.filter(item => item.id !== modalId);
    return {
      ...state,
      modals,
    };
  },

  [acModalChangeValue]: (state, { payload }) => {
    const { name, value, modalId } = payload;
    const modals = state.modals.map((item) => {
      return item.id === modalId
        ? { ...item, [name]: value } : { ...item };
    });
    return {
      ...state,
      modals,
    };
  },

  [acModalSetState]: (state, { payload }) => {
    const { modalId, ...rest } = payload;
    const modals = state.modals.map((item) => {
      return item.id === modalId
        ? { ...item, ...rest }
        : { item };
    });
    return {
      ...state,
      modals,
    };
  },

  [acModalClearAllValues]: (state, { payload }) => {
    const { modalId } = payload;
    const modals = state.modals.map((item) => {
      return item.id === modalId
        ? { id: modalId }
        : { ...item };
    });
    return {
      ...state,
      modals,
    }
  },

  [acLogOut]: () => ({
    ...initialState,
  }),

  [acResetApp]: () => ({
    ...initialState,
  }),
}, initialState);
