import { createStore, applyMiddleware } from 'redux';

import { createBrowserHistory } from 'history';

import { routerMiddleware } from 'connected-react-router';

import { composeWithDevTools } from 'redux-devtools-extension';

import thunk from 'redux-thunk';

import createRootReducer from '../reducers';

export const history = createBrowserHistory();

const middleware = [thunk, routerMiddleware(history)];

export default (preloadedState = {}) => createStore(
  createRootReducer(history),
  preloadedState,
  composeWithDevTools(
    applyMiddleware(...middleware),
  ),
);
