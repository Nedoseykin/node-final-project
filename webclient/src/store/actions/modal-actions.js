import { createAction } from 'redux-actions';

import {
  AC_HIDE_DIALOG,
  AC_MODAL_CHANGE_VALUE,
  AC_MODAL_SET_STATE,
  AC_MODAL_CLEAR_ALL_VALUES,
  AC_SHOW_DIALOG,
} from './actionTypes';

export const acModalChangeValue = createAction(AC_MODAL_CHANGE_VALUE);

export const acModalSetState = createAction(AC_MODAL_SET_STATE);

export const acModalClearAllValues = createAction(AC_MODAL_CLEAR_ALL_VALUES);

export const acShowDialog = createAction(AC_SHOW_DIALOG);

export const acHideDialog = createAction(AC_HIDE_DIALOG);
