import { createAction } from 'redux-actions';
import { AC_RESET_APP } from './actionTypes';

export const acResetApp = createAction(AC_RESET_APP);
