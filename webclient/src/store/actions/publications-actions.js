import { createAction } from 'redux-actions';
import { AC_ADD_CONTENT_ITEM, AC_SET_CONTENT_ITEM } from './actionTypes';

export const acAddContentItem = createAction(AC_ADD_CONTENT_ITEM);

export const acSetContentItem = createAction(AC_SET_CONTENT_ITEM);
