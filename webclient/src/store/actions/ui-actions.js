import { createAction } from 'redux-actions';
import {
  AC_CHANGE_VALUE,
  AC_NEXT_ADMIN_MODE,
  AC_PREV_ADMIN_MODE,
  AC_CHANGE_RECORD_VALUE,
  AC_CHANGE_MODE_MODEL_VALUE,
  AC_START_EDIT_RECORD,
  AC_CANCEL_EDIT_RECORD,
  AC_SET_ENTITY_ITEM_ACTIVE_ID,
  AC_NEXT_PERSONAL_MODE,
  AC_PREV_PERSONAL_MODE,
  AC_NEXT_HOME_MODE,
  AC_PREV_HOME_MODE,
  AC_SET_MODE_PATH,
  AC_ADD_ERROR,
  AC_SHIFT_ERRORS,
} from './actionTypes';

export const acNextAdminMode = createAction(AC_NEXT_ADMIN_MODE);

export const acPrevAdminMode = createAction(AC_PREV_ADMIN_MODE);

export const acSetEntityItemActiveId = createAction(AC_SET_ENTITY_ITEM_ACTIVE_ID);

export const acChangeValue = createAction(AC_CHANGE_VALUE);

export const acChangeRecordValue = createAction(AC_CHANGE_RECORD_VALUE);

export const acChangeModeModelValue = createAction(AC_CHANGE_MODE_MODEL_VALUE);

export const acStartEditRecord = createAction(AC_START_EDIT_RECORD);

export const acCancelEditRecord = createAction(AC_CANCEL_EDIT_RECORD);

export const acNextPersonalMode = createAction(AC_NEXT_PERSONAL_MODE);

export const acPrevPersonalMode = createAction(AC_PREV_PERSONAL_MODE);

export const acNextHomeMode = createAction(AC_NEXT_HOME_MODE);

export const acPrevHomeMode = createAction(AC_PREV_HOME_MODE);

export const acSetModePath = createAction(AC_SET_MODE_PATH);

export const acAddError = createAction(AC_ADD_ERROR);

export const acShiftErrors = createAction(AC_SHIFT_ERRORS);
