import { createAction } from 'redux-actions';

import {
  AC_LOG_IN_START,
  AC_LOG_IN_SUCCESS,
  AC_LOG_IN_ERROR,
  AC_LOG_OUT,
  AC_REGISTER_START,
  AC_REGISTER_SUCCESS,
  AC_REGISTER_ERROR,
} from './actionTypes';

export const acLogInStart = createAction(AC_LOG_IN_START);

export const acLogInSuccess = createAction(AC_LOG_IN_SUCCESS);

export const acLogInError = createAction(AC_LOG_IN_ERROR);

export const acLogOut = createAction(AC_LOG_OUT);

export const acRegisterStart = createAction(AC_REGISTER_START);

export const acRegisterSuccess = createAction(AC_REGISTER_SUCCESS);

export const acRegisterError = createAction(AC_REGISTER_ERROR);
