import { createAction } from 'redux-actions';
import {
  AC_ERROR_LOADING, AC_INSERT_NEW_ITEM_TO_DATA, AC_REMOVE_ITEM_FROM_DATA,
  AC_SET_LOADED_DATA,
  AC_SHOULD_BE_LOADED,
  AC_START_LOADING,
  AC_UPDATE_DATA_WITH_ITEM,
} from './actionTypes';

export const acShouldBeLoaded = createAction(AC_SHOULD_BE_LOADED);

export const acStartLoading = createAction(AC_START_LOADING);

export const acSetLoadedData = createAction(AC_SET_LOADED_DATA);

export const acErrorLoading = createAction(AC_ERROR_LOADING);

export const acUpdateDataWithItem = createAction(AC_UPDATE_DATA_WITH_ITEM);

export const acInsertNewItemToData = createAction(AC_INSERT_NEW_ITEM_TO_DATA);

export const acRemoveItemFromData = createAction(AC_REMOVE_ITEM_FROM_DATA);
