import { push } from 'connected-react-router';
import {
  acCancelEditRecord,
  acChangeRecordValue,
  acHideDialog,
  acSetEntityItemActiveId,
  acShiftErrors,
  acShouldBeLoaded,
  acShowDialog,
  acStartEditRecord,
} from '../actions';
import { isExists } from '../../utils';
import { createEntity, updateEntity } from '../../network';
import { PAGES } from '../../constants';

import {
  CHANGE_PASSWORD_DIALOG,
  ADD_IMAGE_DIALOG,
  EDIT_IMAGE_DIALOG,
  PREVIEW_IMAGE_DIALOG,
  DELETE_CONFIRMATION_DIALOG, EDIT_CONTENT_DIALOG,
} from '../../domain/modals/modalsId';

const reloadEntity = entityName => (dispatch) => {
  dispatch(acShouldBeLoaded({ entityName }));
};

const closeSection = () => (dispatch) => {
  dispatch(push(PAGES.ADMIN.pathName));
};

const closePersonalSection = () => (dispatch) => {
  dispatch(push(PAGES.PERSONAL.pathName));
};

const closeHomeSection = (previousPathName) => (dispatch) => {
  dispatch(push(previousPathName));
};

const entityItemSelect = (entityName, activeId) => (dispatch) => {
  dispatch(acSetEntityItemActiveId({ entityName, activeId }));
};

const newRecord = () => (dispatch) => {
  dispatch(acStartEditRecord({ record: {} }));
};

const editRecord = record => (dispatch) => {
  dispatch(acStartEditRecord({ record }));
};

const cancelEditRecord = () => (dispatch) => {
  dispatch(acCancelEditRecord());
};

const saveRecord = (token, entityName, record) => (dispatch) => {
  if (isExists(record.id)) {
    updateEntity(
      entityName,
      token,
      record,
      () => { dispatch(acCancelEditRecord()) },
    )(dispatch);
  } else {
    createEntity(
      entityName,
      token,
      record,
      () => { dispatch(acCancelEditRecord()) }
    )(dispatch);
  }
};

const onRecordChange = e => (dispatch) => {
  const { name, value } = e.target;
  dispatch(acChangeRecordValue({ name, value }));
};

const showChangePasswordDialog = () => (dispatch) => {
  dispatch(acShowDialog({ modalId: CHANGE_PASSWORD_DIALOG }));
};

const showAddImageDialog = () => (dispatch) => {
  dispatch(acShowDialog({ modalId: ADD_IMAGE_DIALOG }));
};

const showEditContentDialog = (contentId) => (dispatch) => {
  dispatch(acShowDialog({ modalId: EDIT_CONTENT_DIALOG, contentId }));
};

const hideAddImageDialog = () => (dispatch) => {
  dispatch(acHideDialog({ modalId: ADD_IMAGE_DIALOG }))
};

const showEditImageDialog = (image = {}) => (dispatch) => {
  dispatch(acShowDialog({ modalId: EDIT_IMAGE_DIALOG, ...image }));
};

const hideEditImageDialog = () => (dispatch) => {
  dispatch(acHideDialog({ modalId: EDIT_IMAGE_DIALOG }))
};

const showPreviewImageDialog = (image = {}) => (dispatch) => {
  dispatch(acShowDialog({ modalId: PREVIEW_IMAGE_DIALOG, ...image }));
};

const hidePreviewImageDialog = () => (dispatch) => {
  dispatch(acHideDialog({ modalId: PREVIEW_IMAGE_DIALOG }))
};

const showDeleteConfirmationDialog = (payload = {}) => (dispatch) => {
  dispatch(acShowDialog({ modalId: DELETE_CONFIRMATION_DIALOG, ...payload }));
};

const hideDeleteConfirmationDialog = () => (dispatch) => {
  dispatch(acHideDialog({ modalId: DELETE_CONFIRMATION_DIALOG }))
};

const shiftErrors = () => (dispatch) => {
  dispatch(acShiftErrors());
};

export {
  reloadEntity,
  closeSection,
  closePersonalSection,
  closeHomeSection,
  entityItemSelect,
  newRecord,
  editRecord,
  cancelEditRecord,
  saveRecord,
  onRecordChange,
  showChangePasswordDialog,
  showAddImageDialog,
  showEditContentDialog,
  hideAddImageDialog,
  showEditImageDialog,
  hideEditImageDialog,
  showPreviewImageDialog,
  hidePreviewImageDialog,
  showDeleteConfirmationDialog,
  hideDeleteConfirmationDialog,
  shiftErrors,
};
