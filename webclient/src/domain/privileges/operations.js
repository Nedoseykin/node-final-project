const OPERATIONS = {
  READ: {
    prefix: 'read',
    index: 0,
  },
  CREATE: {
    prefix: 'create',
    index: 1,
  },
  UPDATE: {
    prefix: 'update',
    index: 2,
  },
  DELETE: {
    prefix: 'delete',
    index: 3,
  },
  CHANGE_PASSWORD: {
    prefix: 'change password',
    index: 4,
  },
  USER_UNLOCK: {
    prefix: 'unlock',
    index: 5,
  },
  ACTIVATE: {
    prefix: 'activate',
    index: 6,
  },
};

export default OPERATIONS;
