import PRIVILEGES_ID from './privilegesId';
import OPERATIONS_ID from './operations';

const PRIVILEGES = [
  {
    id: PRIVILEGES_ID.USERS,
    label: 'Users',
    letter: 'u',
    items: [
      { ...OPERATIONS_ID.READ },
      { ...OPERATIONS_ID.CREATE },
      { ...OPERATIONS_ID.UPDATE },
      { ...OPERATIONS_ID.DELETE },
      { ...OPERATIONS_ID.CHANGE_PASSWORD },
      { ...OPERATIONS_ID.USER_UNLOCK },
    ],
  },
  {
    id: PRIVILEGES_ID.ROLES,
    label: 'Roles',
    letter: 'r',
    items: [
      { ...OPERATIONS_ID.READ },
      { ...OPERATIONS_ID.CREATE },
      { ...OPERATIONS_ID.UPDATE },
      { ...OPERATIONS_ID.DELETE },
    ],
  },
  {
    id: PRIVILEGES_ID.IMAGES,
    label: 'Images',
    letter: 'i',
    items: [
      { ...OPERATIONS_ID.READ },
      { ...OPERATIONS_ID.CREATE },
      { ...OPERATIONS_ID.UPDATE },
      { ...OPERATIONS_ID.DELETE },
    ],
  },
  {
    id: PRIVILEGES_ID.NEWS,
    label: 'News',
    letter: 'n',
    items: [
      { ...OPERATIONS_ID.READ },
      { ...OPERATIONS_ID.CREATE },
      { ...OPERATIONS_ID.UPDATE },
      { ...OPERATIONS_ID.DELETE },
    ],
  },
  {
    id: PRIVILEGES_ID.ACCOUNT,
    label: 'Account',
    letter: 'a',
    items: [
      { ...OPERATIONS_ID.READ },
      { ...OPERATIONS_ID.UPDATE },
      { ...OPERATIONS_ID.ACTIVATE },
    ]
  }
];

export { PRIVILEGES };
