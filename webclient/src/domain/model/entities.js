import AdminRoot from '../../components/admin/Root';
import AdminUsers from '../../components/admin/Users';
import AdminRoles from '../../components/admin/Roles';

import PersonalRoot from '../../components/personal/Root';
import Account from '../../components/personal/Account';
import PersonalGallery from '../../components/personal/Gallery';
import Publications from '../../components/personal/Publications';

import HomeRoot from '../../components/home/Root';
import News from '../../components/home/News';

import {
  ROOT,
  USERS,
  ROLES,
  PERSONAL_ROOT,
  PERSONAL_ACCOUNT,
  PERSONAL_GALLERY,
  HOME_ROOT,
  HOME_NEWS,
  PERSONAL_NEWS,
  PERSONAL_NEWS_CONTENT,
} from './entityId';

import {
  ADMIN_MODE_PATH,
  PERSONAL_MODE_PATH,
  HOME_MODE_PATH,
} from './entityModePathId';

import { URL } from '../../network';

export default {
  [ROOT]: {
    root: true,
    component: AdminRoot,
    pageId: 'ADMIN',
    modePath: ADMIN_MODE_PATH,
  },
  [USERS]: {
    admin: true,
    label: 'Users',
    component: AdminUsers,
    data: 'users',
    loadStatus: 'usersLoadStatus',
    url: URL.USERS,
    pageId: 'ADMIN_USERS',
    modePath: ADMIN_MODE_PATH,
    /* model: {
      id: '',
      login: '',
      password: '',
      role_id: '',
      is_active: '',
      fails_count: '',
    }, */
  },

  [ROLES]: {
    admin: true,
    label: 'Roles',
    component: AdminRoles,
    data: 'roles',
    loadStatus: 'rolesLoadStatus',
    url: URL.ROLES,
    pageId: 'ADMIN_ROLES',
    modePath: ADMIN_MODE_PATH,
    /* model: {
      id: '',
      name: '',
      privileges: '',
    } */
  },

  [PERSONAL_ROOT]: {
    root: true,
    pageId: 'PERSONAL',
    modePath: PERSONAL_MODE_PATH,
    component: PersonalRoot,
  },

  [PERSONAL_ACCOUNT]: {
    personal: true,
    label: 'Account',
    component: Account,
    data: 'account',
    loadStatus: 'accountLoadStatus',
    insertToData: (oldV, newV) => ({ ...oldV, ...newV }),
    url: URL.PERSONAL_ACCOUNT,
    pageId: 'PERSONAL_ACCOUNT',
    modePath: PERSONAL_MODE_PATH,
    /* model: {
      login: '',
      email: '',
      role_id: 3,
    }
    * */
  },

  [PERSONAL_GALLERY]: {
    personal: true,
    label: 'Personal gallery',
    component: PersonalGallery,
    data: 'images',
    loadStatus: 'imagesLoadStatus',
    url: URL.PERSONAL_GALLERY,
    pageId: 'PERSONAL_GALLERY',
    modePath: PERSONAL_MODE_PATH,
    /* model: {
      id: 0,
      name: '',
      size: '',
      title: '',
      description: '',
    } */
  },

  [PERSONAL_NEWS]: {
    personal: true,
    label: 'Publications',
    component: Publications,
    data: 'news',
    loadStatus: 'newsLoadStatus',
    mapperOut: (item) => ({ ...item, is_active: Boolean(item.is_active) ? 1 : 0 }),
    url: URL.PERSONAL_NEWS,
    pageId: 'PERSONAL_NEWS',
    modePath: PERSONAL_MODE_PATH,
    /* model: {
      id: -1,
      title: '',
      content: -1,
      user_id: -1,
      created_ts,
      updated_ts,
      publishing_ts,
      is_active,
    }
    * */
  },

  [PERSONAL_NEWS_CONTENT]: {
    url: URL.NEWS_CONTENT,
  },

  [HOME_ROOT]: {
    root: true,
    pageId: 'HOME',
    modePath: HOME_MODE_PATH,
    pathname: '/',
    component: HomeRoot,
  },

  [HOME_NEWS]: {
    home: true,
    // pathname: '/news',
    pageId: 'HOME_NEWS',
    modePath: HOME_MODE_PATH,
    label: 'News',
    component: News,
    url: URL.HOME_NEWS,
    data: 'news',
    loadStatus: 'newsLoadStatus',
    /* model: {
      id: 0,
      title: '',
      content: '',
      user_id: 0,
      created_ts: '',
      updated_ts: '',
      publishing_ts: '',
      is_active: false,
    }
    * */
  },
};
