export const ROOT = 'root';

export const USERS = 'users';

export const ROLES = 'roles';

// personal
export const PERSONAL_ROOT = 'personal_root';

export const PERSONAL_ACCOUNT = 'personal_account';

export const PERSONAL_GALLERY = 'personal_gallery';

export const PERSONAL_NEWS = 'personal_news';

export const PERSONAL_NEWS_CONTENT = 'personal_news_content';

// home
export const HOME_ROOT = 'home_root';

export const HOME_NEWS = 'home_news';
