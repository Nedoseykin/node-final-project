import { BasicNewsLayout } from '../../components/publication';
import { POSITIONS } from '../../constants';

const { POSITION_BOTTOM, POSITION_TOP, POSITION_RIGHT, POSITION_LEFT } = POSITIONS;

const PUBLICATIONS = {
  BasicNewsLayout: {
    component: BasicNewsLayout,
    props: {
      textPosition: {
        component: 'BasicSelect',
        props: {
          items: [
            { id: POSITION_TOP, label: 'top' },
            { id: POSITION_RIGHT, label: 'right' },
            { id: POSITION_BOTTOM, label: 'bottom' },
            { id: POSITION_LEFT, label: 'left' },
          ],
        },
      },
      text: {
        component: 'HtmlEditorInput',
      },
      imageSrc: {
        component: 'PersonalImagesSelect',
      },
    }
  },
};

export default PUBLICATIONS;
