import request from './request';
import { URL } from './constants';

export const createThumbnail = ({
  id,
  token,
  dispatch,
  cbSuccess,
}) => {
  const url = `${URL.PERSONAL_GALLERY}/create-thumbnail`;

  return request({
    operation: 'Create thumbnail',
    dispatch,
    url,
    method: 'POST',
    headers: {
      'auth-token': token,
    },
    body: JSON.stringify({ id }),
    onError: (err) => {
      console.error(err);
    },
    onSuccess: (resp) => {
      cbSuccess && cbSuccess(resp);
    },
  });
};
