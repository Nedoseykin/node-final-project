const API = '/api/v1';

export const URL = {
  USERS: `${API}/users`,
  ROLES: `${API}/roles`,
  PERSONAL_ACCOUNT: `${API}/personal/account`,
  PERSONAL_GALLERY: `${API}/personal/images`,
  PERSONAL_NEWS: `${API}/personal/news`,
  HOME_NEWS: `${API}/home/news`,
  NEWS_CONTENT: `${API}/home/news/content`,
  LOGIN: '/auth/login',
  LOGOUT: '/auth/logout',
  REGISTER: '/auth/register',
};
