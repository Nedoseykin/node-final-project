import request from './request';
import ENTITIES from '../domain/model/entities';
import {
  acChangeRecordValue,
  acErrorLoading, acInsertNewItemToData,
  acSetLoadedData,
  acStartLoading,
  acUpdateDataWithItem
} from '../store/actions';

export const loadEntity = (entityName, token, query) => (dispatch) => {
  dispatch(acStartLoading({ entityName }));

  const url = `${ENTITIES[entityName].url}${query ? `?${query}` : ''}`;

  return request({
    operation: 'Load entity',
    dispatch,
    url,
    method: 'GET',
    headers: {
      'auth-token': token,
    },
    onError: (error) => {
      console.error(error);
      dispatch(acErrorLoading({ entityName }));
    },
    onSuccess: (loadedData) => {
      dispatch(acSetLoadedData({ entityName, loadedData }));
    },
  });
};

export const updateEntity = (entityName, token, itemSrc, cbSuccess, cbNotValid) => (dispatch) => {
  const { url, mapperOut } = ENTITIES[entityName];
  const item = mapperOut ? mapperOut(itemSrc) : itemSrc;

  return request({
    operation: 'Update entity',
    dispatch,
    url,
    method: 'PUT',
    headers: {
      'auth-token': token,
    },
    body: JSON.stringify(item),
    onError: (error) => {
      console.error(error);
    },
    onSuccess: (item) => {
      if (item && item.isValid === false) {
        if (cbNotValid) {
          cbNotValid(item);
        } else {
          dispatch(acChangeRecordValue({ name: 'validation', value: item }));
        }
      } else {
        dispatch(acUpdateDataWithItem({ entityName, item }));
        cbSuccess && cbSuccess(item);
      }
    },
  });
};

export const createEntity = (entityName, token, item, cbSuccess) => (dispatch) => {
  const url = ENTITIES[entityName].url;

  return request({
    operation: 'Create entity',
    dispatch,
    url,
    method: 'POST',
    headers: {
      'auth-token': token,
    },
    body: JSON.stringify(item),
    onError: (error) => {
      console.error(error);
    },
    onSuccess: (loadedData) => {
      if (!(loadedData && loadedData.isValid === false)) {
        dispatch(acInsertNewItemToData({ entityName, item: loadedData }));
        cbSuccess(loadedData);
      } else {
        console.log('Validation error: ', loadedData);
        dispatch(acChangeRecordValue({ name: 'validation', value: loadedData }));
      }
    },
  });
};

export const createEntityUniversal = ({
  entityName,
  token,
  body,
  reqHeaders = {},
  cbSuccess,
  cbNotValid,
}) => (dispatch) => {
  const url = ENTITIES[entityName].url;

  const headers = {
    'auth-token': token,
    ...reqHeaders,
  };

  return request({
    operation: 'Create entity',
    dispatch,
    url,
    method: 'POST',
    headers,
    body,
    onError: (err) => {
      console.error(err);
    },
    onSuccess: (resp) => {
      if (resp && resp.isValid === false) {
        cbNotValid && cbNotValid(resp);
      } else {
        cbSuccess && cbSuccess(resp);
      }
    },
  });
};

export const deleteEntity = ({
  entityName,
  token,
  id,
  cbSuccess,
}) => dispatch => {
  const url = `${ENTITIES[entityName].url}/${id}`;

  const headers = {
    'auth-token': token,
  };

  return request({
    operation: 'Delete entity',
    dispatch,
    url,
    method: 'DELETE',
    headers,
    onError: (err) => {
      console.error(err);
    },
    onSuccess: (resp) => {
      cbSuccess && cbSuccess(resp);
    },
  });
};
