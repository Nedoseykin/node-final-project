import { get } from 'lodash';
import { isFilledString } from '../utils';
import { acAddError, acLogOut } from '../store/actions';

const createError = (err, operation) => ({
  message: get(
    err, 'message',
    `${isFilledString(operation) ? `${operation}: ` : ''}Network error`,
  ),
  status: parseInt(get(err, 'status', 500), 10),
  dateTime: parseInt(get(err, 'dateTime', new Date().getTime()), 10),
});

export default ({
  operation = '',
  dispatch = () => {},
  url = '',
  method = 'GET',
  headers = {},
  body = null,
  onError,
  onSuccess,
}) => {
  let status;
  if (headers) {
    if (headers['content-type'] === undefined) {
      headers['content-type'] = 'application/json';
    } else if (headers['content-type'] === null) {
      delete headers['content-type'];
    }
  }
  return fetch(
    url,
    {
      method,
      headers,
      body,
    }
  )
    .then((response) => {
      const { headers: respHeaders } = response;
      ({ status } = response);
      const contentType = respHeaders.get('content-type');
      if (contentType.includes('json')) {
        return response.json();
      }
      if (contentType.includes('text')) {
        return response.text();
      }
      return status < 400;
    })
    .then((data) => {
      if ((status >= 400) || !data || isFilledString(data.message)) {
        throw createError(data)
      }
      return onSuccess && onSuccess(data);
    })
    .catch((err) => {
      const errObj = createError(err, operation);
      dispatch(acAddError({ ...errObj }));
      if (status === 401) {
        dispatch(acLogOut());
      }
      onError && onError(errObj);
    });
};
