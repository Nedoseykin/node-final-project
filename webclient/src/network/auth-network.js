import request from './request';

import {
  acLogInStart,
  acLogInSuccess,
  acLogInError,
  acLogOut,
  acRegisterStart,
  acRegisterError,
  acRegisterSuccess,
} from '../store/actions';

import { URL } from './constants';
import { getErrorObject, isExists, isFilledString } from '../utils';

export const register = ({
  login, email, password, confirmPassword
}, onValidate) => (dispatch) => {
  dispatch(acRegisterStart());

  return request({
    operation: 'Registration',
    dispatch,
    url: URL.REGISTER,
    method: 'POST',
    body: JSON.stringify({ login, email, password, confirmPassword }),
    onError: (err) => {
      console.error(err);
      const errObj = getErrorObject(err, 'Registration');
      dispatch(acRegisterError({ message: errObj.message }));
    },
    onSuccess: (loadedData) => {
      if (loadedData && (loadedData.isValid === false)) {
        onValidate && onValidate(loadedData);
        dispatch(acRegisterError('Registration error: validation failed'));
      } else {
        onValidate && onValidate({ isValid: true });
        const authToken = loadedData && loadedData.token;
        const roleId = loadedData && loadedData.role_id;
        const privileges = loadedData && loadedData.privileges;
        if (isFilledString(authToken) && isExists(roleId)) {
          dispatch(acRegisterSuccess({ authToken, roleId, login, privileges }));
        } else {
          const message = 'Registration error: failed to create session';
          dispatch(acRegisterError({ message }));
        }
      }
    },
  });
};

export const login = (userLogin, userPassword) => (dispatch) => {
  dispatch(acLogInStart());

  return request({
    operation: 'Login',
    dispatch,
    url: URL.LOGIN,
    method: 'POST',
    body: JSON.stringify({ login: userLogin, password: userPassword }),
    onError: (err) => {
      console.error(err);
      const errObj = getErrorObject(err, 'Login');
      dispatch(acLogInError({ message: errObj.message }));
    },
    onSuccess: (loadedData) => {
      const authToken = loadedData && loadedData.token;
      const roleId = loadedData && loadedData.role_id;
      const privileges = loadedData && loadedData.privileges;
      dispatch(acLogInSuccess({ authToken, roleId, login: userLogin, privileges }));
    },
  });
};

export const logout = token => (dispatch) => {
  return request({
    operation: 'Logout',
    dispatch,
    url: URL.LOGOUT,
    method: 'POST',
    body: JSON.stringify({ token }),
    onError: (err) => {
      console.error('Error: logout request failed: ', err);
    },
    onSuccess: () => {
      dispatch(acLogOut());
    },
  });
};
