import request from './request';
import { URL } from './constants';
import {
  acHideDialog, acInsertNewItemToData, acLogInSuccess, acModalChangeValue,
  acUpdateDataWithItem
} from '../store/actions';
import { PERSONAL_ACCOUNT, USERS } from '../domain/model/entityId';
import { CHANGE_PASSWORD_DIALOG } from '../domain/modals/modalsId';
import ENTITIES from '../domain/model/entities';

export const unlockUser = (token, id) => (dispatch) => {
  return request({
    operation: 'User unlocking',
    dispatch,
    url: `${URL.USERS}/unlock`,
    method: 'PUT',
    headers: {
      'auth-token': token,
    },
    body: JSON.stringify({ id }),
    onError: (error) => {
      console.error(error);
    },
    onSuccess: (item) => {
      dispatch(acUpdateDataWithItem({ entityName: USERS, item }));
    },
  });
};

const passwordChange = (entityName, token, payload) => (dispatch) => {
  return request({
    operation: 'Password change',
    dispatch,
    url: `${ENTITIES[entityName].url}/change-password`,
    method: 'PUT',
    headers: {
      'auth-token': token,
    },
    body: JSON.stringify(payload),
    onError: (error) => {
      console.error(error);
    },
    onSuccess: (loadedData) => {
      if (loadedData && (loadedData.isValid === false)) {
        dispatch(acModalChangeValue({ modalId: CHANGE_PASSWORD_DIALOG, name: 'validation', value: loadedData }));
      } else {
        dispatch(acHideDialog({ modalId: CHANGE_PASSWORD_DIALOG }));
      }
    },
  });
};

export const userPasswordChange = (token, payload) => (dispatch) => {
  return passwordChange(USERS, token, payload)(dispatch);
};

export const accountPasswordChange = (token, payload) => (dispatch) => {
  return passwordChange(PERSONAL_ACCOUNT, token, payload)(dispatch);
};

export const activationRequest = (token, cbSuccess) => (dispatch) => {
  return request({
    operation: 'Account activation',
    dispatch,
    url: `${URL.PERSONAL_ACCOUNT}/activate`,
    method: 'PUT',
    headers: {
      'auth-token': token,
    },
    onError: (err) => {
      console.error(err);
    },
    onSuccess: (loadedData) => {
      dispatch(acLogInSuccess({
        authToken: token,
        roleId: loadedData.role_id,
        login: loadedData.login,
        privileges: loadedData.privileges,
      }));
      dispatch(acInsertNewItemToData({
        entityName: PERSONAL_ACCOUNT,
        item: { ...loadedData },
      }));
      cbSuccess && cbSuccess(loadedData);
    },
  });
};
