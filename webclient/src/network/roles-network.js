import { loadEntity } from './entity-network';
import { ROLES } from '../domain/model/entityId';

export const loadRoles = (token) => loadEntity(ROLES, token);
