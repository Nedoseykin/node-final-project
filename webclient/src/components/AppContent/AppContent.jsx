import React from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';

import './AppContent.scss';
import { isFilledString } from '../../utils';

const AppContent = ({
  className,
  children,
}) => {
  const calculatedClassName = classNames(
    'AppContent',
    isFilledString(className) && className,
  );

  return (
    <div className={calculatedClassName}>
      {children}
    </div>
  )
};

AppContent.propTypes = {
  className: PropTypes.string,
  children: PropTypes.any,
};

AppContent.defaultProps = {
  className: '',
  children: [],
};

export default AppContent;
