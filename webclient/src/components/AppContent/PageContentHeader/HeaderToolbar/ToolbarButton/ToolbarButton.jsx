import React from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';

import { isFilledString } from '../../../../../utils';

import './ToolbarButton.scss';

const ToolbarButton = ({
  className,
  children,
  onClick,
  disabled,
  condition,
  type,
  ...rest
}) => {
  const calculatedClassName = classNames(
    'ToolbarButton',
    isFilledString(className) && className,
    disabled && 'ToolbarButton__disabled',
  );

  return condition && (
    <button
      className={calculatedClassName}
      disabled={disabled === true}
      onClick={onClick}
      type={type || 'button'}
      {...rest}
    >
      {children}
    </button>
  )
};

ToolbarButton.propTypes = {
  className: PropTypes.string,
  children: PropTypes.any,
  disabled: PropTypes.bool,
  condition: PropTypes.bool,
  type: PropTypes.string,
  onClick: PropTypes.func,
};

ToolbarButton.defaultProps = {
  className: '',
  children: [],
  disabled: false,
  condition: true,
  type: 'button',
  onClick: () => {},
};

export { ToolbarButton };
