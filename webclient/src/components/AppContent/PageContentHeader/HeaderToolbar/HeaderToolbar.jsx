import React from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';
import { isFilledString } from '../../../../utils';

import './HeaderToolbar.scss';

const HeaderToolbar = ({
  className,
  children,
}) => {
  const calculatedClassName = classNames(
    'HeaderToolbar',
    isFilledString(className) && className,
  );

  return (
    <div className={calculatedClassName}>
      {children}
    </div>
  )
};

HeaderToolbar.propTypes = {
  classNames: PropTypes.string,
  children: PropTypes.any,
};

HeaderToolbar.defaultProps = {
  classNames: '',
  children: [],
};

export { HeaderToolbar };
