import React from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';
import { isFilledString } from '../../../../utils';

import './HeaderTitle.scss';

const HeaderTitle = ({ title, className }) => {
  const calculatedClassName = classNames(
    'HeaderTitle',
    isFilledString(className) && className,
  );

  return (
    <h3 className={calculatedClassName}>
      {title}
    </h3>
  )
};

HeaderTitle.propTypes = {
  title: PropTypes.string,
  className: PropTypes.string,
};

HeaderTitle.defaultProps = {
  title: '',
  className: '',
};

export { HeaderTitle };
