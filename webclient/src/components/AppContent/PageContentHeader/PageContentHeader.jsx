import React from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';

import './PageContentHeader.scss';
import { isFilledString } from '../../../utils';

const PageContentHeader = ({
  className,
  children,
}) => {
  const calculatedClassName = classNames(
    'PageContentHeader',
    isFilledString(className) && className,
  );

  return (
    <div className={calculatedClassName}>
      {children}
    </div>
  )
};

PageContentHeader.propTypes = {
  children: PropTypes.any,
};

PageContentHeader.defaultProps = {
  children: [],
};

export { PageContentHeader };
