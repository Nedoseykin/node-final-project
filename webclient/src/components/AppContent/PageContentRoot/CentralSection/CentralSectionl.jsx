import React from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';

import { isFilledString } from '../../../../utils';

import './CentralSection.scss';

const CentralSection = ({
  className,
  children,
}) => {
  const calculatedClassName = classNames(
    'CentralSection',
    isFilledString(className) && className,
  );

  return (
    <section className={calculatedClassName}>
      {children}
    </section>
  )
};

CentralSection.propTypes = {
  className: PropTypes.string,
  children: PropTypes.any,
};

CentralSection.defaultProp = {
  className: '',
  children: [],
};

export { CentralSection };
