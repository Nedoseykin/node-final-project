import React from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';

import './Aside.scss';
import { isFilledString } from '../../../../utils';

const Aside = ({
  className,
  children,
}) => {
  const calculatedClassName = classNames(
    'Aside',
    isFilledString(className) && className,
  );

  return (
    <aside className={calculatedClassName}>
      {children}
    </aside>
  )
};

Aside.propTypes = {
  className: PropTypes.string,
  children: PropTypes.any,
};

Aside.defaultProps = {
  className: '',
  children: [],
};

export { Aside };
