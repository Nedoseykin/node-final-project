import React from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';

import { isFilledString } from '../../../utils';

import './PageContentRoot.scss';

const PageContentRoot = ({
  className,
  children,
}) => {
  const calculatedClassName = classNames(
    'PageContentRoot',
    isFilledString(className) && className,
  );

  return (
    <div className={calculatedClassName}>
      {children}
    </div>
  );
};

PageContentRoot.propTypes = {
  className: PropTypes.string,
  children: PropTypes.any,
};

PageContentRoot.defaultProps = {
  className: '',
  children: [],
};

export { PageContentRoot };
