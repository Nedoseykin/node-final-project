import { connect } from 'react-redux';
import { push } from 'connected-react-router';

import { HomeRoot } from './HomeRoot';
import { homeRootEntitiesSelector } from '../../../store/selectors';
import ENTITIES from '../../../domain/model/entities';
import { PAGES } from '../../../constants';

const setHomeMode = homeMode => (dispatch) => {
  const { pageId } = ENTITIES[homeMode];
  const { pathName } = PAGES[pageId];
  dispatch(push(pathName));
};

const mapStateToProps = state => ({
  entities: homeRootEntitiesSelector(state),
});

const mapDispatchToProps = {
  setHomeMode,
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeRoot);
