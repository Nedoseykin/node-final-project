import React from 'react';
import * as PropTypes from 'prop-types';

import styles from './HomeRoot.module.scss';

const HomeRoot = ({
  entities,
  setHomeMode,
}) => {
  const items = entities.map((item) => {
    const { id, label } = item;
    return (
      <button
        key={id}
        className={styles.Entity__btn}
        onClick={() => { setHomeMode(id); }}
      >
        {label}
      </button>
    );
  });

  return (
    <div className={styles.HomeRoot}>
      {items}
    </div>
  );
};

HomeRoot.propTypes = {
  entities: PropTypes.arrayOf(PropTypes.shape({})),
  setHomeMode: PropTypes.func,
};

HomeRoot.defaultProps = {
  entities: [],
  setHomeMode: () => {},
};

export { HomeRoot };
