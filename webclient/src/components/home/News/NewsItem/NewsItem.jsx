import React from 'react';
import * as PropTypes from 'prop-types';

import { Link } from 'react-router-dom';

import NewsItemHeader from '../NewsItemHeader';

import styles from './NewsItem.module.scss';
import { PAGES } from '../../../../constants';

const NewsItem = ({
  id,
  title,
  description,
  content,
  login,
  publishing_ts,
}) => {
  const published = publishing_ts
    ? new Date(publishing_ts).toLocaleDateString()
    : '';

  return (
    <li
      id={`news-${id}`}
      className={styles.NewsItem}
    >
      <Link
        className={styles.link}
        to={`${PAGES.HOME_NEWS.pathName}/${id}`}
      >
        <NewsItemHeader
          title={title}
          description={description}
          content={content}
          login={login}
          published={published}
        />
      </Link>
    </li>
  );
};

NewsItem.propTypes = {
  id: PropTypes.number,
  title: PropTypes.string,
  description: PropTypes.string,
  content: PropTypes.number,
};

NewsItem.defaultProps = {
  id: -1,
  title: '',
  description: '',
  content: -1,
};

export { NewsItem };
