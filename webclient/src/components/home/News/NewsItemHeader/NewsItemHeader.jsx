import React from 'react';
import * as PropTypes from 'prop-types';

import styles from './NewsItemHeader.module.scss';

const NewsItemHeader = ({
  title,
  description,
  content,
  login,
  published,
}) => {
  return (
    <div className={styles.NewsItemHeader}>
      <div key="header" className={styles.header}>
        <h3 className={styles.title}>{title}</h3>

        <div key="subHeader" className={styles.subtitle}>
          <span key="author" className={styles.author}>Author: {login}</span>
          {
            published && (
              <span key="publishingDate" className={styles.publishingDate}>Published: {published}</span>
            )
          }
        </div>
      </div>

      <div className={styles.content}>
        {`${description}`}
      </div>
    </div>
  )
};

NewsItemHeader.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  content: PropTypes.number,
  login: PropTypes.string,
  published: PropTypes.string,
};

NewsItemHeader.defaultProps = {
  title: '',
  description: '',
  content: -1,
  login: '',
  published: '',
};

export { NewsItemHeader };
