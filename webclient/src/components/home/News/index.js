import { connect } from 'react-redux';

import { News } from './News';
import {
  closeHomeSection,
  reloadEntity,
} from '../../../store/dispatchers';
import {
  authTokenSelector,
  homeActiveEntityItemIdSelector,
  homeActiveNewsSelector,
  homePreviousPathNameSelector,
  homeSearchTextSelector,
  newsLoadStatusSelector,
  newsSelector,
} from '../../../store/selectors';
import { loadEntity } from '../../../network';
import { HOME_NEWS } from '../../../domain/model/entityId';
import { acChangeModeModelValue } from '../../../store/actions';

const loadNews = (token, searchText) => (dispatch) => {
  const query = searchText ? `search=${searchText}` : '';
  loadEntity(HOME_NEWS, token, query)(dispatch);
};

const changeModeModel = (e) => (dispatch) => {
  const { name, value } = e.target;
  dispatch(acChangeModeModelValue({ entityId: HOME_NEWS, name, value }));
};

const mapStateToProps = state => ({
  authToken: authTokenSelector(state),
  news: newsSelector(state),
  newsId: homeActiveEntityItemIdSelector(state),
  activeNews: homeActiveNewsSelector(state),
  newsLoadStatus: newsLoadStatusSelector(state),
  previousPathName: homePreviousPathNameSelector(state),
  searchText: homeSearchTextSelector(state),
});

const mapDispatchToProps = {
  loadNews,
  reloadEntity,
  closeSection: closeHomeSection,
  changeModeModel,
};

export default connect(mapStateToProps, mapDispatchToProps)(News);