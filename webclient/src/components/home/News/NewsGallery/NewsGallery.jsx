import React from 'react';
import * as PropTypes from 'prop-types';

import styles from './NewsGallery.module.scss';

const NewsGallery = ({ children }) => {
  return (
    <ul className={styles.NewsGallery}>
      {children}
    </ul>
  )
};

NewsGallery.propTypes = {
  children: PropTypes.any,
};

NewsGallery.defaultProps = {
  children: null,
};

export { NewsGallery };
