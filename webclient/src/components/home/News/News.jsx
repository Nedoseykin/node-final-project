import React, { PureComponent } from 'react';
import * as PropTypes from 'prop-types';

import {
  AppContent,
  PageContentHeader,
  HeaderTitle,
  HeaderToolbar,
  ToolbarButton,
  PageContentRoot,
} from '../../AppContent';
import NewsGallery from './NewsGallery';
import NewsItem from './NewsItem';
import { NewsContainer } from '../../publication';
import { SearchInput } from '../../controls';

import { isFilledArray } from '../../../utils';
import { HOME_NEWS } from '../../../domain/model/entityId';

import './News.scss';

class News extends PureComponent {
  static propTypes = {
    authToken: PropTypes.string,
    news: PropTypes.arrayOf(PropTypes.shape({})),
    newsLoadStatus: PropTypes.number,
    closeSection: PropTypes.func,
    reloadEntity: PropTypes.func,
    loadNews: PropTypes.func,
    newsId: PropTypes.number,
    activeNews: PropTypes.shape({}),
    previousPathName: PropTypes.string,
    searchText: PropTypes.string,
    changeModeModel: PropTypes.func,
  };

  static defaultProps = {
    authToken: null,
    news: null,
    newsLoadStatus: 0,
    closeSection: () => {},
    reloadEntity: () => {},
    loadNews: () => {},
    newsId: -1,
    activeNews: null,
    previousPathName: '/',
    searchText: '',
    changeModeModel: () => {},
  };

  componentDidMount() {
    const { newsLoadStatus, reloadEntity } = this.props;
    if (newsLoadStatus > 1) {
      reloadEntity(HOME_NEWS);
    }
    this.updateData();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    this.updateData();
  }

  updateData = () => {
    const {
      news,
      newsLoadStatus,
      authToken,
      loadNews,
      reloadEntity,
      searchText,
    } = this.props;

    if (newsLoadStatus === 0) {
      loadNews(authToken, searchText);
    }

    if ((newsLoadStatus === 2) && !Array.isArray(news)) {
      reloadEntity(HOME_NEWS);
    }
  };

  getNesGalleryContent = () => {
    const {
      news,

    } = this.props;
    return isFilledArray(news)
      ?  news.map((item) =>  <NewsItem key={item.id} {...item} />)
      : 'No news is available';
  };

  render() {
    const {
      closeSection,
      reloadEntity,
      activeNews,
      previousPathName,
      searchText,
      changeModeModel,
    } = this.props;

    const newsGalleryContent = this.getNesGalleryContent();
// todo hide search on page with news ID
    return (
      <AppContent className="News">
        <PageContentHeader>
          <HeaderTitle title="News" />
          <HeaderToolbar>
            {
              isFilledArray(newsGalleryContent) && !activeNews && (
                <span className="toolbar__control search">
              <SearchInput
                id="searchText"
                name="searchText"
                autoComplete="off"
                autoFocus={true}
                placeholder="Enter text for search..."
                value={searchText}
                onChange={changeModeModel}
                onEnterPress={() => { reloadEntity(HOME_NEWS); }}
              />
            </span>
              )
            }
            <ToolbarButton
              key="btnReload"
              onClick={() => { reloadEntity(HOME_NEWS); }}
            >
              Reload
            </ToolbarButton>

            <ToolbarButton
              key="buttonClose"
              onClick={() => closeSection(previousPathName)}
            >
              Close
            </ToolbarButton>
          </HeaderToolbar>
        </PageContentHeader>
        <PageContentRoot>
          {
            isFilledArray(newsGalleryContent) && (!activeNews) && (
              <NewsGallery>
                {newsGalleryContent}
              </NewsGallery>
            )
          }
          {
            !isFilledArray(newsGalleryContent) && newsGalleryContent
          }
          {
            isFilledArray(newsGalleryContent) && activeNews && (
              <NewsContainer activeNews={activeNews} />
            )
          }
        </PageContentRoot>
      </AppContent>
    );
  }
}

export { News };
