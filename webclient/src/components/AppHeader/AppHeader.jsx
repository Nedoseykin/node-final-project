import React from 'react';
import * as PropTypes from 'prop-types';

import styles from './AppHeader.module.scss';

const AppHeader = ({
  title,
  authToken,
  login,
  isLogged,
  withLoginButton,
  logout,
  goToLogin,
}) => {
  const buttonProps = {
    onClick: isLogged
      ? () => { logout(authToken); }
      : goToLogin,
  };
  const buttonLabel = isLogged ? 'Log out' : 'Log in';

  return (
    <header className={styles.AppHeader}>
      <h1 className={styles.title}>
        { title }
      </h1>
      {
        withLoginButton && (
          <span className={styles.toolbar}>
            {
              isLogged && (
                <span className={styles.infoLabel}>
              Logged as: {login}
            </span>
              )
            }
            <button
              className={styles.toolButton}
              {...buttonProps}
            >
              {buttonLabel}
            </button>
          </span>
        )
      }
    </header>
  );
};

AppHeader.propTypes = {
  title: PropTypes.string,
  authToken: PropTypes.string,
  login: PropTypes.string,
  isLogged: PropTypes.bool,
  withLoginButton: PropTypes.bool,
  logout: PropTypes.func,
  goToLogin: PropTypes.func,
};

AppHeader.defaultProps = {
  title: '',
  authToken: '',
  login: '',
  isLogged: false,
  withLoginButton: false,
  logout: () => {},
  goToLogin: () => {},
};

export { AppHeader };
