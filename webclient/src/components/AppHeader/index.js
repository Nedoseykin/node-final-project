import { connect } from 'react-redux';
import { push } from 'connected-react-router';

import { AppHeader } from './AppHeader';

import { logout } from '../../network';

import {
  appTitleSelector,
  authLoginSelector,
  authTokenSelector,
  isLoggedSelector,
  withLoginButtonSelector,
} from '../../store/selectors';

import { PAGES } from '../../constants';

const goToLogin = () => (dispatch) => {
  dispatch(push(PAGES.LOGIN.pathName));
};

const mapStateToProps = state => ({
  title: appTitleSelector(state),
  authToken: authTokenSelector(state),
  login: authLoginSelector(state),
  isLogged: isLoggedSelector(state),
  withLoginButton: withLoginButtonSelector(state),
});

const mapDispatchToProps = {
  logout,
  goToLogin,
};

export default connect(mapStateToProps, mapDispatchToProps)(AppHeader);
