import React from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';

import styles from './Grid.module.scss';
import { isFilledString } from '../../../utils';

const Grid = ({
  className,
  direction,
  wrap,
  justifyContent,
  alignItems,
  isVerticalPadding,
  children,
}) => {
  const calculatedClassName = classNames(
    'Grid',
    styles[direction],
    styles[wrap],
    styles[justifyContent],
    styles[alignItems],
    isVerticalPadding && styles.verticalPadding,
    isFilledString(className) && className,
  );
  return (
    <div className={calculatedClassName}>
      {children}
    </div>
  );
};

Grid.propTypes = {
  direction: PropTypes.oneOf(['column', 'row']),
  wrap: PropTypes.oneOf(['wrap', 'nowrap']),
  justifyContent: PropTypes.oneOf(['justifyCenter', 'spaceBetween', 'spaceAround', 'justifyStart', 'justifyEnd']),
  alignItems: PropTypes.oneOf(['alignCenter', 'alignStart', 'alignEnd', 'stretch', 'baseline']),
  isVerticalPadding: PropTypes.bool,
  children: PropTypes.any,
  className: PropTypes.string,
};

Grid.defaultProps = {
  direction: 'row',
  wrap: 'wrap',
  justifyContent: 'justifyStart',
  alignItems: 'start',
  isVerticalPadding: true,
  children: [],
  className: '',
};

export default Grid;
