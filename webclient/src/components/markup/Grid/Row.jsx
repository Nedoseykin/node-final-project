import React from 'react';

import Grid from './Grid';

const Row = ({
  wrap = 'nowrap',
  justifyContent = 'spaceBetween',
  alignItems = 'center',
  isVerticalPadding = true,
  children = [],
}) => (
  <Grid
    wrap={wrap}
    justifyContent={justifyContent}
    alignItems={alignItems}
    isVerticalPadding={isVerticalPadding}
  >
    {children}
  </Grid>
);

export default Row;
