import React from 'react';
import { NavLink } from 'react-router-dom';

import styles from './NavBar.module.scss'

const NavBar = ({ items, isVisible }) => {
  const navLinks = items.map((page) => {
    const { id, navLinkLabel, pathName } = page;
    return (
      <NavLink
        key={id}
        exact
        to={pathName}
        className={styles.link}
        activeClassName={styles.selected}
      >
        {navLinkLabel}
      </NavLink>
    )
  });

  return isVisible && (
    <nav className={styles.NavBar}>
      { navLinks }
    </nav>
  );
};

export { NavBar };
