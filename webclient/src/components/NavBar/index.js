import { connect } from 'react-redux';

import { NavBar } from './NavBar';

import { navBarItemsSelector, navBarIsVisibleSelector } from '../../store/selectors';

const mapStateToProps = state => ({
  items: navBarItemsSelector(state),
  isVisible: navBarIsVisibleSelector(state),
});

export default connect(mapStateToProps)(NavBar);
