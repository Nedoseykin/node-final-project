import React from 'react';

const TriangleArrowLeft = ({ className }) => {
  return (
    <svg
      className={className}
      width="100%"
      height="100%"
      viewBox="0 0 16 16"
      preserveAspectRatio="xMidYMid meet"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M5 8L11 3V13Z"
        fill="gray"
        stroke="gray"
        strokeWidth="1"
      />
    </svg>
  )
};

export default TriangleArrowLeft;
