import React from 'react';

const Search = ({ className }) => {
  return (
    <svg
      className={className}
      width="1rem"
      height="1rem"
      viewBox="0 0 16 16"
      preserveAspectRatio="xMidYMid meet"
    >
      <g>
        <circle
          cx="10"
          cy="6"
          r="5"
          stroke="rgba(30, 144, 255, 1)"
          strokeWidth="2"
          fill="none"
        />
        <path
          d="M0 16L6 10"
          stroke="rgba(30, 144, 255, 1)"
          strokeWidth="2"
        />
      </g>
    </svg>
  )
};

export default Search;