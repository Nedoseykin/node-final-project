import { connect } from 'react-redux';

import { AdminRoute } from './AdminRoute';
import {
  authTokenSelector,
  isLoggedSelector,
  roleIdSelector,
} from '../../../store/selectors';

const mapStateToProps = state => ({
  isLogged: isLoggedSelector(state),
  authToken: authTokenSelector(state),
  roleId: roleIdSelector(state),
});

export default connect(mapStateToProps)(AdminRoute);
