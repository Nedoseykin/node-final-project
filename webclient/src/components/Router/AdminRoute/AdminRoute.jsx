import React from 'react';
import { Redirect, Route } from 'react-router-dom';

const AdminRoute = ({ children, isLogged, roleId, authToken, ...rest }) => {
  return (
    <Route
      { ...rest }
      render={
        ({ location }) => {
          if (!isLogged) {
            return (
              <Redirect
                to={{
                  pathname: '/login',
                  state: { from: location },
                }}
              />
            );
          }
          if (roleId !== 1) {
            return (
              <Redirect
                to={{
                  pathname: '/',
                  state: { from: location },
                }}
              />
            );
          }
          return children;
        }
      }
    />
  )
};

export { AdminRoute };
