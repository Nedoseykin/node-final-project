import { connect } from 'react-redux';

import { PrivateRoute } from './PrivateRoute';
import {
  adminModePathSelector,
  authTokenSelector,
  homeModePathSelector,
  isLoggedSelector,
  personalModePathSelector,
  privilegesSelector,
} from '../../../store/selectors';
import { acSetModePath } from '../../../store/actions';

const setModePath = (modePath, modePathValue) => (dispatch) => {
  dispatch(acSetModePath({ modePath, modePathValue }));
};

const mapStateToProps = state => ({
  isLogged: isLoggedSelector(state),
  authToken: authTokenSelector(state),
  privileges: privilegesSelector(state),
  homeModePath: homeModePathSelector(state),
  personalModePath: personalModePathSelector(state),
  adminModePath: adminModePathSelector(state),
});

const mapDispatchToProps = {
  setModePath,
};

export default connect(mapStateToProps, mapDispatchToProps)(PrivateRoute);
