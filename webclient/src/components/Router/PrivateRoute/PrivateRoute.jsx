import React from 'react';
import * as PropTypes from 'prop-types';
import { Redirect, Route,  } from 'react-router-dom';
import { useModePath } from '../../../hooks';
import { PAGES } from '../../../constants';
import { hasAnyPrivilegesFor, isFilledString } from '../../../utils';

const PrivateRoute = ({
  children,
  isLogged,
  privileges,
  authToken,
  ...rest
}) => {
  // console.log('location: ', rest.location);
  const { pathname } = rest.location;
  const pageName = Object.keys(PAGES)
    .find(key => PAGES[key].pathName === pathname);
  let hasPrivileges = true;

  if (isFilledString(pageName)) {
    const { privilegesList } = PAGES[pageName];
    hasPrivileges = hasAnyPrivilegesFor(privileges, privilegesList);
    // console.log('%c%s', 'color: green;', 'hasPrivileges: ', hasPrivileges);
  }

  useModePath(rest);

  return (
    <Route
      { ...rest }
      render={
        ({ location }) => (
          isLogged
            ? hasPrivileges
              ? children
              : <Redirect
                  to={{
                    pathname: '/',
                    state: { from: { pathname: '/' } }
                  }}
                />
            : <Redirect
                to={{
                  pathname: '/login',
                  state: { from: location },
                }}
            />
        )
      }
    />
  )
};

PrivateRoute.propTypes = {
  children: PropTypes.any,
  isLogged: PropTypes.bool,
  privileges: PropTypes.string,
  authToken: PropTypes.string,
};

PrivateRoute.defaultProps = {
  children: null,
  isLogged: false,
  privileges: null,
  authToken: null,
};

export { PrivateRoute };
