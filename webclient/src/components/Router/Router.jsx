import React from 'react';
import { Switch, Route } from 'react-router-dom';

import PrivateRoute from './PrivateRoute';
import PublicRoute from './PublicRoute';

import HomePage from '../pages/HomePage';
import LoginPage from '../pages/LoginPage';
import RegistrationPage from '../pages/RegistrationPage';
import AdminPage from '../pages/AdminPage';
import PersonalPage from '../pages/PersonalPage';
import ErrorPage from '../pages/ErrorPage';

const Router = () => (
  <Switch>
    <PublicRoute exact path="/">
      <HomePage />
    </PublicRoute>

    <Route path="/login">
      <LoginPage />
    </Route>

    <Route path="/registration">
      <RegistrationPage />
    </Route>

    <PrivateRoute exact path="/news">
      <HomePage />
    </PrivateRoute>

    <PrivateRoute path="/news/:activeId">
      <HomePage />
    </PrivateRoute>

    <PrivateRoute path="/admin">
      <AdminPage />
    </PrivateRoute>

    <PrivateRoute path="/personal">
      <PersonalPage />
    </PrivateRoute>

    <PrivateRoute path="/personal/account">
      <PersonalPage />
    </PrivateRoute>

    <PrivateRoute path="/personal/gallery">
      <PersonalPage />
    </PrivateRoute>

    <PrivateRoute path="/personal/publications">
      <PersonalPage />
    </PrivateRoute>

    <Route>
      <ErrorPage />
    </Route>
  </Switch>
);

export default Router;
