import { connect } from 'react-redux';

import { PublicRoute } from './PublicRoute';
import { homeModePathSelector } from '../../../store/selectors';
import { acSetModePath } from '../../../store/actions';

const setModePath = (modePath, modePathValue) => (dispatch) => {
  dispatch(acSetModePath({ modePath, modePathValue }));
};

const mapStateToProps = state => ({
  homeModePath: homeModePathSelector(state),
});

const mapDispatchToProps = {
  setModePath,
};

export default connect(mapStateToProps, mapDispatchToProps)(PublicRoute);
