import React from 'react';
import * as PropTypes from 'prop-types';
import { Route } from 'react-router-dom';
import { useModePath } from '../../../hooks';

const PublicRoute = ({ children, ...rest }) => {
  useModePath(rest);

  return (
    <Route { ...rest } render={() => children} />
  )
};

PublicRoute.propTypes = {
  children: PropTypes.any,
};

PublicRoute.defaultProps = {
  children: null,
};

export { PublicRoute };
