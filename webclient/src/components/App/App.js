import React from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';

import styles from './App.module.scss';

import Router from '../Router/Router';
import AppHeader from '../AppHeader';
import NavBar from '../NavBar';
import ErrorModalDialog from '../modal/ErrorModalDialog';

import { configureStore, history } from '../../store';

const store = configureStore();

function App() {
  return (
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <div className={styles.App}>
          <AppHeader />
          <NavBar />
          <Router />
          <ErrorModalDialog />
        </div>
      </ConnectedRouter>
    </Provider>
  );
}

export default App;
