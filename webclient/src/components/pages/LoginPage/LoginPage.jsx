import React, { useEffect } from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';

import styles from './LoginPage.module.scss';

import LoginForm from '../../login/LoginForm';

const LoginPage = ({
  login,
  password,
  isLogging,
  onLogin,
  location,
  isLogged,
  replace,
  onChange,
}) => {
  useEffect(() => {
    if (isLogged) {
      const { from } = location.state || { from: { pathname: '/' } };
      replace(from);
    }
  });

  const calculatedClassName = classNames(
    'PageContent',
    styles.LoginPage,
  );

  return (
    <div className={calculatedClassName}>
      <LoginForm
        login={login}
        password={password}
        onChange={onChange}
        onLogin={onLogin}
        isLogged={isLogged}
        isLogging={isLogging}
      />
    </div>
  )
};

LoginPage.propTypes = {
  login: PropTypes.string,
  password: PropTypes.string,
  isLogging: PropTypes.bool,
  onLogin: PropTypes.func,
  isLogged: PropTypes.bool,
  location: PropTypes.object,
  replace: PropTypes.func,
  onChange: PropTypes.func,
};

LoginPage.defaultProps = {
  login: '',
  password: '',
  isLogging: false,
  onLogin: () => {},
  location: {},
  isLogged: false,
  replace: () => {},
  onChange: () => {},
};

export { LoginPage };
