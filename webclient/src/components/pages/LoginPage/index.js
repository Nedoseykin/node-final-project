import { connect } from 'react-redux';
import { replace } from 'connected-react-router';

import { LoginPage } from './LoginPage'
import { acChangeValue } from '../../../store/actions';
import { login as onLogin } from '../../../network';
import {
  isLoggedSelector,
  isLoggingSelector,
  locationSelector,
  loginSelector,
  passwordSelector
} from '../../../store/selectors';

const onChange = e => (dispatch) => {
  const { name, value } = e.target;
  dispatch(acChangeValue({ name, value }));
};

const mapStateToProps = (state) => ({
  location: locationSelector(state),
  isLogging: isLoggingSelector(state),
  isLogged: isLoggedSelector(state),
  login: loginSelector(state),
  password: passwordSelector(state),
});

const mapDispatchToProps = {
  onLogin,
  replace,
  onChange,
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
