import React from 'react';
import * as PropTypes from 'prop-types';

const AdminPage = ({ adminContent: AdminContent }) => (
  <div className="PageContent">
    {
      AdminContent && ( <AdminContent /> )
    }
  </div>
);

AdminPage.propTypes = {
  adminContent: PropTypes.any,
};

AdminPage.defaultProps = {
  adminContent: null,
};

export { AdminPage };
