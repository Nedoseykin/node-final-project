import { connect } from 'react-redux';
import { AdminPage } from './AdminPage';
import {
  adminContentSelector,
} from '../../../store/selectors';

const mapStateToProps = state => ({
  adminContent: adminContentSelector(state),
});

export default connect(mapStateToProps)(AdminPage);
