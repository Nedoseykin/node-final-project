import { connect } from 'react-redux';

import { PersonalPage } from './PersonalPage';
import { personalContentSelector } from '../../../store/selectors';

const mapStateToProps = state => ({
  personalContent: personalContentSelector(state),
});

export default connect(mapStateToProps)(PersonalPage);
