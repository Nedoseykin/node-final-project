import React from 'react';
import * as PropTypes from 'prop-types';

const PersonalPage = ({ personalContent: PersonalContent }) => (
  <div className="PageContent">
    {
      PersonalContent && ( <PersonalContent /> )
    }
  </div>
);

PersonalPage.propTypes = {
  personalContent: PropTypes.any,
};

PersonalPage.defaultProps = {
  personalContent: null,
};

export { PersonalPage };
