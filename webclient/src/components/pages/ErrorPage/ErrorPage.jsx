import React from 'react';

const ErrorPage = () => (
  <div className="PageContent">
    Something goes wrong. Resource was not found.
  </div>
);

export { ErrorPage };
