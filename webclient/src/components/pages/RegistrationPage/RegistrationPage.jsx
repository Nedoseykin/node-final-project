import React, { useState, useEffect } from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';

import styles from './RegistrationPage.module.scss';

import RegistrationForm from '../../login/RegistrationForm';
import { isExists } from '../../../utils';

const RegistrationPage = ({
  // location,
  isLogged,
  isRegistering,
  register,
  // push,
  replace,
  goBack,
}) => {
  const [state, setState] = useState({});
  const [validation, setValidation] = useState({});

  useEffect(() => {
    if (isLogged) {
      replace({ pathname: '/' });
    }
  });

  const onChange = (e) => {
    const { name, value } = e.target;
    setState({ ...state, [name]: value });
  };

  const onValidate = (validation) => {
    if (validation && isExists(validation.isValid)) {
      setValidation({ ...validation });
    }
  };

  const calculatedClassName = classNames(
    'PageContent',
    styles.RegistrationPage,
  );

  return (
    <div className={calculatedClassName}>
      <RegistrationForm
        login={state.login}
        email={state.email}
        password={state.password}
        confirmPassword={state.confirmPassword}
        validation={validation}
        isRegistering={isRegistering}
        onChange={onChange}
        onSubmit={register}
        onCancel={goBack}
        onValidate={onValidate}
      />
    </div>
  )
};

RegistrationPage.propTypes = {
  // location: PropTypes.object,
  isLogged: PropTypes.bool,
  isRegistering: PropTypes.bool,
  register: PropTypes.func,
  goBack: PropTypes.func,
  // push: PropTypes.func,
  replace: PropTypes.func,
};

RegistrationPage.defaultProps = {
  // location: {},
  isLogged: false,
  isRegistering: false,
  register: () => {},
  goBack: () => {},
  // push: () => {},
  replace: () => {},
};

export { RegistrationPage };
