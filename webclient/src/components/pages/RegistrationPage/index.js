import { connect } from 'react-redux';
import { replace, push, goBack } from 'connected-react-router';

import { RegistrationPage } from './RegistrationPage';
import { isLoggedSelector, isRegisteringSelector, /* locationSelector */ } from '../../../store/selectors';
import { register } from '../../../network';

const mapStateToProps = state => ({
  // login: '',
  // password: '',
  // email: '',
  // confirmPassword: '',
  isLogged: isLoggedSelector(state),
  isRegistering: isRegisteringSelector(state),
  // location: locationSelector(state),
});

const mapDispatchToProps = {
  replace,
  push,
  goBack,
  register,
};

export default connect(mapStateToProps, mapDispatchToProps)(RegistrationPage);
