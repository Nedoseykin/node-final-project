import { connect } from 'react-redux';
import { HomePage } from './HomePage';
import { homeContentSelector } from '../../../store/selectors';

const mapStateToProps = state => ({
  homeContent: homeContentSelector(state),
});

export default connect(mapStateToProps)(HomePage);
