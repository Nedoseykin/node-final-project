import React from 'react';
import * as PropTypes from 'prop-types';

const HomePage = ({ homeContent: HomeContent }) => (
  <div className="PageContent">
    {
      HomeContent && ( <HomeContent /> )
    }
  </div>
);

HomePage.propTypes = {
  homeContent: PropTypes.any,
};

HomePage.defaultProps = {
  homeContent: null,
};

export { HomePage };
