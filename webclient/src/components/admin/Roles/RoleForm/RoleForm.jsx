import React from 'react';
import * as PropTypes from 'prop-types';
import { get } from 'lodash';

import { BasicInput, TogglerSet } from '../../../controls';

import { PRIVILEGES } from '../../../../domain/privileges';

import styles from './RoleForm.module.scss';

const RoleForm = ({
  role,
  editMode,
  onChange,
}) => {
  const name = get(role, 'name', '');
  const privileges = get(role, 'privileges', '');

  const privilegesItems = PRIVILEGES
    .map((p) => {
      const { id: itemId, label, letter, items } = p;

      return (
        <TogglerSet
          key={itemId}
          classes={{
            root: styles.TogglerSet__root,
            content: styles.TogglerSet__content,
          }}
          label={label}
          name="privileges"
          value={privileges}
          items={
            items.map(item => ({ index: item.index, letter, label: item.prefix }))
          }
          onChange={onChange}
          disabled={!editMode}
        />
      )
    });

  return (
    <div className={styles.RoleForm}>
      {
        role && (
          <form name="roleForm">
            <div key="name" className={styles.row}>
              <label htmlFor="name">Name</label>
              <BasicInput
                id="name"
                name="name"
                value={name}
                disabled={!editMode}
                onChange={onChange}
              />
            </div>

            <div key="privileges" className={styles.row}>
              {privilegesItems}
            </div>
          </form>
        )
      }
    </div>
  )
};

RoleForm.propTypes = {
  name: PropTypes.string,
  privileges: PropTypes.number,
};

RoleForm.defaultProps = {
  name: '',
  privileges: 0,
};

export { RoleForm };
