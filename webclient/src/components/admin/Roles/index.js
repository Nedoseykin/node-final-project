import { connect } from 'react-redux';

import { AdminRoles } from './Roles';

import { loadEntity } from '../../../network';
import {
  adminActiveEntityItemIdSelector,
  authTokenSelector,
  rolesLoadStatusSelector,
  rolesSelector,
  roleSelector,
  editModeSelector,
} from '../../../store/selectors';

import {
  reloadEntity,
  closeSection,
  entityItemSelect,
  newRecord,
  editRecord,
  cancelEditRecord,
  saveRecord,
  onRecordChange,
} from '../../../store/dispatchers';

import { ROLES } from '../../../domain/model/entityId';

const onRoleSelect = id => (dispatch) => {
  entityItemSelect(ROLES, id)(dispatch);
};

const saveRoleRecord = (token, role) => (dispatch) => {
  saveRecord(token, ROLES, role)(dispatch);
};

const mapStateToProps = state => ({
  rolesLoadStatus: rolesLoadStatusSelector(state),
  roles: rolesSelector(state),
  activeRoleId: adminActiveEntityItemIdSelector(state),
  selectedRole: roleSelector(state),

  editMode: editModeSelector(state),

  authToken: authTokenSelector(state),
});

const mapDispatchToProps = {
  closeSection,
  reloadEntity,
  loadEntity,
  newRecord,
  editRecord,
  cancelEditRecord,
  onRecordChange,
  onRoleSelect,
  saveRecord: saveRoleRecord,
};

export default connect(mapStateToProps, mapDispatchToProps)(AdminRoles);
