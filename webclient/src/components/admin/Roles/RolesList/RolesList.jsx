import React from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';

import styles from './RolesList.module.scss';
import { BasicList, BasicListItem } from '../../../controls/basic';
import { isFilledArray } from '../../../../utils';

const RolesList = ({
  items,
  value,
  onItemClick,
  onItemDoubleClick,
  onWheelLimit,
}) => {
  const rolesListItems = isFilledArray(items)
    ? items.map((item) => {
      const { name, id } = item;
      return (
        <BasicListItem
          key={id}
          className={classNames(styles.RolesListItem)}
          value={id}
          isSelected={value === id}
          disabled={false}
          onClick={() => { onItemClick(id) }}
          onDoubleClick={() => { onItemDoubleClick(id) }}
        >
          {name}
        </BasicListItem>
      )
    })
    : [];

  return (
    <BasicList
      className={styles.RolesList}
      onWheelLimit={onWheelLimit}
    >
      {rolesListItems}
    </BasicList>
  )
};

RolesList.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({})),
  value: PropTypes.number,
  onItemClick: PropTypes.func,
  onItemDoubleClick: PropTypes.func,
  onWheelLimit: PropTypes.func,
};

RolesList.defaultProps = {
  items: [],
  value: -1,
  onItemClick: () => {},
  onItemDoubleClick: () => {},
  onWheelLimit: () => {},
};

export { RolesList };
