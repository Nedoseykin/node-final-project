import React, { PureComponent } from 'react';
import * as PropTypes from 'prop-types';
// import classNames from 'classnames';

import {
  AppContent,
  PageContentHeader,
  HeaderTitle,
  HeaderToolbar,
  ToolbarButton,
  PageContentRoot,
  Aside,
  CentralSection,
} from '../../AppContent';

import RolesList from '../Roles/RolesList';
import RoleForm from '../Roles/RoleForm';

import { ROLES } from '../../../domain/model/entityId';
import { isExists, isFilledArray } from '../../../utils';

class AdminRoles extends PureComponent {
  static propTypes = {
    rolesLoadStatus: PropTypes.oneOf([0, 1, 2, 3]),
    roles: PropTypes.arrayOf(PropTypes.shape({})),
    activeRoleId: PropTypes.number,
    selectedRole: PropTypes.shape({}),
    onRoleSelect: PropTypes.func,

    authToken: PropTypes.string,
    editMode: PropTypes.bool,

    closeSection: PropTypes.func,
    reloadEntity: PropTypes.func,
    loadEntity: PropTypes.func,

    newRecord: PropTypes.func,
    editRecord: PropTypes.func,
    cancelEditRecord: PropTypes.func,
    onRecordChange: PropTypes.func,
    saveRecord: PropTypes.func,
  };

  static defaultProps = {
    rolesLoadStatus: 0,
    roles: null,
    activeRoleId: -1,
    selectedRole: null,
    onRoleSelect: () => {},

    authToken: '',
    editMode: false,

    closeSection: () => {},
    reloadEntity: () => {},
    loadEntity: () => {},

    newRecord: () => {},
    editRecord: () => {},
    cancelEditRecord: () => {},
    onRecordChange: () => {},
    saveRecord: () => {},
  };

  componentDidMount() {
    const { reloadEntity } = this.props;

    [ROLES]
      .forEach((entityName) => {
        const { [`${entityName}LoadStatus`]: loadStatus } = this.props;
        if (!isExists(loadStatus) || (loadStatus > 1)) {
          reloadEntity(entityName);
        }
      });

    this.updateData();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    this.updateData();
  }

  updateData = () => {
    const { authToken, loadEntity, reloadEntity } = this.props;

    [ROLES]
      .forEach((entityName) => {
        const {
          [`${entityName}LoadStatus`]: loadStatus,
          [entityName]: entity,
        } = this.props;

        if (loadStatus === 0) {
          loadEntity(entityName, authToken);
        }

        if ((loadStatus === 2) && !isExists(entity)) {
          reloadEntity(entityName);
        }
      });
  };

  onSaveRecord = () => {
    const {
      saveRecord,
      authToken,
      selectedRole,
    } = this.props;
    if (selectedRole) {
      saveRecord(authToken, selectedRole);
    }
  };

  render() {
    const {
      roles,
      activeRoleId,
      selectedRole,
      onRoleSelect,

      editMode,

      newRecord,
      editRecord,
      cancelEditRecord,
      closeSection,
      reloadEntity,
      onRecordChange,
    } = this.props;

    return (
      <AppContent>
        <PageContentHeader>
          <HeaderTitle title="Roles" />

          <HeaderToolbar>
            <ToolbarButton
              key="btnNew"
              condition={!editMode}
              onClick={newRecord}
            >
              New
            </ToolbarButton>

            <ToolbarButton
              key="btnEdit"
              condition={!editMode && isExists(selectedRole)}
              onClick={() => { editRecord(selectedRole) }}
            >
              Edit
            </ToolbarButton>

            <ToolbarButton
              key="btnSave"
              condition={editMode}
              onClick={this.onSaveRecord}
            >
              Save
            </ToolbarButton>

            <ToolbarButton
              key="btnCancel"
              condition={editMode}
              onClick={cancelEditRecord}
            >
              Cancel
            </ToolbarButton>

            <ToolbarButton
              key="btnReload"
              onClick={() => { reloadEntity(ROLES); }}
            >
              Reload
            </ToolbarButton>

            <ToolbarButton
              key="btnClose"
              onClick={closeSection}
            >
              Close
            </ToolbarButton>
          </HeaderToolbar>
        </PageContentHeader>

        <PageContentRoot>
          <Aside>
            {
              isFilledArray(roles) && (
                <RolesList
                  value={activeRoleId}
                  items={roles}
                  onItemClick={onRoleSelect}
                />
              )
            }
          </Aside>

          <CentralSection>
            <RoleForm
              editMode={editMode}
              role={selectedRole}
              onChange={onRecordChange}
            />
          </CentralSection>
        </PageContentRoot>
      </AppContent>
    );
  }
}

export { AdminRoles };
