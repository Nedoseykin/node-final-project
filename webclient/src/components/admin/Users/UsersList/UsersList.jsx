import React from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';

import styles from './UsersList.module.scss';

import { BasicList, BasicListItem } from '../../../controls/basic';
import { isFilledArray } from '../../../../utils';

const UsersList = ({
  items,
  value,
  onItemClick,
  onItemDoubleClick,
  onWheelLimit,
}) => {
  const usersListItems = isFilledArray(items)
    ? items.map((item) => {
      const { login, id, is_active } = item;
      return (
        <BasicListItem
          key={id}
          className={classNames(styles.UsersListItem, !is_active && styles.itemInactive)}
          value={id}
          isSelected={value === id}
          disabled={false}
          onClick={() => { onItemClick(id) }}
          onDoubleClick={() => { onItemDoubleClick(id) }}
        >
          {login}
        </BasicListItem>
      )
    })
    : [];

  return (
    <BasicList
      className={styles.UsersList}
      onWheelLimit={onWheelLimit}
    >
      { usersListItems }
    </BasicList>
  )
};

UsersList.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({})),
  value: PropTypes.number,
  onItemClick: PropTypes.func,
  onItemDoubleClick: PropTypes.func,
  onWheelLimit: PropTypes.func,
};

UsersList.defaultProps = {
  items: [],
  value: -1,
  onItemClick: () => {},
  onItemDoubleClick: () => {},
  onWheelLimit: () => {},
};

export { UsersList };
