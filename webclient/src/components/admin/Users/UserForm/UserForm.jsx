import React from 'react';
import * as PropTypes from 'prop-types';
import { get } from 'lodash';

import styles from './UserForm.module.scss';

import { BasicInput, BasicSelect } from '../../../controls/basic';

const UserForm = ({
  user,
  roles,
  editMode,
  onChange,
  onUnlock,
  onChangePassword,
}) => {
  const login = get(user, 'login', '');
  const email = get(user, 'email', '');
  const roleId = get(user, 'role_id', -1);
  const created = get(user, 'created_ts', '');
  const updated = get(user, 'updated_ts', '');
  const failsCount = get(user, 'fails_count', 0);
  const isActive = get(user, 'is_active', 1);

  return (
    <div className={styles.UserForm}>
      {
        user && (
          <form name="userForm">
            <div key="login" className={styles.row}>
              <label htmlFor="login">Login</label>
              <BasicInput
                id="login"
                name="login"
                value={login}
                disabled={!editMode}
                onChange={onChange}
              />
            </div>
            <div key="email" className={styles.row}>
              <label htmlFor="email">E-mail</label>
              <BasicInput
                id="email"
                name="email"
                value={email}
                disabled={!editMode}
                onChange={onChange}
              />
            </div>
            <div key="roleSelect" className={styles.row}>
              <label htmlFor="role_id">Role in select</label>
              <BasicSelect
                id="role_id"
                name="role_id"
                className={styles.Select}
                items={roles}
                value={roleId}
                asId="id"
                asLabel="name"
                disabled={!editMode}
                onChange={onChange}
              />
            </div>
            <div key="created" className={styles.row}>
              <label htmlFor="created">Created</label>
              <BasicInput
                id="created"
                name="created"
                value={created}
                disabled={true}
              />
            </div>
            <div key="updated" className={styles.row}>
              <label htmlFor="updated">Updated</label>
              <BasicInput
                id="updated"
                name="updated"
                value={updated}
                disabled={true}
              />
            </div>
            <div key="failsCount" className={styles.row}>
              <label htmlFor="failsCount">Login fails count</label>
              <BasicInput
                id="failsCount"
                name="failsCount"
                value={`${failsCount}`}
                disabled={true}
              />
            </div>
            <div key="isActive" className={styles.row}>
              <label htmlFor="isActive">Is active</label>
              <BasicInput
                id="isActive"
                name="isActive"
                value={isActive === 1 ? 'Yes' : 'No'}
                disabled={true}
              />
            </div>
            <div key="toolbar" className={styles.toolbar}>
              {
                isActive === 0 && (
                  <button
                    id="btnUnblock"
                    type="button"
                    className={styles.toolbarButton}
                    onClick={() => { onUnlock && onUnlock(user) }}
                  >
                    Unlock
                  </button>
                )
              }
              <button
                id="btnChangePassword"
                type="button"
                className={styles.toolbarButton}
                onClick={onChangePassword}
              >
                Change password
              </button>
            </div>
          </form>
        )
      }
      {
        !user && (
          <span className={styles.emptyMessage}>
            No user is selected
          </span>
        )
      }
    </div>
  )
};

UserForm.propTypes = {
  user: PropTypes.shape({}),
  roles: PropTypes.arrayOf(PropTypes.shape({})),
  editMode: PropTypes.bool,
  onChange: PropTypes.func,
  onUnlock: PropTypes.func,
  onChangePassword: PropTypes.func,
};

UserForm.defaultProps = {
  user: null,
  roles: [],
  editMode: false,
  onChange: () => {},
  onUnlock: () => {},
  onChangePassword: () => {},
};

export { UserForm };
