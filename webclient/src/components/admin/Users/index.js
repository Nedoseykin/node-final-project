import { connect } from 'react-redux';

import { AdminUsers } from './Users';

import {
  adminActiveEntityItemIdSelector,
  authTokenSelector,
  editModeSelector,
  rolesLoadStatusSelector,
  rolesSelector,
  userSelector,
  usersLoadStatusSelector,
  usersSelector,
} from '../../../store/selectors';

import {
  loadEntity,
  unlockUser,
} from '../../../network';

import { USERS } from '../../../domain/model/entityId';

import {
  reloadEntity,
  closeSection,
  editRecord,
  cancelEditRecord,
  entityItemSelect,
  onRecordChange,
  saveRecord,
  showChangePasswordDialog,
} from '../../../store/dispatchers';

const onUserSelect = id => (dispatch) => {
  entityItemSelect(USERS, id)(dispatch);
};

const saveUserRecord = (token, user) => (dispatch) => {
  saveRecord(token, USERS, user)(dispatch);
};

const mapStateToProps = state => ({
  usersLoadStatus: usersLoadStatusSelector(state),
  users: usersSelector(state),
  activeUserId: adminActiveEntityItemIdSelector(state),
  selectedUser: userSelector(state),

  rolesLoadStatus: rolesLoadStatusSelector(state),
  roles: rolesSelector(state),

  authToken: authTokenSelector(state),
  editMode: editModeSelector(state),
});

const mapDispatchToProps = {
  closeSection,
  loadEntity,
  reloadEntity,
  onUserSelect,
  editRecord,
  cancelEditRecord,
  onRecordChange,
  unlockUser,
  saveRecord: saveUserRecord,
  showChangePasswordDialog,
};

export default connect(mapStateToProps, mapDispatchToProps)(AdminUsers);
