import React from 'react';

import ChangePasswordDialog from '../../../modal/ChangePasswordDialog';
import * as PropTypes from 'prop-types';

const AdminChangePasswordDialog = ({
  userLogin,
  onSubmit,
}) => {
  return (
    <ChangePasswordDialog
      userLogin={userLogin}
      onSubmit={onSubmit}
    />
  )
};

AdminChangePasswordDialog.propTypes = {
  userLogin: PropTypes.string,
  onSubmit: PropTypes.func,
};

AdminChangePasswordDialog.defaultProps = {
  userLogin: '',
  onSubmit: () => {},
};

export { AdminChangePasswordDialog };
