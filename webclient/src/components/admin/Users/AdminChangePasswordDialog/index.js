import { connect } from 'react-redux';

import { AdminChangePasswordDialog } from './AdminChangePasswordDialog';
import { userPasswordChange } from '../../../../network';
import { userLoginAdminPasswordChangeSelector } from '../../../../store/selectors';

const mapStateToProps = state => ({
  userLogin: userLoginAdminPasswordChangeSelector(state),
});

const mapDispatchToProps = {
  onSubmit: userPasswordChange,
};

export default connect(mapStateToProps, mapDispatchToProps)(AdminChangePasswordDialog);
