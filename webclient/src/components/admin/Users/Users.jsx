import React, { PureComponent } from 'react';
import * as PropTypes from 'prop-types';

import {
  AppContent,
  PageContentHeader,
  HeaderTitle,
  HeaderToolbar,
  ToolbarButton,
  PageContentRoot,
  Aside,
  CentralSection,
} from '../../AppContent';
import UsersList from './UsersList';
import UserForm from './UserForm';
import AdminChangePasswordDialog from './AdminChangePasswordDialog';

import { isExists, isFilledArray } from '../../../utils';
import { ROLES, USERS } from '../../../domain/model/entityId';

class AdminUsers extends PureComponent {
  static propTypes = {
    usersLoadStatus: PropTypes.oneOf([0, 1, 2, 3]),
    users: PropTypes.arrayOf(PropTypes.shape({})),
    activeUserId: PropTypes.number,
    selectedUser: PropTypes.shape({}),
    onUserSelect: PropTypes.func,
    unlockUser: PropTypes.func,
    saveRecord: PropTypes.func,
    showChangePasswordDialog: PropTypes.func,

    rolesLoadStatus: PropTypes.oneOf([0, 1, 2, 3]),
    roles: PropTypes.arrayOf(PropTypes.shape({})),

    authToken: PropTypes.string,
    editMode: PropTypes.bool,

    loadEntity: PropTypes.func,
    reloadEntity: PropTypes.func,
    closeSection: PropTypes.func,

    editRecord: PropTypes.func,
    cancelEditRecord: PropTypes.func,
    onRecordChange: PropTypes.func,
  };

  static defaultProps = {
    usersLoadStatus: 0,
    users: null,
    activeUserId: -1,
    selectedUser: null,
    onUserSelect: () => {},
    unlockUser: () => {},
    saveRecord: () => {},
    showChangePasswordDialog: () => {},

    rolesLoadStatus: 0,
    roles: null,

    authToken: '',
    editMode: false,

    loadEntity: () => {},
    reloadEntity: () => {},
    closeSection: null,

    editRecord: () => {},
    cancelEditRecord: () => {},
    onRecordChange: () => {},
  };

  componentDidMount() {
    const { reloadEntity } = this.props;

    [USERS, ROLES]
      .forEach((entityName) => {
        const { [`${entityName}LoadStatus`]: loadStatus } = this.props;
        if (!isExists(loadStatus) || (loadStatus > 1)) {
          reloadEntity(entityName);
        }
      });

    this.updateData();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    this.updateData();
  }

  updateData = () => {
    const { authToken, loadEntity, reloadEntity } = this.props;

    [USERS, ROLES]
      .forEach((entityName) => {
        const {
          [`${entityName}LoadStatus`]: loadStatus,
          [entityName]: entity,
        } = this.props;

        if (loadStatus === 0) {
          loadEntity(entityName, authToken);
        }

        if ((loadStatus === 2) && !isExists(entity)) {
          reloadEntity(entityName);
        }
      });
  };

  onUnlockUser = () => {
    const {
      unlockUser,
      authToken,
      selectedUser,
    } = this.props;
    if (selectedUser && isExists(selectedUser.id)) {
      unlockUser(authToken, selectedUser.id);
    }
  };

  onSaveRecord = () => {
    const {
      saveRecord,
      authToken,
      selectedUser,
    } = this.props;
    if (selectedUser && isExists(selectedUser.id)) {
      saveRecord(authToken, selectedUser);
    }
  };

  render() {
    const {
      users,
      activeUserId,
      selectedUser,
      onUserSelect,
      roles,
      reloadEntity,
      closeSection,

      editMode,

      editRecord,
      cancelEditRecord,
      onRecordChange,
      showChangePasswordDialog,
    } = this.props;

    /*
    * <ToolbarButton
     key="btnNew"
     condition={!editMode}
     onClick={null}
     >
     New
     </ToolbarButton>
    * */

    return (
      <AppContent>
        <PageContentHeader>
          <HeaderTitle title="Users" />

          <HeaderToolbar>
            <ToolbarButton
              key="btnEdit"
              condition={!editMode && isExists(selectedUser)}
              onClick={() => { editRecord(selectedUser) }}
            >
              Edit
            </ToolbarButton>

            <ToolbarButton
              key="btnSave"
              condition={editMode}
              onClick={this.onSaveRecord}
            >
              Save
            </ToolbarButton>

            <ToolbarButton
              key="btnCancel"
              condition={editMode}
              onClick={cancelEditRecord}
            >
              Cancel
            </ToolbarButton>

            <ToolbarButton
              key="btnReload"
              onClick={() => { reloadEntity(USERS) }}
            >
              Reload
            </ToolbarButton>

            <ToolbarButton
              key="btnClose"
              onClick={closeSection}
            >
              Close
            </ToolbarButton>
          </HeaderToolbar>
        </PageContentHeader>

        <PageContentRoot>
          <Aside>
            {
              isFilledArray(users) && (
                <UsersList
                  value={activeUserId}
                  items={users}
                  onItemClick={onUserSelect}
                />
              )
            }
          </Aside>

          <CentralSection>
            <UserForm
              editMode={editMode}
              user={selectedUser}
              roles={roles}
              onChange={onRecordChange}
              onUnlock={this.onUnlockUser}
              onChangePassword={showChangePasswordDialog}
            />
          </CentralSection>

          <AdminChangePasswordDialog />
        </PageContentRoot>
      </AppContent>
    );
  }
}

export { AdminUsers };