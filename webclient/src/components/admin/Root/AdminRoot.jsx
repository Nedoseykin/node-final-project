import React from 'react';

import styles from './AdminRoot.module.scss';

const AdminRoot = ({ entities, setAdminMode }) => {
  const items = entities.map((item) => {
    const { id, label } = item;
    return (
      <button
        key={id}
        className={styles.Entity__btn}
        onClick={() => { setAdminMode(id); }}
      >
        {label}
      </button>
    )
  });

  return (
    <div className={styles.AdminRoot}>
      {items}
    </div>
  )
};

export { AdminRoot };
