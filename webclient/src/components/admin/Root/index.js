import { connect } from 'react-redux';

import { AdminRoot } from './AdminRoot';
import { adminRootEntitiesSelector } from '../../../store/selectors';
import ENTITIES from '../../../domain/model/entities';
import { PAGES } from '../../../constants';
import { push } from "connected-react-router";

const setAdminMode = adminMode => (dispatch) => {
  const { pageId } = ENTITIES[adminMode];
  const { pathName } = PAGES[pageId];
  dispatch(push(pathName));
};

const mapStateToProps = state => ({
  entities: adminRootEntitiesSelector(state),
});

const mapDispatchToProps = {
  setAdminMode,
};

export default connect(mapStateToProps, mapDispatchToProps)(AdminRoot);
