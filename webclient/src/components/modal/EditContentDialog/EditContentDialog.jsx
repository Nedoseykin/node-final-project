import React, { useEffect } from 'react';
import * as PropTypes from 'prop-types';
import { get } from 'lodash';
import classNames from 'classnames';

import styles from './EditContentDialog.module.scss';

import { BasicModal } from '../index'
import { ToolbarButton } from '../../AppContent';
import * as Controls from '../../controls';

import PUBLICATIONS from '../../../domain/publications/publications';

const EditContentDialog = ({
  contentId,
  contentSrc,
  authToken,
  isVisible,
  componentId,
  componentIdItems,
  component: SelectedComponent,
  propId,
  propIdItems,
  propValues,
  onLoadContent,
  onClose,
  onChange,
  onSave,
  onContentIdChange,
}) => {
  useEffect(() => {
    if (!contentSrc && contentId > 0) {
      onLoadContent({ authToken, contentId });
    }
  });

  const handleSave = () => {
    const props = propValues
      ? JSON.stringify(propValues)
      : JSON.stringify({text: '', textPosition: '', imageSrc: ''});

    const payload = {
      id: contentId, props, component: componentId,
    };
    onSave && onSave(payload, authToken, onContentIdChange);
  };

  const handleSelectChange = (e) => {
    const name = e.target.name;
    const value = e.value;
    onChange && onChange({ target: { name, value } });
  };

  const handleChangeProp = (e) => {
    const propValue = e.value || e.target.value;
    const { name: propName } = e.target;
    const model = { [propName]: propValue };
    const newPropValues = propValues
      ? { ...propValues, ...model }
      : model;
    onChange && onChange({ target: { name: 'propValues', value: newPropValues } });
  };

  const getComponentConfig = () => {
    return componentId && propId
      ? PUBLICATIONS[componentId].props[propId]
      : '';
  };

  const getPropComponent = () => {
    const componentConfig = getComponentConfig();
    const controlId = get(componentConfig, 'component', '');
    const controlProps = get(componentConfig, 'props', {});
    const Control = controlId ? Controls[controlId] : null;
    return Control && (
      <Control
        {...controlProps}
        id={propId}
        name={propId}
        value={(propValues && propValues[propId]) || ''}
        onChange={handleChangeProp}
      />
    );
  };

  return isVisible && (
    <BasicModal
      className={styles.EditContentDialog}
      onClick={(e) => {
        e.stopPropagation();
        onClose && onClose();
      }}
    >
      <div
        className={styles.editor}
        onClick={(e) => {
          e.stopPropagation();
        }}
      >
        <div key="header" className={styles.header}>
          <div key="title" className={styles.headerTitle}>
            Content editor
          </div>
          <div key="headerToolbar" className={styles.headerToolbar}>
            <span key="componentId" className={styles.headerToolbarControl}>
              <label htmlFor="componentId">Component</label>
              <Controls.BasicSelect
                id="componentId"
                name="componentId"
                items={componentIdItems}
                value={componentId}
                onChange={handleSelectChange}
              />
            </span>

            {
              componentId && (
                <span key="propNameSelector" className={styles.headerToolbarControl}>
                  <label htmlFor="propId">Prop</label>
                  <Controls.BasicSelect
                    id="propId"
                    name="propId"
                    items={propIdItems}
                    value={propId}
                    onChange={handleSelectChange}
                  />
                </span>
              )
            }

            {
              propId && (
                <span
                  key="propValueEditor"
                  className={classNames(
                    styles.headerToolbarControl,
                    styles.propComponent,
                  )}
                >
                  <label htmlFor={propId}>Value</label>
                  { getPropComponent() }
                </span>
              )
            }
          </div>
        </div>

        <div key="centralSection" className={styles.centralSection}>
          {
            SelectedComponent
              ? <SelectedComponent
                  editMode={true}
                  data={propValues}
                />
              : 'No component is selected'
          }
        </div>

        <div key="toolbar" className={styles.toolbar}>
          <ToolbarButton
           key="btnCancel"
           onClick={onClose}
          >
            Cancel
          </ToolbarButton>
          <ToolbarButton
           key="btnSave"
           onClick={handleSave}
          >
            Save
          </ToolbarButton>
        </div>
      </div>
    </BasicModal>
  )
};

EditContentDialog.propTypes = {
  contentId: PropTypes.number,
  contentSrc: PropTypes.shape({}),
  authToken: PropTypes.string,
  isVisible: PropTypes.bool,
  componentId: PropTypes.string,
  componentIdItems: PropTypes.arrayOf(PropTypes.shape({})),
  component: PropTypes.any,
  propId: PropTypes.string,
  propIdItems: PropTypes.arrayOf(PropTypes.shape({})),
  propValues: PropTypes.shape({}),
  onChange: PropTypes.func,
  onSave: PropTypes.func,
  onClose: PropTypes.func,
  onLoadContent: PropTypes.func,
};

EditContentDialog.defaultProps = {
  contentId: -1,
  contentSrc: null,
  authToken: null,
  isVisible: false,
  componentId: '',
  componentIdItems: [],
  component: null,
  propId: '',
  propIdItems: [],
  propValues: {},
  onChange: () => {},
  onSave: () => {},
  onClose: () => {},
  onLoadContent: () => {},
};

export { EditContentDialog };
