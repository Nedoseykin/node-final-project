import { connect } from 'react-redux';
import { get } from 'lodash';

import PUBLICATIONS from '../../../domain/publications/publications';

import { EditContentDialog } from './EditContentDialog';
import {
  authTokenSelector,
  componentIdEditContentDialogSelector, contentIdEditContentDialogSelector, contentSrcEditContentDialogSelector,
  isVisibleEditContentDialogSelector,
  propIdEditContentDialogSelector, propValuesEditContentDialogSelector,
} from '../../../store/selectors';
import {
  acHideDialog,
  acModalChangeValue, acModalSetState,
} from '../../../store/actions';
import { EDIT_CONTENT_DIALOG } from '../../../domain/modals/modalsId';
import { isFilledString } from '../../../utils';
import { createEntity, request, updateEntity, URL } from '../../../network';
import { PERSONAL_NEWS_CONTENT } from '../../../domain/model/entityId';

const componentIdItems = PUBLICATIONS
  ? Object.keys(PUBLICATIONS)
    .map(id => ({ id, label: id }))
  : [];

const onClose = () => (dispatch) => {
  dispatch(acHideDialog({ modalId: EDIT_CONTENT_DIALOG }));
};

const onChange = e => (dispatch) => {
  const { name, value } = e.target;
  dispatch(acModalChangeValue({ modalId: EDIT_CONTENT_DIALOG, name, value }));
};

const onSetState = payload => (dispatch) => {
  dispatch(acModalSetState({
    modalId: EDIT_CONTENT_DIALOG,
    ...payload
  }));
};

const onSave = (props, token, onContentIdChange) => (dispatch) => {
  const operation = props.id > 0 ? updateEntity : createEntity;
  operation(
    PERSONAL_NEWS_CONTENT,
    token,
    props,
    (loadedData) => {
      if (loadedData && loadedData.id) {
        onContentIdChange(loadedData.id);
        onClose()(dispatch);
      }
    },
  )(dispatch);
};

const onLoadContent = ({ authToken, contentId }) => (dispatch) => {
  onChange({
    target: {
      name: 'contentSrc',
      value: request({
        operation: 'loading of news content',
        dispatch,
        url: `${URL.NEWS_CONTENT}/${contentId}`,
        headers: {
          'auth-token': authToken,
        },
        onError: (err) => { console.error(err); },
        onSuccess: (loadedData) => {
          onSetState({
            componentId: loadedData.component,
            propValues: loadedData.props,
            contentSrc: loadedData,
          })(dispatch);
        },
      }),
    },
  })(dispatch);
};

const mapStateToProps = state => {
  const componentId = componentIdEditContentDialogSelector(state);
  const propId = propIdEditContentDialogSelector(state);
  const component = isFilledString(componentId)
    ? get(PUBLICATIONS, [componentId, 'component'], null)
    : null;
  const componentProps = isFilledString(componentId)
    ? get(PUBLICATIONS, [componentId, 'props'], {})
    : null;
  const propIdItems = componentProps
    ? Object.keys(componentProps).map(id => ({ id, label: id }))
    : [];

  return {
    authToken: authTokenSelector(state),
    isVisible: isVisibleEditContentDialogSelector(state),
    contentId: contentIdEditContentDialogSelector(state),
    contentSrc: contentSrcEditContentDialogSelector(state),
    componentId,
    componentIdItems,
    component,
    propId,
    // componentProps,
    propIdItems,
    propValues: propValuesEditContentDialogSelector(state),
  }
};

const mapDispatchToProps = {
  onClose,
  onChange,
  onSave,
  onLoadContent,
};

export default connect(mapStateToProps, mapDispatchToProps)(EditContentDialog);
