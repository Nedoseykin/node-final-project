import React from 'react';
import * as PropTypes from 'prop-types';

import { BasicModal } from '../index';
import { ToolbarButton } from '../../AppContent';


import styles from './DeleteConfirmationDialog.module.scss';

const DeleteConfirmationDialog = ({
  isVisible,
  message,
  onClose,
  onDelete,
}) => {
  return isVisible && (
    <BasicModal
      className={styles.DeleteConfirmationDialog}
      onClick={(e) => {
        e.stopPropagation();
        onClose();
      }}
    >
      <form
        className={styles.contentBox}
        onClick={(e) => {
          e.stopPropagation();
        }}
      >
        <fieldset className={styles.content}>
          <legend className={styles.contentTitle}>
            Confirmation required
          </legend>
          <div className={styles.row}>
            {message}
          </div>
        </fieldset>
        <div className={styles.toolbar}>
          <ToolbarButton
            key="btnCancel"
            id="btnCancel"
            onClick={onClose}
          >
            Cancel
          </ToolbarButton>

          <ToolbarButton
            key="btnDelete"
            id="btnDelete"
            onClick={onDelete}
          >
            Delete
          </ToolbarButton>
        </div>
      </form>
    </BasicModal>
  )
};

DeleteConfirmationDialog.propTypes = {
  message: PropTypes.node,
  isVisible: PropTypes.bool,
  onClose: PropTypes.func,
  onDelete: PropTypes.func,
};

DeleteConfirmationDialog.defaultProps = {
  message: null,
  isVisible: false,
  onClose: () => {},
  onDelete: () => {},
};

export { DeleteConfirmationDialog };
