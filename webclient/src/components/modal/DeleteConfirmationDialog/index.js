import { connect } from 'react-redux';

import { DeleteConfirmationDialog } from './DeleteConfirmationDialog';
import { hideDeleteConfirmationDialog } from '../../../store/dispatchers';
import {
  deleteConfirmationDialogMessageSelector,
  deleteConfirmationDialogOnDeleteSelector,
  isVisibleDeleteConfirmationDialogSelector,
} from '../../../store/selectors';

const mapStateToProps = state => ({
  isVisible: isVisibleDeleteConfirmationDialogSelector(state),
  message: deleteConfirmationDialogMessageSelector(state),
  onDelete: deleteConfirmationDialogOnDeleteSelector(state),
});

const mapDispatchToProps = {
  onClose: hideDeleteConfirmationDialog,
};

export default connect(mapStateToProps, mapDispatchToProps)(DeleteConfirmationDialog);
