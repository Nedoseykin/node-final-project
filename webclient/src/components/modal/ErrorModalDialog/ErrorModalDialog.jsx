import React, { useEffect } from 'react';
import * as PropTypes from 'prop-types';

import { BasicModal } from '../index';
import { ToolbarButton } from '../../AppContent';

import styles from './EooroModalDialog.module.scss';

const ErrorModalDialog = ({
  isVisible,
  item,
  onClose,
}) => {
  const { message, status, dateTime } = item;

  useEffect(() => {
    const modalRoot = document.getElementById('modalRoot');
    if (modalRoot) {
      const btnOk = modalRoot.querySelector('#errorModalBtnOk');
      if (btnOk) btnOk.focus();
    }
  });

  return isVisible && (
    <BasicModal
      className={styles.ErrorModalDialog}
      onClick={(e) => {
        e.stopPropagation();
        onClose();
      }}
    >
      <form
        className={styles.contentBox}
        onClick={(e) => {
          e.stopPropagation();
        }}
      >
        <fieldset className={styles.content}>
          <legend className={styles.contentTitle}>
            Error
          </legend>
          <div key="message" className={styles.row}>
            {message}
          </div>
          <div key="metaInfo" className={styles.row}>
            <div className={styles.metaInfo}>
              <span key="status">
                Status: {status}
              </span>

              <span key="dateTime">
                Date: {dateTime}
              </span>
            </div>
          </div>
        </fieldset>
        <div className={styles.toolbar}>
          <ToolbarButton
            key="btnOk"
            id="errorModalBtnOk"
            tabIndex="1"
            onClick={onClose}
          >
            Ok
          </ToolbarButton>
        </div>
      </form>
    </BasicModal>
  );
};

ErrorModalDialog.propTypes = {
  message: PropTypes.node,
  isVisible: PropTypes.bool,
  onClose: PropTypes.func,
  onOk: PropTypes.func,
};

ErrorModalDialog.defaultProps = {
  message: null,
  isVisible: false,
  onClose: () => {},
  onOk: () => {},
};

export { ErrorModalDialog };
