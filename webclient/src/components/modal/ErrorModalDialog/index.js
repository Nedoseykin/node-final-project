import { connect } from 'react-redux';

import { ErrorModalDialog } from './ErrorModalDialog';
import { shiftErrors } from '../../../store/dispatchers';
import {
  isVisibleErrorMessageDialogSelector,
  itemErrorMessageDialogSelector,
} from '../../../store/selectors';

const mapStateToProps = state => ({
  isVisible: isVisibleErrorMessageDialogSelector(state),
  item: itemErrorMessageDialogSelector(state),
});

const mapDispatchToProps = {
  onClose: shiftErrors,
};

export default connect(mapStateToProps, mapDispatchToProps)(ErrorModalDialog);
