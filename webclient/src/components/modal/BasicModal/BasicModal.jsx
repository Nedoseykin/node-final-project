import React, { PureComponent } from 'react';
import ReactDom from 'react-dom';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';

import './BasicModal.scss';
import { isFilledString } from '../../../utils';

const modalRoot = document.getElementById('modalRoot');

class BasicModal extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    children: PropTypes.any,
    onClick: PropTypes.func,
  };

  static defaultProps = {
    className: '',
    children: [],
    onClick: () => {},
  };

  constructor(props) {
    super(props);
    this.el = document.createElement('div');
    modalRoot.appendChild(this.el);
  }

  componentWillUnmount() {
    modalRoot.removeChild(this.el);
  }

  render() {
    const { children, className, onClick } = this.props;
    const calculatedClassName = classNames(
      'BasicModal',
      isFilledString(className) && className,
    );

    return ReactDom.createPortal(
      <div
        className={calculatedClassName}
        onClick={onClick}
      >
        {children}
      </div>,
      this.el,
    );
  }
}

export { BasicModal };
