import { connect } from 'react-redux';

import { ChangePasswordDialog } from './ChangePasswordDialog';

import {
  authTokenSelector,
  confirmPasswordSelector,
  currentPasswordSelector,
  isVisibleChangePasswordDialogSelector,
  newPasswordSelector, validationChangePasswordSelector,
} from '../../../store/selectors';

import {
  acHideDialog,
  acModalChangeValue,
  acModalClearAllValues,
} from '../../../store/actions';

import { CHANGE_PASSWORD_DIALOG } from '../../../domain/modals/modalsId';

const onChange = e => (dispatch) => {
  const { name, value } = e.target;
  dispatch(acModalChangeValue({ modalId: CHANGE_PASSWORD_DIALOG, name, value }));
};

const onClose = () => (dispatch) => {
  dispatch(acHideDialog({ modalId: CHANGE_PASSWORD_DIALOG }));
};

const onClearAll = () => (dispatch) => {
  dispatch(acModalClearAllValues({ modalId: CHANGE_PASSWORD_DIALOG }));
};

const mapStateToProps = state => ({
  authToken: authTokenSelector(state),
  currentPassword: currentPasswordSelector(state),
  newPassword: newPasswordSelector(state),
  confirmPassword: confirmPasswordSelector(state),
  validation: validationChangePasswordSelector(state),
  isVisible: isVisibleChangePasswordDialogSelector(state),
});

const mapDispatchToProps = {
  onChange,
  onClose,
  onClearAll,
};

export default connect(mapStateToProps, mapDispatchToProps)(ChangePasswordDialog);
