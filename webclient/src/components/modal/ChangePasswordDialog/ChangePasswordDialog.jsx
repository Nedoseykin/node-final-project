import React from 'react';
import * as PropTypes from 'prop-types';

import styles from './ChangePasswordDialog.module.scss';

import { BasicModal } from '../index'
import { BasicInput } from '../../controls/basic';
import { focusById, isFilledString } from '../../../utils';

const ChangePasswordDialog = ({
  authToken,
  userLogin,
  currentPassword,
  newPassword,
  confirmPassword,
  validation,
  isVisible,
  onChange,
  onClose,
  onClearAll,
  onSubmit,
}) => {
  return isVisible && (
    <BasicModal
      className={styles.ChangePasswordDialog}
      onClick={onClose}
    >
      <form
        name="changePasswordForm"
        className={styles.ChangePasswordForm}
        onClick={(e) => {
          e.stopPropagation();
          e.preventDefault();
        }}
      >
        <fieldset className={styles.content}>
          <legend className={styles.contentTitle}>
            Change password
          </legend>
          <div key="currentPassword" className={styles.row}>
            <label htmlFor="currentPassword">Current password</label>
            <BasicInput
              id="currentPassword"
              name="currentPassword"
              tabIndex="1"
              type="password"
              autoComplete="off"
              autoFocus
              value={currentPassword || ''}
              validationMessage={validation.currentPassword || ''}
              change={onChange}
              onEnterPress={() => focusById('newPassword')}
            />
          </div>
          <div key="newPassword" className={styles.row}>
            <label htmlFor="newPassword">New password</label>
            <BasicInput
              id="newPassword"
              name="newPassword"
              tabIndex="2"
              type="password"
              autoComplete="off"
              autoFocus
              value={newPassword || ''}
              validationMessage={validation.newPassword || ''}
              change={onChange}
              onEnterPress={() => focusById('confirmPassword')}
            />
          </div>
          <div key="confirmPassword" className={styles.row}>
            <label htmlFor="confirmPassword">Confirm password</label>
            <BasicInput
              id="confirmPassword"
              name="confirmPassword"
              tabIndex="3"
              type="password"
              autoComplete="off"
              autoFocus
              value={confirmPassword || ''}
              validationMessage={validation.confirmPassword || ''}
              change={onChange}
              onEnterPress={() => focusById('btnSubmit')}
            />
          </div>
        </fieldset>
        <div className={styles.toolbar}>
          <button
            key="btnCancel"
            id="btnCancel"
            className={styles.toolbarButton}
            type="button"
            onClick={onClose}
          >
            Cancel
          </button>
          <button
            key="btnClearAll"
            id="btnClearAll"
            className={styles.toolbarButton}
            type="button"
            onClick={onClearAll}
          >
            Clear all
          </button>
          <button
            key="btnSubmit"
            id="btnSubmit"
            className={styles.toolbarButton}
            type="button"
            onClick={() => {
              if (
                isFilledString(currentPassword)
                && isFilledString(newPassword)
                && isFilledString(confirmPassword)
                && (newPassword === confirmPassword)
              ) {
                onSubmit && onSubmit(
                  authToken,
                  {
                    login: userLogin,
                    currentPassword,
                    newPassword,
                    confirmPassword,
                  },
                )
              }
            }}
          >
            Submit
          </button>
        </div>
      </form>
    </BasicModal>
  );
};

ChangePasswordDialog.propTypes = {
  userLogin: PropTypes.string,
  authToken: PropTypes.string,
  currentPassword: PropTypes.string,
  newPassword: PropTypes.string,
  confirmPassword: PropTypes.string,
  validation: PropTypes.shape({}),
  isVisible: PropTypes.bool,
  onChange: PropTypes.func,
  onClose: PropTypes.func,
};

ChangePasswordDialog.defaultProps = {
  userLogin: '',
  authToken: '',
  currentPassword: '',
  newPassword: '',
  confirmPassword: '',
  validation: {},
  isVisible: false,
  onChange: () => {},
  onClose: () => {},
  onClearAll: () => {},
  onSubmit: () => {},
};

export { ChangePasswordDialog };
