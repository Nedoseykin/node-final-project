import React from 'react';
import * as PropTypes from 'prop-types';

import styles from './PersonalRoot.module.scss';

const PersonalRoot = ({
  entities,
  setPersonalMode,
}) => {
  const items = entities.map((item) => {
    const { id, label } = item;
    return (
      <button
        key={id}
        className={styles.Entity__btn}
        onClick={() => { setPersonalMode(id); }}
      >
        {label}
      </button>
    );
  });

  return (
    <div className={styles.PersonalRoot}>
      {items}
    </div>
  );
};

PersonalRoot.propTypes = {
  entities: PropTypes.arrayOf(PropTypes.shape({})),
  setPersonalMode: PropTypes.func,
};

PersonalRoot.defaultProps = {
  entities: [],
  setPersonalMode: () => {},
};

export { PersonalRoot };
