import { connect } from 'react-redux';
import { push } from "connected-react-router";

import { PersonalRoot } from './PersonalRoot';
import { personalRootEntitiesSelector } from '../../../store/selectors';
import ENTITIES from '../../../domain/model/entities';
import { PAGES } from '../../../constants';

const setPersonalMode = personalMode => (dispatch) => {
  const { pageId } = ENTITIES[personalMode];
  const { pathName } = PAGES[pageId];
  dispatch(push(pathName));
};

const mapStateToProps = state => ({
  entities: personalRootEntitiesSelector(state),
});

const mapDispatchToProps = {
  setPersonalMode,
};

export default connect(mapStateToProps, mapDispatchToProps)(PersonalRoot);
