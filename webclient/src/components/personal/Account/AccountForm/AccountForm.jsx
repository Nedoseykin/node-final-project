import React from 'react';
import * as PropTypes from 'prop-types';
import { get } from 'lodash';

import { BasicInput } from '../../../controls/basic';
import { ToolbarButton } from '../../../AppContent';

import styles from './AccountForm.module.scss';
import { focusById } from '../../../../utils';

const AccountForm = ({
  account,
  editMode,
  onChange,
  onChangePassword,
}) => {
  const login = get(account, 'login', '');
  const email = get(account, 'email', '');
  const roleId = get(account, 'role_id', 3);

  const hasNotActivated = roleId === 3;

  return (
    <form
      className={styles.AccountForm}
      name="accountForm"
    >
      <fieldset className={styles.content}>
        <legend className={styles.contentTitle}>
          Account
        </legend>

        <div key="login" className={styles.row}>
          <label htmlFor="login">Login</label>
          <BasicInput
            id="login"
            name="login"
            value={login}
            disabled={!editMode}
            onChange={onChange}
            onEnterPress={focusById('email')}
          />
        </div>

        <div key="email" className={styles.row}>
          <label htmlFor="email">E-mail</label>
          <BasicInput
            id="email"
            name="email"
            value={email}
            disabled={!editMode}
            onChange={onChange}
          />
        </div>

        {
          hasNotActivated && (
            <div key="isActive" className={styles.row}>
              <label>Is not activated</label>
            </div>
          )
        }
      </fieldset>
      <div className={styles.toolbar}>
        <ToolbarButton
          id="btnChangePassword"
          onClick={onChangePassword}
          style={{ width: '10rem' }}
        >
          Change password
        </ToolbarButton>
      </div>
    </form>
  );
};

AccountForm.propTypes = {
  account: PropTypes.shape({}),
  editMode: PropTypes.bool,
  onChange: PropTypes.func,
  onChangePassword: PropTypes.func,
};

AccountForm.defaultProps = {
  account: null,
  editMode: false,
  onChange: () => {},
  onChangePassword: () => {},
};

export { AccountForm };
