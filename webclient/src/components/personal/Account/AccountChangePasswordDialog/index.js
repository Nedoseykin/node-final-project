import { connect } from 'react-redux';

import { AccountChangePasswordDialog } from './AccountChangePasswordDialog';
import { accountPasswordChange } from '../../../../network';
import { authLoginSelector } from '../../../../store/selectors';

const mapStateToProps = state => ({
  userLogin: authLoginSelector(state),
});

const mapDispatchToProps = {
  onSubmit: accountPasswordChange,
};

export default connect(mapStateToProps, mapDispatchToProps)(AccountChangePasswordDialog);