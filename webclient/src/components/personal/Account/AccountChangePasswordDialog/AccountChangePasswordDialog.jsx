import React from 'react';

import ChangePasswordDialog from '../../../modal/ChangePasswordDialog';
import * as PropTypes from 'prop-types';

const AccountChangePasswordDialog = ({
  userLogin,
  onSubmit,
}) => {
  return (
    <ChangePasswordDialog
      userLogin={userLogin}
      onSubmit={onSubmit}
    />
  )
};

AccountChangePasswordDialog.propTypes = {
  userLogin: PropTypes.string,
  onSubmit: PropTypes.func,
};

AccountChangePasswordDialog.defaultProps = {
  userLogin: '',
  onSubmit: () => {},
};

export { AccountChangePasswordDialog };
