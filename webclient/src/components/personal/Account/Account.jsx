import React, { PureComponent } from 'react';
import * as PropTypes from 'prop-types';

import './Account.scss';

import {
  AppContent,
  PageContentHeader,
  HeaderTitle,
  HeaderToolbar,
  ToolbarButton,
  PageContentRoot,
} from '../../AppContent';

import AccountForm from './AccountForm';
import AccountChangePasswordDialog from './AccountChangePasswordDialog';

import { PERSONAL_ACCOUNT } from '../../../domain/model/entityId';

class Account extends PureComponent {
  static propTypes = {
    authToken: PropTypes.string,
    account: PropTypes.shape({}),
    accountLoadStatus: PropTypes.number,
    editMode: PropTypes.bool,
    editRecord: PropTypes.func,
    closeSection: PropTypes.func,
    reloadEntity: PropTypes.func,
    loadAccount: PropTypes.func,
    onRecordChange: PropTypes.func,
    cancelEditRecord: PropTypes.func,
    saveRecord: PropTypes.func,
    activationRequest: PropTypes.func,
    showChangePasswordDialog: PropTypes.func,
  };

  static defaultProps = {
    authToken: null,
    account: null,
    accountLoadStatus: 0,
    editMode: false,
    editRecord: () => {},
    closeSection: () => {},
    reloadEntity: () => {},
    loadAccount: () => {},
    onRecordChange: () => {},
    cancelEditRecord: () => {},
    saveRecord: () => {},
    activationRequest: () => {},
    showChangePasswordDialog: () => {},
  };

  componentDidMount() {
    const { accountLoadStatus, reloadEntity } = this.props;
    if (accountLoadStatus > 1) {
      reloadEntity(PERSONAL_ACCOUNT);
    }
    this.updateData();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    this.updateData();
  }

  updateData = () => {
    const {
      account,
      accountLoadStatus,
      authToken,
      loadAccount,
      reloadEntity,
    } = this.props;

    if (accountLoadStatus === 0) {
      loadAccount(authToken);
    }

    if ((accountLoadStatus === 2) && !account) {
      reloadEntity(PERSONAL_ACCOUNT);
    }
  };

  onSaveRecord = () => {
    const {
      saveRecord,
      authToken,
      account,
    } = this.props;
    if(account) {
      saveRecord(authToken, account);
    }
  };

  onActivate = () => {
    const { authToken, activationRequest } = this.props;
    activationRequest(authToken);
  };

  render() {
    const {
      account,
      editMode,
      editRecord,
      closeSection,
      reloadEntity,
      onRecordChange,
      cancelEditRecord,
      showChangePasswordDialog,
    } = this.props;

    const hasActivation = account && account.role_id !== 3;

    return (
      <AppContent>
        <PageContentHeader>
          <HeaderTitle title="Account information" />
          <HeaderToolbar>
            <ToolbarButton
              key="btnActivate"
              condition={!hasActivation && Boolean(account)}
              onClick={this.onActivate}
            >
              Activate
            </ToolbarButton>

            <ToolbarButton
              key="btnEdit"
              condition={hasActivation && !editMode && Boolean(account)}
              onClick={() => { editRecord(account) }}
            >
              Edit
            </ToolbarButton>

            <ToolbarButton
              key="btnSave"
              condition={editMode}
              onClick={this.onSaveRecord}
            >
              Save
            </ToolbarButton>

            <ToolbarButton
              key="btnCancel"
              condition={editMode}
              onClick={cancelEditRecord}
            >
              Cancel
            </ToolbarButton>

            <ToolbarButton
              key="buttonReload"
              onClick={() => { reloadEntity(PERSONAL_ACCOUNT); }}
            >
              Reload
            </ToolbarButton>

            <ToolbarButton
              key="buttonClose"
              onClick={closeSection}
            >
              Close
            </ToolbarButton>
          </HeaderToolbar>
        </PageContentHeader>
        <PageContentRoot  className="Account">
          {
            account && (
              <AccountForm
                account={account}
                editMode={editMode}
                onChange={onRecordChange}
                onChangePassword={showChangePasswordDialog}
              />
            )
          }
          <AccountChangePasswordDialog />
        </PageContentRoot>
      </AppContent>
    );
  }
}

export { Account };
