import { connect } from 'react-redux';

import { Account } from './Account';
import {
  accountLoadStatusSelector,
  authTokenSelector,
  editModeSelector,
  personalAccountSelector
} from '../../../store/selectors';
import { loadEntity, activationRequest } from '../../../network';
import { PERSONAL_ACCOUNT } from '../../../domain/model/entityId';
import {
  editRecord,
  closePersonalSection,
  reloadEntity,
  onRecordChange,
  saveRecord,
  cancelEditRecord,
  showChangePasswordDialog,
} from '../../../store/dispatchers';
import { acChangeValue } from '../../../store/actions';

const loadAccount = (token) => (dispatch) => {
  loadEntity(PERSONAL_ACCOUNT, token)(dispatch);
};

const toggleMode = mode => (dispatch) => {
  dispatch(acChangeValue({ name: 'accountEditMode', value: !mode }));
};

const saveAccountRecord = (token, account) => (dispatch) => {
  saveRecord(token, PERSONAL_ACCOUNT, account)(dispatch);
};

const mapStateToProps = state => ({
  authToken: authTokenSelector(state),
  editMode: editModeSelector(state),
  account: personalAccountSelector(state),
  accountLoadStatus: accountLoadStatusSelector(state),
});

const mapDispatchToProps = {
  loadAccount,
  reloadEntity,
  closeSection: closePersonalSection,
  toggleMode,
  editRecord,
  onRecordChange,
  cancelEditRecord,
  saveRecord: saveAccountRecord,
  activationRequest,
  showChangePasswordDialog,
};

export default connect(mapStateToProps, mapDispatchToProps)(Account);
