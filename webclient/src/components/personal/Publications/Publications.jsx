import React, { PureComponent } from 'react';
import * as PropTypes from 'prop-types';

import {
  AppContent,
  PageContentHeader,
  HeaderTitle,
  HeaderToolbar,
  ToolbarButton,
  PageContentRoot,
  Aside,
  CentralSection,
} from '../../AppContent';

import { ItemsList } from '../../controls/complex';
import PublicationForm from './PublicationForm';
import { EditContentDialog } from '../../modal';

import './Publications.scss';
import ENTITIES from '../../../domain/model/entities';
import { PERSONAL_NEWS } from '../../../domain/model/entityId';
import { isExists, isFilledArray, toCompatibleTimeStamp } from '../../../utils';

class Publications extends PureComponent {
  static propTypes = {
    authToken: PropTypes.string,
    news: PropTypes.arrayOf(PropTypes.shape({})),
    newsLoadStatus: PropTypes.number,
    publication: PropTypes.shape({}),
    activeNewsId: PropTypes.number,
    entityItemSelect: PropTypes.func,
    editMode: PropTypes.bool,
    reloadEntity: PropTypes.func,
    closeSection: PropTypes.func,
    loadPersonalNews: PropTypes.func,
    toggleMode: PropTypes.func,
    onRecordChange: PropTypes.func,
    newRecord: PropTypes.func,
    editRecord: PropTypes.func,
    cancelEditRecord: PropTypes.func,
    onEditContent: PropTypes.func,
  };

  static defaultProps = {
    authToken: null,
    news: null,
    newsLoadStatus: 0,
    publication: null,
    activeNewsId: -1,
    entityItemSelect: () => {},
    editMode: false,
    reloadEntity: () => {},
    closeSection: () => {},
    loadPersonalNews: () => {},
    toggleMode: () => {},
    onRecordChange: () => {},
    newRecord: () => {},
    editRecord: () => {},
    cancelEditRecord: () => {},
    onEditContent: () => {},
  };

  componentDidMount() {
    const { reloadEntity } = this.props;

    [PERSONAL_NEWS]
      .forEach((entityName) => {
        const { [ENTITIES[PERSONAL_NEWS].loadStatus]: loadStatus } = this.props;
        if (!isExists(loadStatus) || (loadStatus > 1)) {
          reloadEntity(entityName);
        }
      });

    this.updateData();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    this.updateData();
  }

  updateData = () => {
    const { authToken, loadEntity, reloadEntity } = this.props;

    [PERSONAL_NEWS]
      .forEach((entityName) => {
        const {
          [ENTITIES[entityName].loadStatus]: loadStatus,
          [ENTITIES[entityName].data]: data,
        } = this.props;

        if (loadStatus === 0) {
          loadEntity(entityName, authToken);
        }

        if ((loadStatus === 2) && !isExists(data)) {
          reloadEntity(entityName);
        }
      });
  };

  handleListItemClick = (id) => {
    const { entityItemSelect } = this.props;
    entityItemSelect(PERSONAL_NEWS, id);
  };

  onSaveRecord = () => {
    const {
      saveRecord,
      authToken,
      publication,
    } = this.props;
    if (publication) {
      if (publication && publication.publishing_ts) {
        const { publishing_ts } = publication;
        /* let date = toCompatibleTimeStamp(publishing_ts);
        let date = new Date(publishing_ts);
        if (!isNaN(date)) {
          date = date.toISOString();
          date = date.split(/[TZ]/).filter(Boolean).join(' ');
        } */
        publication.publishing_ts = toCompatibleTimeStamp(publishing_ts);
      }
      saveRecord(authToken, publication);
    }
  };

  render() {
    const {
      news,
      publication,
      activeNewsId,
      editMode,
      reloadEntity,
      closeSection,
      onRecordChange,
      newRecord,
      editRecord,
      cancelEditRecord,
      onEditContent,
    } = this.props;

    return (
      <AppContent>
        <PageContentHeader>
          <HeaderTitle title="Personal publications" />
          <HeaderToolbar>
            <ToolbarButton
              key="btnNew"
              condition={!editMode}
              onClick={newRecord}
            >
              New
            </ToolbarButton>

            <ToolbarButton
              key="btnEdit"
              condition={!editMode && isExists(publication)}
              onClick={() => { editRecord(publication) }}
            >
              Edit
            </ToolbarButton>

            <ToolbarButton
              key="btnSave"
              condition={editMode}
              onClick={this.onSaveRecord}
            >
              Save
            </ToolbarButton>

            <ToolbarButton
              key="btnCancel"
              condition={editMode}
              onClick={cancelEditRecord}
            >
              Cancel
            </ToolbarButton>

            <ToolbarButton
              key="btnReload"
              onClick={() => { reloadEntity(PERSONAL_NEWS); }}
            >
              Reload
            </ToolbarButton>

            <ToolbarButton
              key="buttonClose"
              onClick={closeSection}
            >
              Close
            </ToolbarButton>
          </HeaderToolbar>
        </PageContentHeader>

        <PageContentRoot>
          <Aside>
            {
              isFilledArray(news)
              ? (
                  <ItemsList
                    value={activeNewsId}
                    items={news}
                    asName="title"
                    onItemClick={this.handleListItemClick}
                  />
                )
              : 'No data is available'
            }
          </Aside>

          <CentralSection>
            <PublicationForm
              publication={publication}
              editMode={editMode}
              onChange={onRecordChange}
              onEditContent={onEditContent}
            />
          </CentralSection>

          <EditContentDialog
            onContentIdChange={
              (v) => { onRecordChange({
                target: {
                  name: 'content',
                  value: v,
                },
              }) }
            }
          />
        </PageContentRoot>
      </AppContent>
    );
  }
}

export { Publications };
