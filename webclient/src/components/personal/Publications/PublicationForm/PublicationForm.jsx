import React from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';
import { get } from 'lodash';

import {
  BasicInput,
  BasicDataInput,
} from '../../../controls/basic';

import { Toggler } from '../../../controls/labeled';
import { ToolbarButton } from '../../../AppContent';

import styles from './PublicationForm.module.scss';
import { focusById } from '../../../../utils';

const PublicationForm = ({
  publication,
  editMode,
  onChange,
  onEditContent,
}) => {
  const getValidationMessage = (field) => get(publication, `validation.${field}`, '');
  const contentId = get(publication, 'content', -1);

  /* <BasicInput
    id="publishing_ts"
    name="publishing_ts"
    disabled={!editMode}
    value={publication.publishing_ts}
    validationMessage={getValidationMessage('publishing_ts')}
    onChange={onChange}
  /> */

  return (
    <div className={styles.PublicationForm}>
      {
        publication && (
          <form name="publicationForm">
            <div key="title" className={styles.row}>
              <label htmlFor="title">Title</label>
              <BasicInput
                id="title"
                name="title"
                disabled={!editMode}
                value={publication.title}
                validationMessage={getValidationMessage('title')}
                onChange={onChange}
                onEnterPress={() => focusById('description')}
              />
            </div>

            <div key="description" className={styles.row}>
              <label htmlFor="description">Description</label>
              <BasicInput
                id="description"
                name="description"
                disabled={!editMode}
                value={publication.description}
                validationMessage={getValidationMessage('description')}
                onChange={onChange}
                onEnterPress={() => focusById('publishing_ts')}
              />
            </div>

            <div key="dateTime" className={classNames(styles.row, styles.rowWide)}>
              <div key="created" className={styles.cell3}>
                <label htmlFor="created_ts">Created</label>
                <BasicInput
                  id="created_ts"
                  name="created_ts"
                  disabled={true}
                  value={publication.created_ts}
                  onChange={() => {}}
                />
              </div>

              <div key="updated" className={styles.cell3}>
                <label htmlFor="updated_ts">Updated</label>
                <BasicInput
                  id="updated_ts"
                  name="updated_ts"
                  disabled={true}
                  value={publication.updated_ts}
                  onChange={() => {}}
                />
              </div>

              <div key="publishing" className={styles.cell3}>
                <label htmlFor="publishing_ts">Publishing</label>
                <BasicDataInput
                  id="publishing_ts"
                  name="publishing_ts"
                  disabled={!editMode}
                  value={publication.publishing_ts}
                  validationMessage={getValidationMessage('publishing_ts')}
                  onChange={onChange}
                />
              </div>

              <div key="isActive" className={styles.cell3}>
                <Toggler
                  id="is_active"
                  name="is_active"
                  label="Is active"
                  disabled={!editMode}
                  value={Boolean(publication.is_active)}
                  validationMessage={getValidationMessage('is_active')}
                  onChange={onChange}
                />
              </div>
            </div>

            <div key="btnAddContent" className={classNames(styles.row, styles.formToolbar)}>
              <ToolbarButton
                onClick={() => onEditContent(contentId)}
                style={{ width: '10rem' }}
                disabled={!editMode}
              >
                Content editor
              </ToolbarButton>
            </div>
          </form>
        )
      }
    </div>
  );
};

PublicationForm.propTypes = {
  publication: PropTypes.shape({}),
  editMode: PropTypes.bool,
  validation: PropTypes.shape({}),
  onChange: PropTypes.func,
  onEditContent: PropTypes.func,
};

PublicationForm.defaultProps = {
  publication: null,
  editMode: false,
  validation: {},
  onChange: () => {},
  onEditContent: () => {},
};

export { PublicationForm };
