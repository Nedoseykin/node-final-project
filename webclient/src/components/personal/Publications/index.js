import { connect } from 'react-redux';

import { Publications } from './Publications';

import {
  closePersonalSection,
  entityItemSelect,
  onRecordChange,
  reloadEntity,
  newRecord,
  editRecord,
  cancelEditRecord,
  saveRecord,
  showEditContentDialog,
} from '../../../store/dispatchers';
import {
  personalActiveEntityItemIdSelector,
  authTokenSelector,
  newsLoadStatusSelector,
  newsSelector,
  publicationSelector,
  editModeSelector,
} from '../../../store/selectors';
import { loadEntity } from '../../../network';
import { PERSONAL_NEWS } from '../../../domain/model/entityId';

const savePublicationRecord = (token, publication) => (dispatch) => {
  delete(publication.validation);
  saveRecord(token, PERSONAL_NEWS, publication)(dispatch);
};

const mapStateToProps = state => ({
  authToken: authTokenSelector(state),
  news: newsSelector(state),
  newsLoadStatus: newsLoadStatusSelector(state),
  activeNewsId: personalActiveEntityItemIdSelector(state),
  publication: publicationSelector(state),
  editMode: editModeSelector(state),
});

const mapDispatchToProps = {
  entityItemSelect,
  loadEntity,
  reloadEntity,
  onRecordChange,
  newRecord,
  editRecord,
  saveRecord: savePublicationRecord,
  cancelEditRecord,
  closeSection: closePersonalSection,
  onEditContent: showEditContentDialog,
};

export default connect(mapStateToProps, mapDispatchToProps)(Publications);
