import React from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';

import styles from './ImagesGallery.module.scss';

const ImagesGallery = ({
  editMode,
  children,
}) => {
  const calculatedClassName = classNames(
    styles.ImagesGallery,
    editMode && styles.ImagesGalleryEditing,
  );

  return (
    <ul className={calculatedClassName}>
      { children }
    </ul>
  );
};

ImagesGallery.propTypes = {
  editMode: PropTypes.bool,
  children: PropTypes.any,
};

ImagesGallery.defaultProps = {
  editMode: false,
  children: [],
};

export { ImagesGallery };
