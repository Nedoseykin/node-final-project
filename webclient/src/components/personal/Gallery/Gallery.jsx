import React, { PureComponent } from 'react';
import * as PropTypes from 'prop-types';

import {
  AppContent,
  PageContentHeader,
  HeaderTitle,
  HeaderToolbar,
  ToolbarButton,
  PageContentRoot,
} from '../../AppContent';

import AddImageDialog from './AddImageDialog';
import EditImageDialog from './EditImageDialog';
import ImagePreviewDialog from './ImagePreviewDialog';
import ImagesGallery from './ImagesGallery';
import ImageItem from './ImageItem';

import { PERSONAL_GALLERY } from '../../../domain/model/entityId';
import { isFilledArray } from '../../../utils';

class PersonalGallery extends PureComponent {
  static propTypes = {
    authToken: PropTypes.string,
    images: PropTypes.arrayOf(PropTypes.shape({})),
    imagesLoadStatus: PropTypes.number,
    editMode: PropTypes.bool,
    toggleMode: PropTypes.func,
    showAddImageDialog: PropTypes.func,
    showEditImageDialog: PropTypes.func,
    showPreviewImageDialog: PropTypes.func,
    closeSection: PropTypes.func,
    loadImages: PropTypes.func,
    reloadEntity: PropTypes.func,
  };

  static defaultProps = {
    authToken: null,
    images: null,
    imagesLoadStatus: 0,
    editMode: false,
    toggleMode: () => {},
    showAddImageDialog: () => {},
    showEditImageDialog: () => {},
    showPreviewImageDialog: () => {},
    closeSection: () => {},
    loadImages: () => {},
    reloadEntity: () => {},
    onImageLoadError: () => {},
    setNoNeedThumb: () => {},
  };

  componentDidMount() {
    const { imagesLoadStatus, reloadEntity } = this.props;
    if (imagesLoadStatus > 1) {
      reloadEntity(PERSONAL_GALLERY);
    }
    this.updateData();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    this.updateData();
  }

  updateData = () => {
    const {
      images,
      imagesLoadStatus,
      authToken,
      loadImages,
      reloadEntity,
    } = this.props;

    if (imagesLoadStatus === 0) {
      loadImages(authToken);
    }

    if ((imagesLoadStatus === 2) && !Array.isArray(images)) {
      reloadEntity(PERSONAL_GALLERY);
    }
  };

  handleImageItemClick = ({id, ...rest}) => () => {
    const {
      showEditImageDialog,
      showPreviewImageDialog,
      editMode,
    } = this.props;
    const item = { imageId: id, ...rest };
    if (editMode) {
      showEditImageDialog && showEditImageDialog(item);
    } else {
      showPreviewImageDialog && showPreviewImageDialog(item);
    }
  };

  getImageItems = (images) => {
    return isFilledArray(images)
      ? images.map((item) => {
          const { curr_name: name, id, title } = item;
          return (
            <ImageItem
              key={id}
              id={id}
              title={title}
              alternativeSrc={`/images/${name}`}
              src={`/images/thumbnails/${name}`}
              onClick={this.handleImageItemClick(item)}
            />
          );
        })
      : 'No images are available';
  };

  render() {
    const {
      images,
      editMode,
      toggleMode,
      showAddImageDialog,
      reloadEntity,
      closeSection,
    } = this.props;

    const imageItems = this.getImageItems(images);

    return (
      <AppContent>
        <PageContentHeader>
          <HeaderTitle title="Personal gallery" />
          <HeaderToolbar>
            <ToolbarButton
              key="buttonAdd"
              onClick={showAddImageDialog}
            >
              Add image
            </ToolbarButton>

            <ToolbarButton
              key="buttonMode"
              style={{ width: 130 }}
              onClick={() => toggleMode(editMode)}
            >
              {editMode ? 'Preview mode' : 'Edit mode'}
            </ToolbarButton>

            <ToolbarButton
              key="btnReload"
              onClick={() => { reloadEntity(PERSONAL_GALLERY); }}
            >
              Reload
            </ToolbarButton>

            <ToolbarButton
              key="buttonClose"
              onClick={closeSection}
            >
              Close
            </ToolbarButton>
          </HeaderToolbar>
        </PageContentHeader>

        <PageContentRoot>
          <ImagesGallery editMode={editMode}>
            {imageItems}
          </ImagesGallery>

          <AddImageDialog />

          <EditImageDialog />

          <ImagePreviewDialog />
        </PageContentRoot>
      </AppContent>
    );
  }
}

export { PersonalGallery };
