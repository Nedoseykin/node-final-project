import React, { useState } from 'react';
import * as PropTypes from 'prop-types';

import { BasicModal } from '../../../modal';
import { BasicInput } from '../../../controls/basic';
import { ToolbarButton } from '../../../AppContent';
import { DeleteConfirmationDialog } from '../../../modal';

import styles from './EditImageDialog.module.scss';
import { focusById, handleOnLoadAndResize, isFilledString } from '../../../../utils';

const EditImageDialog = ({
  authToken,
  id,
  name,
  size,
  title,
  description,
  validation,
  isVisible,
  onChange,
  onSubmit,
  onClose,
  showDeleteConfirmationDialog,
  onDelete,
  onThumbCreate,
}) => {
  const [state, setState] = useState({ needThumb: false, loadStatus: 2 });

  const handleSubmit = () => {
    const payload = {
      id,
      title,
      description,
    };
    onSubmit && onSubmit(authToken, payload);
  };

  const handleDelete = () => {
    showDeleteConfirmationDialog({
      message: `Please confirm that you want to delete the image: ${title}?`,
      onDelete: () => {
        onDelete(authToken, id);
      },
    });
  };

  const handleOnLoadError = () => {
    !state.needThumb && setState({ needThumb: true, loadStatus: 3 });
  };

  const onThumbCreateSuccess = (id) => {
    setState({ needThumb: false, loadStatus: 2 });
  };

  const handleOnLoadSuccess = (e) => {
    setState({ needThumb: false, loadStatus: 2 });
    handleOnLoadAndResize(e);
  };

  return isVisible && (
    <BasicModal
      className={styles.EditImageDialog}
      onClick={onClose}
    >
      <form
        name="editImageForm"
        className={styles.editImageForm}
        onClick={(e) => {
          e.stopPropagation();
        }}
      >
        <fieldset className={styles.content}>
          <legend className={styles.contentTitle}>
            Edit image
          </legend>

          <div key="title" className={styles.row}>
            <label htmlFor="title">Title</label>
            <BasicInput
              id="title"
              name="title"
              tabIndex="1"
              autoComplete="on"
              autoFocus
              value={title || ''}
              validationMessage={validation.title}
              change={onChange}
              onEnterPress={() => focusById('description')}
            />
          </div>

          <div key="description" className={styles.row}>
            <label htmlFor="description">Description</label>
            <BasicInput
              id="description"
              name="description"
              tabIndex="2"
              autoComplete="on"
              value={description || ''}
              validationMessage={validation.description}
              change={onChange}
              onEnterPress={() => focusById('btnSubmit')}
            />
          </div>

          <div key="size" className={styles.row}>
            <label htmlFor="size">Size</label>
            <BasicInput
              id="size"
              name="size"
              tabIndex="3"
              autoComplete="off"
              value={`${size}` || '0'}
              disabled={true}
              change={() => {}}
            />
          </div>

          <div key="preview" className={styles.row}>
            <label htmlFor="preview">Preview</label>
            <div className={styles.preview}>
              {
                isFilledString(name) && state.loadStatus === 2 && (
                  <img
                    src={`/images/thumbnails/${name}`}
                    alt={title}
                    onLoad={handleOnLoadSuccess}
                    onError={handleOnLoadError}
                  />
                )
              }
            </div>
          </div>
        </fieldset>

        <div className={styles.toolbar}>
          <ToolbarButton
            key="btnDelete"
            id="btnCancel"
            onClick={handleDelete}
          >
            Delete
          </ToolbarButton>

          <ToolbarButton
            key="btnCreateThumb"
            id="btnCreateThumb"
            condition={state.needThumb}
            onClick={() => {
              setState({ ...state, loadStatus: 1 });
              onThumbCreate(authToken, id, onThumbCreateSuccess);
            }}
          >
            Thumbnail
          </ToolbarButton>

          <ToolbarButton
            key="btnCancel"
            id="btnCancel"
            onClick={onClose}
          >
            Close
          </ToolbarButton>

          <ToolbarButton
            key="btnSubmit"
            id="btnSubmit"
            onClick={handleSubmit}
          >
            Submit
          </ToolbarButton>
        </div>
      </form>

      <DeleteConfirmationDialog />

    </BasicModal>
  )
};

EditImageDialog.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string,
  size: PropTypes.number,
  title: PropTypes.string,
  description: PropTypes.string,
  validation: PropTypes.shape({}),
  isVisible: PropTypes.bool,
  onChange: PropTypes.func,
  onSubmit: PropTypes.func,
  onClose: PropTypes.func,
  showDeleteConfirmationDialog: PropTypes.func,
  onDelete: PropTypes.func,
};

EditImageDialog.defaultProps = {
  size: 0,
  name: '',
  title: '',
  description: '',
  validation: {},
  isVisible: false,
  onChange: () => {},
  onSubmit: () => {},
  onClose: () => {},
  showDeleteConfirmationDialog: () => {},
  onDelete: () => {},
};

export { EditImageDialog };
