import { connect } from 'react-redux';

import { EditImageDialog } from './EditImageDialog';

import {
  hideDeleteConfirmationDialog,
  hideEditImageDialog,
  showDeleteConfirmationDialog,
} from '../../../../store/dispatchers'
import { acModalChangeValue, acRemoveItemFromData } from '../../../../store/actions';
import { EDIT_IMAGE_DIALOG } from '../../../../domain/modals/modalsId';
import {
  authTokenSelector,
  editImageDescriptionSelector,
  editImageIdSelector,
  editImageNameSelector,
  editImageNeedThumbSelector,
  editImageSizeSelector,
  editImageTitleSelector,
  editImageValidationSelector,
  isVisibleEditImageDialogSelector,
} from '../../../../store/selectors';
import { deleteEntity, updateEntity, createThumbnail } from '../../../../network';
import { PERSONAL_GALLERY } from '../../../../domain/model/entityId';

const onChange = e => (dispatch) => {
  const { name, value } = e.target;
  dispatch(acModalChangeValue({ modalId: EDIT_IMAGE_DIALOG, name, value }));
};

const onSubmit = (token, item) => (dispatch) =>  {
  const cbSuccess = () => hideEditImageDialog()(dispatch);
  const cbNotValid = (result) => dispatch(acModalChangeValue({
    modalId: EDIT_IMAGE_DIALOG,
    name: 'validation',
    value: result,
  }));
  updateEntity(
    PERSONAL_GALLERY,
    token,
    item,
    cbSuccess,
    cbNotValid,
  )(dispatch);
};

const onDelete = (token, id) => (dispatch) => {
  deleteEntity({
    entityName: PERSONAL_GALLERY,
    id,
    token,
    cbSuccess: (resp) => {
      dispatch(acRemoveItemFromData({ entityName: PERSONAL_GALLERY, id: resp.id }));
      hideDeleteConfirmationDialog()(dispatch);
      hideEditImageDialog()(dispatch);
    },
  })(dispatch);
};

const onThumbCreate = (token, id, cbSuccess) => dispatch => {
  createThumbnail({
    token,
    id,
    dispatch,
    cbSuccess,
  });
};

const mapStateToProps = state => ({
  authToken: authTokenSelector(state),
  id: editImageIdSelector(state),
  name: editImageNameSelector(state),
  size: editImageSizeSelector(state),
  title: editImageTitleSelector(state),
  description: editImageDescriptionSelector(state),
  validation: editImageValidationSelector(state),
  isVisible: isVisibleEditImageDialogSelector(state),
  needThumb: editImageNeedThumbSelector(state),
});

const mapDispatchToProps = {
  onChange,
  onSubmit,
  onClose: hideEditImageDialog,
  showDeleteConfirmationDialog,
  onDelete,
  onThumbCreate,
};

export default connect(mapStateToProps, mapDispatchToProps)(EditImageDialog);
