import { connect } from 'react-redux';

import { AddImageDialog } from './AddImageDialog';

import { hideAddImageDialog } from '../../../../store/dispatchers';
import {
  authTokenSelector,
  imageDescriptionSelector,
  imageFileNameSelector,
  imageTitleSelector, imageValidationSelector,
  isVisibleAddImageDialogSelector,
} from '../../../../store/selectors';
import { acInsertNewItemToData, acModalChangeValue } from '../../../../store/actions';
import { ADD_IMAGE_DIALOG } from '../../../../domain/modals/modalsId';
import { createEntityUniversal } from '../../../../network';
import { PERSONAL_GALLERY } from '../../../../domain/model/entityId';

const onChange = e => (dispatch) => {
  const { name, value } = e.target;
  dispatch(acModalChangeValue({ modalId: ADD_IMAGE_DIALOG, name, value }));
};

const onSubmit = (token, formData) => (dispatch) => {
  const cbSuccess = (loadedData) => {
    dispatch(acInsertNewItemToData({ entityName: PERSONAL_GALLERY, item: loadedData }));
    hideAddImageDialog()(dispatch);
  };
  const cbNotValid = (validation) => {
    dispatch(acModalChangeValue({
      modalId: ADD_IMAGE_DIALOG,
      name: 'validation',
      value: validation,
    }));
  };
  createEntityUniversal({
    entityName: PERSONAL_GALLERY,
    token,
    body: formData,
    reqHeaders: { 'content-type': null },
    cbSuccess,
    cbNotValid,
  })(dispatch);
};

const mapStateToProps = state => ({
  isVisible: isVisibleAddImageDialogSelector(state),
  title: imageTitleSelector(state),
  description: imageDescriptionSelector(state),
  fileName: imageFileNameSelector(state),
  authToken: authTokenSelector(state),
  validation: imageValidationSelector(state),
});

const mapDispatchToProps = {
  onChange,
  onSubmit,
  onClose: hideAddImageDialog,
};

export default connect(mapStateToProps, mapDispatchToProps)(AddImageDialog);
