import React, { PureComponent } from 'react';
import * as PropTypes from 'prop-types';

import { BasicModal } from '../../../modal';
import { BasicInput } from '../../../controls/basic';
import { ToolbarButton } from '../../../AppContent';

import styles from './AddImageDialog.module.scss';
import { focusById, handleOnLoadAndResize } from '../../../../utils';

class AddImageDialog extends PureComponent {
  static propTypes = {
    isVisible: PropTypes.bool,
    title: PropTypes.string,
    description: PropTypes.string,
    fileName: PropTypes.string,
    authToken: PropTypes.string,
    validation: PropTypes.shape({}),
    onSubmit: PropTypes.func,
    onChange: PropTypes.func,
    onClose: PropTypes.func,
  };

  static defaultProps = {
    isVisible: false,
    title: '',
    description: '',
    fileName: '',
    authToken: '',
    validation: {},
    onSubmit: () => {},
    onChange: () => {},
    onClose: () => {},
  };

  constructor(props) {
    super(props);
    this.fileInput = document.createElement('input');
    this.fileInput.setAttribute('id', 'fileInput');
    this.fileInput.setAttribute('type', 'file');
    this.fileInput.setAttribute('accept', 'image/*');
    this.fileInput.addEventListener('change', this.handleFileSelected, false);
    this.state = {
      srcImage: null,
    };
  }

  getFileSize = () => (
    this.fileInput && this.fileInput.files && this.fileInput.files[0]
      ? this.fileInput.files[0].size
      : 0
  );

  handleFileSelected = () => {
    const { onChange } = this.props;
    if (this.fileInput.files.length) {
      const { files } = this.fileInput;
      const { name, size } = files[0];
      this.setState({
        srcImage: URL.createObjectURL(files[0]),
      }, () => {
        onChange && onChange({ target: { name: 'fileName', value: name } });
        onChange && onChange({ target: { name: 'fileSize', value: size } });
      });
    }
  };

  handleSubmit = () => {
    const {
      title,
      description,
      fileName,
      authToken,
      onSubmit,
    } = this.props;

    const formData = new FormData();

    formData.append('title', title);
    formData.append('description', description);
    formData.append('fileName', fileName);
    formData.append('fileSize', this.getFileSize());
    formData.append('image', this.fileInput.files[0]);
    onSubmit && onSubmit(authToken, formData);
  };

  render() {
    const {
      isVisible,
      title,
      description,
      fileName,
      validation,
      onChange,
      onClose,
    } = this.props;

    const {
      srcImage,
    } = this.state;

    const fileSize = this.getFileSize();

    return isVisible && (
      <BasicModal
        className={styles.AddImageDialog}
        onClick={onClose}
      >
        <form
          className={styles.AddImageForm}
          onClick={(e) => {
            e.stopPropagation();
          }}
        >
          <fieldset className={styles.content}>
            <legend className={styles.contentTitle}>
              Add image
            </legend>
            <div key="title" className={styles.row}>
              <label htmlFor="title">Title</label>
              <BasicInput
                id="title"
                name="title"
                tabIndex="1"
                autoComplete="on"
                autoFocus
                value={title || ''}
                validationMessage={validation.title}
                change={onChange}
                onEnterPress={() => focusById('description')}
              />
            </div>

            <div key="description" className={styles.row}>
              <label htmlFor="description">Description</label>
              <BasicInput
                id="description"
                name="description"
                tabIndex="2"
                autoComplete="on"
                value={description}
                change={onChange}
                onEnterPress={() => focusById('btnBrowse')}
              />
            </div>

            <div key="preview" className={styles.row}>
              <label htmlFor="preview">Preview</label>
              <div className={styles.preview}>
                {
                  srcImage && (
                    <img
                      src={srcImage}
                      alt="For upload"
                      onLoad={handleOnLoadAndResize}
                    />
                  )
                }
              </div>
            </div>

            <div key="fileName" className={styles.row}>
              <label htmlFor="fileName">File name</label>
              <BasicInput
                id="fileName"
                name="fileName"
                value={fileName}
                validationMessage={validation.fileSize}
                disabled={false}
                onChange={onChange}
              />
            </div>

            <div key="fileSize" className={styles.row}>
              <label htmlFor="fileSize">File size</label>
              <BasicInput
                id="fileSize"
                name="fileSize"
                value={`${fileSize}`}
                validationMessage={validation.fileSize}
                disabled={true}
                onChange={onChange}
              />
            </div>
          </fieldset>

          <div className={styles.toolbar}>
            <ToolbarButton
              key="btnBrowse"
              id="btnBrowse"
              onClick={() => {
                this.fileInput.click();
              }}
            >
              Browse
            </ToolbarButton>

            <ToolbarButton
              key="btnCancel"
              id="btnCancel"
              onClick={onClose}
            >
              Cancel
            </ToolbarButton>

            <ToolbarButton
              key="btnSubmit"
              id="btnSubmit"
              onClick={this.handleSubmit}
            >
              Submit
            </ToolbarButton>
          </div>
        </form>
      </BasicModal>
    );
  }
}

export { AddImageDialog };
