import React, { useState } from 'react';
import * as PropTypes from 'prop-types';

import styles from './ImageItem.module.scss';
import { handleOnLoadAndResize } from '../../../../utils';

const ImageItem = ({
  id,
  src,
  alternativeSrc,
  title,
  onClick,
  onLoadError,
  onLoadSuccess,
  // description,
  // size,
}) => {
  const [source, setSource] = useState(src);

  const handleError = () => {
    if (alternativeSrc && (source !== alternativeSrc)) { setSource(alternativeSrc); }
    onLoadError && onLoadError(id);
  };

  return (
    <li className={styles.ImageItem} onClick={onClick}>
      <figure>
        <div className={styles.imageBox}>
          <img
            src={source}
            alt={title}
            onLoad={(e) => {
              onLoadSuccess && onLoadSuccess();
              handleOnLoadAndResize(e);
            }}
            onError={handleError}
          />
        </div>
        <figcaption>
          {title}
        </figcaption>
      </figure>
    </li>
  );
};

ImageItem.propTypes = {
  id: PropTypes.number,
  src: PropTypes.string,
  alternativeSrc: PropTypes.string,
  title: PropTypes.string,
  needThumb: PropTypes.bool,
  onClick: PropTypes.func,
  onLoadError: PropTypes.func,
  onLoadSuccess: PropTypes.func,
};

ImageItem.defaultProps = {
  id: -1,
  src: '',
  alternativeSrc: '',
  title: '',
  needThumb: false,
  onClick: () => {},
  onLoadError: () => {},
  onLoadSuccess: () => {},
};

export { ImageItem };
