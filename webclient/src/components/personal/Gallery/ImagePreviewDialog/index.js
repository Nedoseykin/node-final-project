import { connect } from 'react-redux';
import { ImagePreviewDialog } from './ImagePreviewDialog';
import { hidePreviewImageDialog } from '../../../../store/dispatchers';
import {
  isVisiblePreviewImageDialogSelector,
  previewImageDescriptionSelector,
  previewImageSrcSelector,
  previewImageTitleSelector,
} from '../../../../store/selectors';

const mapStateToProps = state => ({
  isVisible: isVisiblePreviewImageDialogSelector(state),
  title: previewImageTitleSelector(state),
  src: previewImageSrcSelector(state),
  description: previewImageDescriptionSelector(state),
});

const mapDispatchToProps = {
  onClose: hidePreviewImageDialog,
};

export default connect(mapStateToProps, mapDispatchToProps)(ImagePreviewDialog);
