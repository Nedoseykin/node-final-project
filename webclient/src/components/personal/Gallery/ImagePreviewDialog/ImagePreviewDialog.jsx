import React, { PureComponent } from 'react';
import * as PropTypes from 'prop-types';

import { BasicModal } from '../../../modal';

import styles from './ImagePreviewDialog.module.scss';
import { isFilledString } from '../../../../utils';
import { handleOnLoadAndResize } from '../../../../utils';

class ImagePreviewDialog extends PureComponent {
  static propTypes = {
    title: PropTypes.string,
    description: PropTypes.string,
    src: PropTypes.string,
    isVisible: PropTypes.bool,
    onClose: PropTypes.func,
  };

  static defaultProps = {
    title: '',
    description: '',
    src: '',
    isVisible: false,
    onClose: () => {},
  };

  constructor(props) {
    super(props);
    this.state = {
      isDescriptionVisible: false,
      width: 0,
      height: 0,
    };
  }

  handleClick = (e) => {
    const { isDescriptionVisible } = this.state;
    e.stopPropagation();
    this.setState({ isDescriptionVisible: !isDescriptionVisible });
  };

  handleImgLoaded = (e) => {
    const { currentTarget: eCurrentTarget } = e;
    const { naturalWidth, naturalHeight, parentElement } = eCurrentTarget;
    const currentTarget = parentElement;
    parentElement.naturalWidth = naturalWidth;
    parentElement.naturalHeight = naturalHeight;

    handleOnLoadAndResize({ currentTarget }, '90%', '90%');
    this.setState({ width: naturalWidth, height: naturalHeight });
  };

  render() {
    const {
      title,
      description,
      src,
      isVisible,
      onClose,
    } = this.props;

    const {
      isDescriptionVisible,
      height,
      width,
    } = this.state;

    return isVisible && (
      <BasicModal
        className={styles.ImagePreviewDialog}
        onClick={onClose}
      >
        <div
          className={styles.previewContainer}
          onClick={this.handleClick}
        >
          {
            isFilledString(src) && (
              <img
                src={src}
                alt={title}
                ref={(elm) => { this.img = elm; }}
                onLoad={this.handleImgLoaded}
              />
            )
          }
          {
            isDescriptionVisible && (
              <div className={styles.description}>
                <div key="text" className={styles.text}>
                  {description}
                </div>
                <div key="parameters" className={styles.parameters}>
                  {`${width} x ${height}`}
                </div>
              </div>
            )
          }
        </div>
      </BasicModal>
    )
  }
}

export { ImagePreviewDialog };
