import { connect } from 'react-redux';

import { PersonalGallery } from './Gallery';

import {
  showAddImageDialog,
  showEditImageDialog,
  showPreviewImageDialog,
  closePersonalSection,
  reloadEntity,
} from '../../../store/dispatchers';
import {
  authTokenSelector,
  imagesLoadStatusSelector,
  imagesSelector,
  imagesEditModeSelector,
} from '../../../store/selectors';
import { loadEntity } from '../../../network';
import { PERSONAL_GALLERY } from '../../../domain/model/entityId';
import { acChangeValue } from '../../../store/actions';

const loadImages = (token) => (dispatch) => {
  loadEntity(PERSONAL_GALLERY, token)(dispatch);
};

const toggleMode = mode => (dispatch) => {
  dispatch(acChangeValue({ name: 'imagesEditMode', value: !mode }));
};

const mapStateToProps = state => ({
  authToken: authTokenSelector(state),
  images: imagesSelector(state),
  imagesLoadStatus: imagesLoadStatusSelector(state),
  editMode: imagesEditModeSelector(state),
});

const mapDispatchToProps = {
  loadImages,
  reloadEntity,
  showAddImageDialog,
  showEditImageDialog,
  showPreviewImageDialog,
  closeSection: closePersonalSection,
  toggleMode,
};

export default connect(mapStateToProps, mapDispatchToProps)(PersonalGallery);
