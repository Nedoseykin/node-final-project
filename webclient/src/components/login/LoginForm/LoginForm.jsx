import React from 'react';
import * as PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { BasicInput } from '../../controls/basic';

import styles from './LoginForm.module.scss';

const LoginForm = ({
  login,
  password,
  isLogging,
  isLogged,
  disabled,
  onLogin,
  onChange,
}) => {

  const handleLogin = () => {
    if (!isLogging && !disabled && !isLogged && onLogin) {
      onLogin(login, password);
    }
  };

  return (
    <form
      name="loginForm"
      className={styles.LoginForm}
    >
      {
        isLogging && (
          <div className={styles.preloader}>Log in...</div>
        )
      }
      <fieldset className={styles.content}>
        <legend className={styles.contentTitle}>Please, log in</legend>
        <div key="loginBox" className={styles.row}>
          <label htmlFor="login">Login</label>
          <BasicInput
            id="login"
            name="login"
            tabIndex="1"
            type="text"
            autoComplete="off"
            autoFocus
            value={login}
            change={onChange}
            onEnterPress={() => { document.getElementById('password').focus(); }}
          />
        </div>
        <div key="passwordBox" className={styles.row}>
          <label htmlFor="password">Password</label>
          <BasicInput
            id="password"
            name="password"
            tabIndex="2"
            type="password"
            autoComplete="off"
            value={password}
            change={onChange}
            onEnterPress={() => { document.getElementById('btnLogin').focus(); }}
          />
        </div>
      </fieldset>
      <div className={styles.toolbar}>
        <Link to="/registration">Registration</Link>
        <button
          type="button"
          id="btnLogin"
          disabled={isLogged || isLogging || disabled}
          className={styles.toolbarButton}
          onClick={handleLogin}
        >
          Log in
        </button>
      </div>
    </form>
  )
};

LoginForm.propTypes = {
  login: PropTypes.string,
  password: PropTypes.string,
  isLogging: PropTypes.bool,
  isLogged: PropTypes.bool,
  disabled: PropTypes.bool,
  onLogin: PropTypes.func,
  onChange: PropTypes.func,
};

LoginForm.defaultProps = {
  login: '',
  password: '',
  isLogging: false,
  isLogged: false,
  disabled: false,
  onLogin: () => {},
  onChange: () => {},
};

export { LoginForm };
