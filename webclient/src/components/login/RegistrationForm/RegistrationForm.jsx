import React from 'react';
import * as PropTypes from 'prop-types';

import { BasicInput } from '../../controls/basic';

import styles from './RegistrationForm.module.scss';
import { focusById } from '../../../utils';

const RegistrationForm = ({
  login,
  email,
  password,
  confirmPassword,
  validation,
  isRegistering,
  onChange,
  onCancel,
  onSubmit,
  onValidate,
}) => {
  return (
    <form
      name="registrationForm"
      className={styles.RegistrationForm}
    >
      {
        isRegistering && (
          <div className={styles.preloader}>Registration...</div>
        )
      }
      <fieldset className={styles.content}>
        <legend className={styles.contentTitle}>
          Registration
        </legend>
        <div key="login" className={styles.row}>
          <label htmlFor="login">Login</label>
          <BasicInput
            id="login"
            name="login"
            tabIndex="1"
            type="text"
            autoComplete="off"
            autoFocus
            value={login || ''}
            validationMessage={validation.login}
            change={onChange}
            onEnterPress={() => { focusById('email'); }}
          />
        </div>
        <div key="email" className={styles.row}>
          <label htmlFor="email">E-mail</label>
          <BasicInput
            id="email"
            name="email"
            tabIndex="2"
            type="email"
            autoComplete="off"
            value={email}
            validationMessage={validation.email}
            change={onChange}
            onEnterPress={() => { focusById('password'); }}
          />
        </div>
        <div key="password" className={styles.row}>
          <label htmlFor="password">Password</label>
          <BasicInput
            id="password"
            name="password"
            tabIndex="3"
            type="password"
            autoComplete="off"
            value={password}
            validationMessage={validation.password}
            change={onChange}
            onEnterPress={() => { focusById('confirmPassword'); }}
          />
        </div>
        <div key="confirmPassword" className={styles.row}>
          <label htmlFor="confirmPassword">Confirm password</label>
          <BasicInput
            id="confirmPassword"
            name="confirmPassword"
            tabIndex="4"
            type="password"
            autoComplete="off"
            value={confirmPassword}
            validationMessage={validation.confirmPassword}
            change={onChange}
            onEnterPress={() => { focusById('btnSubmit'); }}
          />
        </div>
      </fieldset>
      <div key="toolbar" className={styles.toolbar}>
        <button
          key="btnCancel"
          id="btnCancel"
          className={styles.toolbarButton}
          tabIndex="5"
          type="button"
          onClick={onCancel}
        >
          Cancel
        </button>
        <button
          key="btnSubmit"
          id="btnSubmit"
          className={styles.toolbarButton}
          tabIndex="6"
          type="button"
          onClick={() => {
            onSubmit && onSubmit(
              {
                login,
                email,
                password,
                confirmPassword,
              },
              onValidate,
            );
          }}
        >
          Submit
        </button>
      </div>
    </form>
  )
};

RegistrationForm.propTypes = {
  login: PropTypes.string,
  email: PropTypes.string,
  password: PropTypes.string,
  confirmPassword: PropTypes.string,
  validation: PropTypes.shape({
    isValid: PropTypes.bool,
  }),
  isRegistering: PropTypes.bool,
  onChange: PropTypes.func,
  onCancel: PropTypes.func,
  onSubmit: PropTypes.func,
  onValidate: PropTypes.func,
};

RegistrationForm.defaultProps = {
  login: '',
  email: '',
  password: '',
  confirmPassword: '',
  validation: {},
  isRegistering: false,
  onChange: () => {},
  onCancel: () => {},
  onSubmit: () => {},
  onValidate: () => {},
};

export { RegistrationForm };
