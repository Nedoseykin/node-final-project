import React from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';

import NewsComponentRenderer from '../NewsComponentRenderer';
import NewsItemHeader from '../../home/News/NewsItemHeader';
import NewsComponentDataService from '../NewsComponentDataService';

import './NewsContainer.scss';

const NewsContainer = ({ activeNews }) => {
  const calculatedClassName = classNames('NewsContainer');
  const published = activeNews && activeNews.publishing_ts
    ? new Date(activeNews.publishing_ts).toLocaleDateString()
    : '';

  return activeNews && (
    <section id={activeNews.id} className={calculatedClassName}>
      <div className="section__header">
        <NewsItemHeader
          title={activeNews.title}
          description={activeNews.description}
          login={activeNews.login}
          published={published}
        />
      </div>

      <div className="section__content">
        <NewsComponentRenderer componentId={activeNews.content || -1} />
      </div>

      <NewsComponentDataService />
    </section>
  )
};

NewsContainer.propTypes = {
  activeNews: PropTypes.shape({}),
};

NewsContainer.defaultProps = {
  activeNews: { id: -1 },
};

export { NewsContainer };
