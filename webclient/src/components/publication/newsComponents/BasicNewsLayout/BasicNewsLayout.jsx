import React, { PureComponent } from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';
import { get } from 'lodash';
import { POSITIONS } from '../../../../constants';
import { isFilledString } from '../../../../utils';

import styles from './BasicNewsLayout.module.scss';

const {
  POSITION_BOTTOM,
  POSITION_TOP,
  POSITION_LEFT,
  POSITION_RIGHT
} = POSITIONS;

class BasicNewsLayout extends PureComponent{
  static propTypes = {
    editMode: PropTypes.bool,
    data: PropTypes.shape({
      textPosition: PropTypes.oneOf([
        POSITION_LEFT,
        POSITION_RIGHT,
        POSITION_TOP,
        POSITION_BOTTOM,
      ]),
      text: PropTypes.string,
      imageSrc: PropTypes.string,
    }),
  };

  static defaultProps = {
    editMode: false,
    data: null,
  };

  getDataProp = (propName, defaultValue = '') => get(this.props.data, propName, defaultValue);

  getText = () => {
    const { editMode } = this.props;
    let text = this.getDataProp('text');
    if (isFilledString(text)) {
      text = text.replace(/<[/]*script>/ig, '');
    }
    return (text || editMode) && (
      <div
        key="text"
        className={styles.newsText}
        dangerouslySetInnerHTML={{__html: text}}
      />
    )
  };

  getImage = () => {
    const { editMode } = this.props;
    const imageSrc = this.getDataProp('imageSrc');
    return (imageSrc || editMode) && (
      <div key="imageContainer" className={styles.imageContainer}>
        <img src={imageSrc} alt={'Error loading'} />
      </div>
    )
  };

  render() {
    const { editMode } = this.props;
    const textPosition = this.getDataProp('textPosition');
    const postfix = isFilledString(textPosition)
      ? textPosition.toLowerCase()
      : 'position_top';

    const calculatedClassName = classNames(
      styles.BasicNewsLayout,
      styles[`text_${postfix}`],
      editMode && styles.editMode
    );

    return (
      <div className={calculatedClassName}>
        {
          (textPosition === POSITION_BOTTOM) || (textPosition === POSITION_RIGHT)
            ? [this.getImage(), this.getText()]
            : [this.getText(), this.getImage()]
        }
      </div>
    );
  }
}

export { BasicNewsLayout };
