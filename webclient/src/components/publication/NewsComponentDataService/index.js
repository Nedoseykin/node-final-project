import { connect } from 'react-redux';

import { NewsComponentDataService } from './NewsComponentDataService';
import { authTokenSelector, dataSelector } from '../../../store/selectors';
import { request, URL } from '../../../network';
import { acSetContentItem } from '../../../store/actions';

const loadDataItem = (id, token) => (dispatch) => {
  const value = new Promise((resolve, reject) => {
    request({
      operation: 'Load news content item',
      dispatch,
      url: `${URL.NEWS_CONTENT}/${id}`,
      method: 'GET',
      headers: {
        'auth-token': token,
      },
      onError: (err) => {
        console.error(err);
        reject(err);
      },
      onSuccess: (loadedData) => {
        dispatch(acSetContentItem({
          id,
          value: loadedData,
        }));
        resolve(loadedData);
      },
    });
  });
  dispatch(acSetContentItem({ id, value }));
};

const mapStateToProps = state => ({
  authToken: authTokenSelector(state),
  data: dataSelector(state),
});

const mapDispatchToProps = {
  loadDataItem,
};

export default connect(mapStateToProps, mapDispatchToProps)(NewsComponentDataService);
