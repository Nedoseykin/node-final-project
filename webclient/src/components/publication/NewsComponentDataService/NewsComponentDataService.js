import { PureComponent } from 'react';
import * as PropTypes from 'prop-types';

class NewsComponentDataService extends PureComponent{
  static propTypes = {
    data: PropTypes.shape({}),
    authToken: PropTypes.string,
    loadDataItem: PropTypes.func,
  };

  static defaultProps = {
    data: {},
    authToken: null,
    loadDataItem: () => {},
  };

  componentDidMount() {
    this.updateData();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    this.updateData();
  }

  updateData = () => {
    const {
      data,
      authToken,
      loadDataItem,
    } = this.props;

    Object.keys(data)
      .forEach((key) => {
        const item = data[key];
        if (item === null) {
          loadDataItem(key, authToken);
        }
      });
  };

  render() {
    return null;
  }
}

export { NewsComponentDataService };
