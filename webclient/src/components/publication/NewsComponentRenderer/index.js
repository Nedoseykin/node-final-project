import { connect } from 'react-redux';

import { NewsComponentRenderer } from './NewsComponentRenderer';
import { dataItemRendererSelector } from '../../../store/selectors';
import { acAddContentItem } from '../../../store/actions';

const initItemLoad = (id) => (dispatch) => {
  dispatch(acAddContentItem({ id }));
};

const mapStateToProps = (state, ownProps) => {
  const { componentId } = ownProps;
  // console.log('NewsComponentRenderer: mapStateToProps: ownProps: ', ownProps);
  state.publications.dataId = componentId;
  const { component, props, loadStatus } = dataItemRendererSelector(state);

  return {
    loadStatus,
    props,
    component,
  }
};

const mapDispatchToProps = {
  initItemLoad,
};

export default connect(mapStateToProps, mapDispatchToProps)(NewsComponentRenderer);
