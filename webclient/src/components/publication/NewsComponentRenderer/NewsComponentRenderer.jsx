import React, { PureComponent } from 'react';
import * as PropTypes from 'prop-types';

class NewsComponentRenderer extends PureComponent {
  static propTypes = {
    componentId: PropTypes.number.isRequired,
    component: PropTypes.any,
    props: PropTypes.shape({}),
    loadStatus: PropTypes.number,
    initItemLoad: PropTypes.func,
  };

  static defaultProps = {
    component: null,
    props: {},
    loadStatus: 2,
    initItemLoad: () => {},
  };

  componentDidMount() {
    this.updateData();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    this.updateData();
  }

  updateData = () => {
    const {
      componentId,
      loadStatus,
      initItemLoad,
    } = this.props;

    if (loadStatus === 0) {
      initItemLoad(componentId);
    }
  };

  render() {
    const {
      component: NewsComponent,
      props: newsComponentProps,
    } = this.props;

    // console.log('RENDERER: component: ', NewsComponent);
    // console.log('RENDERER: props: ', newsComponentProps);

    return NewsComponent && (
      <NewsComponent data={newsComponentProps} />
    )
  }
}

export { NewsComponentRenderer };
