import React, { useState, useEffect } from 'react';
import * as PropTypes from 'prop-types';

import BasicSelect from '../../basic/BasicSelect';

import './PersonalImagesSelect.scss';
import { isFilledArray } from '../../../../utils';

const PersonalImagesSelect = ({
  authToken,
  loadImages,
  ...rest
}) => {
  const [items, setItems] = useState([]);
  const [loadStatus, setLoadStatus] = useState(0);

  useEffect(() => {
    if (loadStatus === 0) {
      setLoadStatus(1);
      loadImages(authToken)
        .then((data) => {
          const dataItems = isFilledArray(data)
            ? data.map(item => ({
                id: `/images/${item.curr_name}`,
                label: item.title,
              }))
            : [];
          setLoadStatus(2);
          setItems(dataItems);
        });
    }
  }, [loadStatus, authToken, loadImages]);

  return (
    <BasicSelect
      {...rest}
      items={items}
    />
  )
};

PersonalImagesSelect.propTypes = {
  authToken: PropTypes.string,
  loadImages: PropTypes.func,
  rest: PropTypes.shape({}),
};

PersonalImagesSelect.defaultProps = {
  authToken: null,
  loadImages: () => {},
  rest: {},
};

export { PersonalImagesSelect };
