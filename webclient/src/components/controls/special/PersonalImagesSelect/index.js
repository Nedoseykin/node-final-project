import { connect } from 'react-redux';

import request from '../../../../network/request';
import { PersonalImagesSelect } from './PersonalImagesSelect';
import { authTokenSelector } from '../../../../store/selectors';
import { URL } from '../../../../network';

const loadImages = (token) => (dispatch) => {
  return request({
    operation: 'loading of personal images',
    dispatch,
    url: URL.PERSONAL_GALLERY,
    headers: {
      'auth-token': token,
    },
    onError: (err) => { console.error(err) },
    onSuccess: (loadedData) => {
      return loadedData;
    },
  });
};

const mapStateToProps = state => ({
  authToken: authTokenSelector(state),
});

const mapDispatchToProps = {
  loadImages,
};

export default connect(mapStateToProps, mapDispatchToProps)(PersonalImagesSelect);
