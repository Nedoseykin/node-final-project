import React, { useState } from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';
import { get } from 'lodash';

import { BasicInput } from '../../basic';
import { Search } from '../../../icons';

import './SearchInput.scss';
import { isFilledString } from '../../../../utils';

const SearchInput = ({
  classes,
  value,
  isOpen,
  onChange,
  onEnterPress,
  ...rest
}) => {
  const [searchIsOpen, setSearchIsOpen] = useState(
    isOpen || isFilledString(value)
  );

  const classRoot = get(classes, 'root', '');
  const classInput = get(classes, 'input', '');
  const classIcon = get(classes, 'icon', '');

  return (
    <div
      className={classNames(
        'SearchInput',
        isFilledString(classRoot) && classRoot
      )}
    >
      {
        searchIsOpen ? (
          <BasicInput
            className={classNames('SearchInput__input', isFilledString(classInput) && classInput)}
            value={value}
            onChange={onChange}
            onEnterPress={onEnterPress}
            onBlur={() => {
              if (!value) { setSearchIsOpen(false); }
            }}
            {...rest}
          />
        ) : (
          <span
            className={classNames(
              'SearchInput__icon',
              isFilledString(classIcon) && classIcon,
            )}
            onClick={() => { setSearchIsOpen(true); }}
          >
            <Search />
          </span>
        )
      }
    </div>
  );
};

SearchInput.propTypes = {
  classes: PropTypes.shape({
    root: PropTypes.string,
    input: PropTypes.string,
    icon: PropTypes.string,
  }),
  value: PropTypes.string,
  onChange: PropTypes.func,
  onEnterPress: PropTypes.func,
};

SearchInput.defaultProps = {
  classes: {},
  value: '',
  onChange: () => {},
  onEnterPress: () => {},
};

export { SearchInput };
