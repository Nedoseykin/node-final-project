import React from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';

import './TogglerSet.scss';

import { Toggler } from '../../labeled';
import { isFilledArray, isFilledString } from '../../../../utils';

const TogglerSet = ({
  classes,
  label,
  value,
  name,
  items,
  disabled,
  onChange,
}) => {
  const togglers = isFilledArray(items)
    ? items.map((item) => {
        const {
          index,
          letter,
          label: itemLabel,
        } = item;

        let itemValue = 0;

        const re = new RegExp(`${letter}(\\d*)`);
        const arr = re.exec(value);

        if (arr) {
          itemValue = parseInt(arr[1], 10);
        }

        const bitValue = Math.pow(2, index);

        const onChangeItemValue = () => {
          itemValue = itemValue ^ bitValue;
          let newValue = isFilledArray(arr)
            ? value.replace(re, `${letter}${itemValue}`)
            : `${value}${letter}${itemValue}`;
          onChange && onChange({ target: { name, value: newValue } });
        };

        return (
          <Toggler
            key={index}
            label={itemLabel}
            value={Boolean(itemValue & bitValue)}
            name={name}
            disabled={disabled}
            onChange={onChangeItemValue}
          />
        )
      })
    : [];

  const getClassFromClasses = (selector) => {
    return classes && isFilledString(classes[selector]) ? classes[selector] : '';
  };

  const calculatedClassName = classNames(
    'TogglerSet',
    getClassFromClasses('root'),
    disabled && 'TogglerSet__disabled',
  );

  return (
    <div className={calculatedClassName}>
      <label className="TogglerSet__label">
        {label}
      </label>
      <div className={
        classNames(
          'TogglerSet__content',
          getClassFromClasses('content'),
        )
      }>
        {togglers}
      </div>
    </div>
  )
};

TogglerSet.propTypes = {
  classes: PropTypes.shape({
    root: PropTypes.string,
    content: PropTypes.string,
  }),
  label: PropTypes.string,
  value: PropTypes.string,
  items: PropTypes.arrayOf(PropTypes.shape({})),
  disabled: PropTypes.bool,
  onChange: PropTypes.func,
};

TogglerSet.defaultProps = {
  className: '',
  label: '',
  value: 0,
  items: [],
  disabled: false,
  onChange: () => {},
};

export { TogglerSet };
