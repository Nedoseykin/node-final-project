import React from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';
import { get } from 'lodash';

import './ItemsList.scss';

import { BasicList, BasicListItem } from '../../../controls/basic';
import { isFilledArray, isFilledString } from '../../../../utils';

const ItemsList = ({
  items,
  value,
  classes,
  onItemClick,
  onItemDoubleClick,
  onWheelLimit,
  asName,
  asId,
}) => {
  const rootClass = get(classes, 'root', '');
  const itemClass = get(classes, 'item', '');

  const listItems = isFilledArray(items)
    ? items.map((item) => {
      const { [asName]: name, [asId]: id } = item;
      return (
        <BasicListItem
          key={id}
          className={classNames('listItem', isFilledString(itemClass) && itemClass)}
          value={id}
          isSelected={value === id}
          disabled={false}
          onClick={() => { onItemClick(id) }}
          onDoubleClick={() => { onItemDoubleClick(id) }}
        >
          {name}
        </BasicListItem>
      )
    })
    : [];

  return (
    <BasicList
      className={classNames('ItemsList', isFilledString(rootClass) && rootClass)}
      onWheelLimit={onWheelLimit}
    >
      {listItems}
    </BasicList>
  )
};

ItemsList.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({})),
  value: PropTypes.number,
  classes: PropTypes.shape({
    root: PropTypes.string,
    item: PropTypes.string,
  }),
  asName: PropTypes.string,
  asId: PropTypes.string,
  onItemClick: PropTypes.func,
  onItemDoubleClick: PropTypes.func,
  onWheelLimit: PropTypes.func,
};

ItemsList.defaultProps = {
  items: [],
  value: -1,
  classes: {
    root: '',
    item: '',
  },
  asName: 'name',
  asId: 'id',
  onItemClick: () => {},
  onItemDoubleClick: () => {},
  onWheelLimit: () => {},
};

export { ItemsList };
