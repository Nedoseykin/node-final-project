import React, { useState, useRef } from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';
import { get } from 'lodash';

// import MarkdownIt from 'markdown-it';
// import MdEditor from 'react-markdown-editor-lite';

import JoditEditor from 'jodit-react';

import { BasicInput } from '../../basic';
import { ToolbarButton } from '../../../AppContent';
import { BasicModal } from '../../../modal';

import './HtmlEditorInput.scss';
import { isFilledString } from '../../../../utils';

const HtmlEditorInput = ({
  classes,
  name,
  value,
  onChange,
  ...rest
}) => {
  const [isOpen, setIsOpen] = useState(false);

  const editor = useRef(null);
  const [content, setContent] = useState(value);

  const config = {
    readonly: false,
    height: '100%',
    allowResizeY: false,
  };

  const rootClass = get(classes, 'root', '');
  const inputClass = get(classes, 'input', '');
  const buttonClass = get(classes, 'button', '');

  return (
    <div
      className={classNames(
        'HtmlEditorInput',
        isFilledString(rootClass) && rootClass,
      )}
    >
      <BasicInput
        className={classNames(
          'HtmlEditorInput__input',
          isFilledString(inputClass) && inputClass,
        )}
        name={name}
        value={value}
        onChange={onChange}
        {...rest}
      />
      <ToolbarButton
        className={classNames(
          'HtmlEditorInput__button',
          isFilledString(buttonClass) && buttonClass,
        )}
        onClick={() => { setIsOpen(true) }}
      >
        HTML
      </ToolbarButton>
      {
        isOpen && (
          <BasicModal
            className="HtmlEditorModal"
            onClick={(e) => {
              e.stopPropagation();
              setIsOpen(false);
            }}
          >
            <form
              className="HtmlEditorForm"
              name="htmlEditor"
              onClick={(e) => {
                e.stopPropagation();
              }}
            >
              <fieldset className="content">
                <legend className="contentTitle">HTML Editor</legend>
                <div className="row editor">
                  <JoditEditor
                    ref={editor}
                    value={content}
                    config={config}
                    tabIndex={1}
                    onBlur={(newContent) => { setContent(newContent); }}
                  />
                </div>
              </fieldset>
              <div
                key="toolbar"
                className="toolbar"
              >
                <ToolbarButton
                  key="cancelBtn"
                  onClick={() => { setIsOpen(false) }}
                >
                  Cancel
                </ToolbarButton>

                <ToolbarButton
                  key="okBtn"
                  onClick={() => {
                    onChange && onChange({ target: {
                      name, value: content,
                    } });
                    setIsOpen(false);
                  }}
                >
                  Ok
                </ToolbarButton>
              </div>
            </form>
          </BasicModal>
        )
      }
    </div>
  )
};

HtmlEditorInput.propTypes = {
  classes: PropTypes.shape({
    root: PropTypes.string,
    input: PropTypes.string,
    button: PropTypes.string,
  }),
};

HtmlEditorInput.defaultProps = {
  classes: {},
};

export { HtmlEditorInput };
