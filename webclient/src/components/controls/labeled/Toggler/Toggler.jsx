import React from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';

import { BasicToggler } from '../../basic';
import { isFilledString } from '../../../../utils';

import './Toggler.scss';

const Toggler = ({
  id,
  className,
  label,
  value,
  name,
  disabled,
  onChange,
  validationMessage,
  ...rest
}) => {
  const handleLabelClick = (e) => {
    const toggler = e.target
      .parentElement
      .querySelector('.BasicToggler > div');
    if (toggler) {
      toggler && toggler.focus();
      toggler && toggler.click();
    }
  };

  const calculatedClassName = classNames(
    'Toggler',
    value && 'Toggler__active',
    isFilledString(className) && className,
    disabled && 'Toggler__disabled',
  );

  return (
    <span id={id} className={calculatedClassName}>
      <label
        onClick={handleLabelClick}
      >
        {label}
      </label>
      <BasicToggler
        id={`id-${BasicToggler}`}
        value={value}
        name={name}
        disabled={disabled}
        onChange={onChange}
        validationMessage={validationMessage}
        {...rest}
      />
    </span>
  )
};

Toggler.propTypes = {
  id: PropTypes.string,
  className: PropTypes.string,
  label: PropTypes.string,
  value: PropTypes.bool,
  name: PropTypes.string,
  disabled: PropTypes.bool,
  onChange: PropTypes.func,
};

Toggler.defaultProps = {
  className: '',
  label: '',
  value: false,
  name: '',
  disabled: false,
  onChange: () => {},
};

export { Toggler };
