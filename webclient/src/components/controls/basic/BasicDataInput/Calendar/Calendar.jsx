import React, { PureComponent } from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';

import './Calendar.scss';
import {
  addCharToLength,
  getFirstDayFromMonday,
  getLastDateInMonth,
  getLastDayFromMonday, getMonthNameEng,
  isFilledString,
} from '../../../../../utils';

class Calendar extends PureComponent {
  static propTypes = {
    value: PropTypes.oneOfType([
      PropTypes.number, PropTypes.string,
    ]),
    valueIsUTC: PropTypes.bool,
    className: PropTypes.string,
    ownRef: PropTypes.func,
    onChange: PropTypes.func,
  };

  static defaultProps = {
    value: null,
    valueIsUTC: true,
    className: '',
    ownRef: () => {},
    onChange: () => {},
  };

  constructor(props) {
    super(props);
    const { value, valueIsUTC } = props;
    let d = new Date(value).getTime();
    d = value && !isNaN(d)
      ? new Date(value)
      : new Date();
    if (value && valueIsUTC) {
      const offset = d.getTimezoneOffset();
      const min = d.getMinutes();
      d.setMinutes(min - offset);
    }

    this.state = {
      month: d.getMonth(),
      year: d.getFullYear(),
    };
  }

  handleMonthButtonClick = (e) => {
    const { month, year } = this.state;
    const { id } = e.target;
    const delta = id === 'prev' ? -1 : 1;
    let newMonth = month + delta;
    let newYear = year;
    if (newMonth > 11) {
      newMonth = 0;
      newYear++;
    } else if (newMonth < 0) {
      newMonth = 11;
      newYear--;
    }
    this.setState({ month: newMonth, year: newYear });
  };

  getDays = () => {
    let { value, valueIsUTC, onChange } = this.props;
    const { month, year } = this.state;
    value = value && !isNaN(new Date(value).getTime())
      ? new Date(value)
      : new Date();

    if (this.props.value && valueIsUTC) {
      const min = value.getMinutes();
      const offset = value.getTimezoneOffset();
      value.setMinutes(min - offset);
    }

    // const year = value.getFullYear();
    // const month = value.getMonth();
    const date = value.getDate();

    const lastDate = getLastDateInMonth(month, year);
    const firstDay = getFirstDayFromMonday(month, year);
    const startDelta = firstDay - 1;
    const lastDay = getLastDayFromMonday(month, year);
    const endDelta = 7 - lastDay;
    const start = new Date(year, month, 1 - startDelta).getTime();
    const end = new Date(year, month, lastDate + endDelta).getTime();
    const daysCount = (end - start) / (86400000) + 1;
    const weeksCount = daysCount / 7;

    const weeks = [];
    const weekDayNames = ['Mn', 'Tu', 'We', 'Th', 'Fi', 'Sa', 'Sn']
      .map(d => (
        <div key={d} className={classNames('weekDayName')}>
          {d}
        </div>
      ));
    weeks.push(weekDayNames);
    for (let w = 0; w < weeksCount; w++) {
      const week = [];
      for (let d = 0; d < 7; d++) {
        const addTime = (d + w * 7) * 86400000;
        const fullTime = start + addTime;
        let dayValue = new Date(fullTime).getDate();
        const selected = (dayValue === date) && (new Date(fullTime).getMonth() === month);
        dayValue = addCharToLength(`${dayValue}`, 2);
        week.push(
          <div
            key={d}
            className={classNames('weekDay', selected && 'weekDay__selected')}
            onClick={() => {
              onChange && onChange(fullTime);
            }}
          >
            {dayValue}
          </div>
        );
      }
      weeks.push(week);
    }

    return weeks.map((week, i) => (
      <div key={i} className="monthWeek">
        {week}
      </div>
    ));
  };

  getYearAndMonth = () => {
    const { month, year } = this.state;
    return { year, month: getMonthNameEng(month) };
  };

  render() {
    const {
      className,
      ownRef,
      // onChange,
    } = this.props;

    const { year, month } = this.getYearAndMonth();

    const calculatedClassName = classNames(
      'Calendar',
      isFilledString(className) && className,
    );

    return (
      <div
        className={calculatedClassName}
        ref={ownRef}
        onClick={(e) => { e.stopPropagation(); }}
      >
        <div key="header" className="Calendar__header">
          <div key="year" className="header__label">
            {year}
          </div>

          <div key="month" className="header__label">
            <span
              key="prev"
              id="prev"
              className="month__button"
              onClick={this.handleMonthButtonClick}
            >
              {'<<'}
            </span>
            <span key="month" className="month__label">
              {month}
            </span>
            <span
              key="prev"
              id="next"
              className="month__button"
              onClick={this.handleMonthButtonClick}
            >
              {'>>'}
            </span>
          </div>
        </div>
        <div key="days" className="Calendar__days">
          {this.getDays()}
        </div>
      </div>
    );
  }
}

export { Calendar };
