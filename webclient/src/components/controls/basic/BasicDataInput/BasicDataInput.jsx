import React, { PureComponent } from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';

import { BasicModal } from '../../../modal';
import Calendar from './Calendar';

import './BasicDataInput.scss';

import {
  addCharToLength,
  getMonthNameEng,
  isFilledString,
} from '../../../../utils';

class BasicDataInput extends PureComponent {
  static propTypes = {
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    className: PropTypes.string,
    value: PropTypes.oneOfType([
      PropTypes.number, PropTypes.string,
    ]),
    validationMessage: PropTypes.string,
    disabled: PropTypes.bool,
    valueIsUTC: PropTypes.bool,
    onChange: PropTypes.func,
  };

  static defaultProps = {
    className: '',
    value: '',
    validationMessage: '',
    disabled: false,
    valueIsUTC: true,
    onChange: () => {},
  };

  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
    };
    this.calendar = null;
    this.input = null;
  }

  componentDidMount() {
    this.state.isOpen && this.update();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    this.state.isOpen && this.update();
  }

  update = () => {
    const { isOpen } = this.state;
    if (isOpen && this.input && this.calendar) {
      const { left, top, height } = this.input.getBoundingClientRect();
      this.calendar.style.top = `${top + height}px`;
      this.calendar.style.left = `${left}px`;
    }
  };

  getValue = (v, isUTC) => {
    let result = '';
    let d = new Date(v);
    if (v && !isNaN(d.getTime())) {
      if (isUTC) {
        const offset = d.getTimezoneOffset();
        const min = d.getMinutes();
        d.setMinutes(min - offset);
      }
      const y = d.getFullYear();
      const m = getMonthNameEng(d.getMonth());
      const dt = addCharToLength(`${d.getDate()}`, 2);
      result = `${dt} ${m} ${y}`;
    }

    return result;
  };

  toggleOpen = () => {
    const { isOpen } = this.state;
    this.setState({ isOpen: !isOpen });
  };

  handleDateObjChange = (numValue) => {
    const { onChange, name } = this.props;
    const value = numValue
      ? new Date(numValue)
        .toISOString()
        .split(/[TZ]/i)
        .filter(Boolean)
        .join(' ')
      : null;

    onChange && onChange({ target: { name, value } });
  };

  render() {
    const {
      id,
      name,
      value,
      valueIsUTC,
      className,
      disabled,
      validationMessage
    } = this.props;

    const { isOpen } = this.state;

    const calculatedClassName = classNames(
      'BasicDataInput',
      isFilledString(className) && className,
      isFilledString(validationMessage) && 'BasicDataInput__invalid',
      disabled && 'BasicDataInput__disabled'
    );

    return (
      <>
        <input
          id={id}
          name={name}
          className={calculatedClassName}
          ref={(elm) => { this.input = elm; }}
          type="text"
          autoComplete="off"
          disabled={disabled === true}
          value={this.getValue(value, valueIsUTC)}
          onClick={disabled ? null : this.toggleOpen}
        />
        {
          isOpen && (
            <BasicModal
              onClick={(e) => {
                e.stopPropagation();
                this.toggleOpen();
              }}
            >
              <Calendar
                value={value}
                valueIsUTC={valueIsUTC}
                ownRef={(elm) => { this.calendar = elm }}
                onChange={this.handleDateObjChange}
              />
            </BasicModal>
          )
        }
      </>
    );
  }
}

export { BasicDataInput };
