import React from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';
import { isFilledString } from '../../../../utils';

import './BasicToggler.scss';

const BasicToggler = ({
  id,
  className,
  name,
  value,
  validationMessage,
  disabled,
  tabIndex,
  onChange,
  ...rest
}) => {
  const calculatedClassName = classNames(
    'BasicToggler',
    value && 'BasicToggler__active',
    isFilledString(className) && className,
    isFilledString(validationMessage) && 'BasicToggler__invalid',
    disabled && 'BasicToggler__disabled',
  );

  const handleChange = () => {
    const target = { name, value: !value };
    onChange && onChange({ target });
  };

  return (
    <div id={id} className={calculatedClassName}>
      <div
        className="BasicToggler__scale"
        tabIndex={tabIndex}
        onClick={disabled ? null : handleChange}
        {...rest}
      >
        <div
          className="BasicToggler__handler"
        />
      </div>
    </div>
  )
};

BasicToggler.propTypes = {
  id: PropTypes.string,
  className: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.bool,
  disabled: PropTypes.bool,
  tabIndex: PropTypes.number,
  onChange: PropTypes.func,
};

BasicToggler.defaultProps = {
  id: '',
  className: '',
  name: '',
  value: false,
  disabled: false,
  tabIndex: 0,
  onChange: () => {},
};

export { BasicToggler };
