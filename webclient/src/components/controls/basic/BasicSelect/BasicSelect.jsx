import React, { PureComponent } from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';
import {isExists, isFilledArray, isFilledString} from "../../../../utils";
import { BasicModal } from '../../../modal';
import { TriangleArrowLeft } from '../../../icons';

import './BasicSelect.scss';

// items: [{ id, label }]
class BasicSelect extends PureComponent {
  static propTypes = {
    name: PropTypes.string,
    id: PropTypes.string,
    className: PropTypes.string,
    items: PropTypes.arrayOf(PropTypes.shape({})),
    value: PropTypes.oneOfType([ PropTypes.number, PropTypes.string ]),
    asId: PropTypes.string,
    asLabel: PropTypes.string,
    disabled: PropTypes.bool,
    validationMessage: PropTypes.string,
    onChange: PropTypes.func,
  };

  static defaultProps = {
    name: '',
    id: '',
    className: '',
    items: [],
    value: -1,
    asId: 'id',
    asLabel: 'label',
    disabled: false,
    validationMessage: '',
    onChange: () => {},
  };

  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
    };
  }

  componentDidMount() {
    this.update();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    this.update();
  }

  update = () => {
    let inputWidth = 300;
    let inputLeft = 0;
    let inputTop = 0;
    let inputHeight = 0;

    if (this.input) {
      inputWidth = this.input.offsetWidth;
      ({
        left: inputLeft,
        top: inputTop,
        height: inputHeight,
      } = this.input.getBoundingClientRect());
    }
    if (this.optionsBox) {
      this.optionsBox.style.width = `${inputWidth}px`;
      this.optionsBox.style.left = `${inputLeft}px`;
      this.optionsBox.style.top = `${inputTop + inputHeight}px`;
    }
  };

  toggleOpen = () => {
    const { isOpen } = this.state;
    this.setState({ isOpen: !isOpen });
  };

  getValue = () => {
    const { items, value, asId, asLabel } = this.props;

    let result = '';
    if (isFilledArray(items) && isExists(value)) {
      const item = items.find(v => v[asId] === value);
      if (item) { result = item[asLabel]; }
    }
    return result;
  };

  getOptions = () => {
    const {
      items,
      asId,
      asLabel,
      onChange,
      value,
      name,
    } = this.props;

    return isFilledArray(items)
      ? items.map((item) => {
        const {[asId]: id, [asLabel]: label} = item;
        const isSelected = id === value;

        return (
          <li
            key={id}
            className={classNames(
              'BasicSelect__option',
              isSelected && 'BasicSelect__option__selected',
            )}
            onClick={(e) => {
              e.target.name = name;
              e.target.value = id;
              e.option = { ...item };
              e.value = id;
              onChange(e);
            }}
          >
            {label}
          </li>
        )
      })
      : [];
  };

  render () {
    const {
      className,
      disabled,
      name,
      id,
      validationMessage,
    } = this.props;

    const { isOpen } = this.state;

    const options = this.getOptions();

    const calculatedClassName = classNames(
      'BasicSelect',
      isFilledString(className) && className,
      isFilledString(validationMessage) && 'BasicSelect__invalid',
      disabled && 'BasicSelect__disabled',
    );

    return (
      <div
        className={calculatedClassName}
      >
        <input
          id={id}
          name={name}
          className="BasicSelect__input"
          type="text"
          value={this.getValue()}
          autoComplete="off"
          disabled={disabled === true}
          onClick={disabled ? null : this.toggleOpen}
          onChange={() => {}}
          ref={(elm) => { this.input = elm; }}
        />
        <span
          className={classNames('BasicSelect__icon_box')}
          onClick={disabled ? null : this.toggleOpen}
        >
          <TriangleArrowLeft
            className={classNames(
              'BasicSelect__icon',
              isOpen && 'BasicSelect__icon__open',
            )}
          />
        </span>
        {
          isOpen && isFilledArray(options) && (
            <BasicModal
              onClick={this.toggleOpen}
            >
              <ul
                className="BasicSelect__options_box"
                ref={(elm) => { this.optionsBox = elm; }}
              >
                {options}
              </ul>
            </BasicModal>
          )
        }
      </div>
    );
  }
}

export { BasicSelect };
