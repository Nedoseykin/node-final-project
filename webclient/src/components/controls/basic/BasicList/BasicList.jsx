import React, { PureComponent } from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';

import './BasicList.scss';
import { isFilledString } from '../../../../utils';

class BasicList extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    children: PropTypes.arrayOf(PropTypes.node),
    disabled: PropTypes.bool,
    onWheelLimit: PropTypes.func,
  };

  static defaultProps = {
    className: '',
    children: [],
    disabled: false,
    onWheelLimit: () => {},
  };

  constructor(props) {
    super(props);
    this.list = null;
  }

  componentDidMount() {
    this.setup();
  }

  componentWillUnmount() {
    this.unmount();
  }

  setup = () => {
    if (this.list) {
      this.list.addEventListener('wheel', this.onListWheel, false);
    }
  };

  unmount = () => {
    if (this.list) {
      this.list.removeEventListener('wheel', this.onListWheel);
    }
  };

  onListWheel = (e) => {
    const { onWheelLimit } = this.props;
    const {
      deltaY,
      target,
    } = e;
    const {
      scrollHeight,
      offsetHeight,
      scrollTop,
    } = target.parentElement;

    const scrollBottom = scrollHeight - offsetHeight - scrollTop;

    if (
      ((deltaY > 0) && (scrollBottom === 0))
      || ((deltaY < 0) && (scrollTop === 0))
    ) {
      onWheelLimit && onWheelLimit({ deltaYSign: Math.sign(deltaY) });
    }
  };

  render() {
    const {
      className,
      children,
      disabled,
    } = this.props;

    const calculatedClassName = classNames(
      'BasicList',
      isFilledString(className) && className,
      disabled && 'BasicList__disabled',
    );

    return (
      <ul
        className={calculatedClassName}
        ref={(elm) => { this.list = elm; }}
      >
        {children}
      </ul>
    )
  }
}

export { BasicList };
