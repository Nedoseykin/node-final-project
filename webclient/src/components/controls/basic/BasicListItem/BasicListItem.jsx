import React from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';

import './BasicListItem.scss';

import { isFilledString } from '../../../../utils';

const BasicListItem = ({
  className,
  value,
  children,
  isSelected,
  disabled,
  onClick,
  onDoubleClick,
}) => {
  const calculatedClassName = classNames(
    'BasicListItem',
    isFilledString(className) && className,
    isSelected && 'BasicListItem__selected',
    disabled && 'BasicListItem__disabled',
  );

  return (
    <li
      className={calculatedClassName}
      onClick={disabled || !onClick ? null : () => onClick(value)}
      onDoubleClick={disabled || !onDoubleClick ? null : () => onDoubleClick(value)}
    >
      {children}
    </li>
  )
};

BasicListItem.propTypes = {
  className: PropTypes.string,
  value: PropTypes.any,
  children: PropTypes.any,
  isSelected: PropTypes.bool,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
  onDoubleClick: PropTypes.func,
};

BasicListItem.defaultProps = {
  className: '',
  value: null,
  children: [],
  isSelected: false,
  disabled: false,
  onClick: () => {},
  onDoubleClick: () => {},
};

export { BasicListItem };
