import React from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';

import './BasicInput.scss';

import { isFilledString } from '../../../../utils';

const BasicInput = ({
  className,
  type,
  value,
  isValid,
  disabled,
  validationMessage,
  change,
  onEnterPress,
  ...rest
}) => {
  const handleKeyDown = (e) => {
    const { target, keyCode, which } = e;
    const code = keyCode || which;
    if (code === 13) {
      change(e);
      onEnterPress && onEnterPress(target);
    }
  };

  const calculatedClassName = classNames(
    'BasicInput',
    (isValid === 'valid') && 'BasicInput__valid',
    (isValid === 'invalid') && 'BasicInput__invalid',
    isFilledString(className) && className,
    isFilledString(validationMessage) && 'BasicInput__invalid',
    disabled && 'BasicInput__disabled',
  );

  const displayedValue = isFilledString(value)
    ? value
    : '';

  return (
    <input
      className={calculatedClassName}
      type={type}
      value={displayedValue}
      disabled={disabled}
      onChange={disabled ? null : change}
      onKeyDown={handleKeyDown}
      {...rest}
    />
  )
};

BasicInput.propTypes = {
  className: PropTypes.string,
  type: PropTypes.string,
  value: PropTypes.any,
  disabled: PropTypes.bool,
  validationMessage: PropTypes.string,
  isValid: PropTypes.oneOf(['default', 'valid', 'invalid']),
  change: PropTypes.func,
  onEnterPress: PropTypes.func,
};

BasicInput.defaultProps = {
  className: '',
  type: 'text',
  value: '',
  disabled: false,
  validationMessage: '',
  isValid: 'default',
  change: () => {},
  onEnterPress: () => {},
};

export { BasicInput };
