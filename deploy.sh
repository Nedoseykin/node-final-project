#!/bin/bash
echo "Deploy started..."
cd  /home/doc/dev/node-final/node-final-project/
echo "- stop back-end server"
node  ./server/killer.js
echo "- pull git repository"
git pull
echo "- update webclient dependencies"
cd  /home/doc/dev/node-final/node-final-project/webclient
npm install
echo "- create production build"
npm run build
echo "- update server dependencies"
cd ../server
npm install
echo "- start back-end server in production mode"
npm run prod&
cd ..
echo "Enjoy!"

