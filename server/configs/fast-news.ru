limit_req_zone $binary_remote_addr zone=Zone_1rsClient:1m rate=1r/s;

server {

        client_max_body_size 15M;

        gzip on;

        gzip_disable "msie6";

        gzip_min_length 512;

        gzip_buffers 4 8k;

        gzip_types text/plain text/xml application/xml text/css application/x-javascript application/javascript application/json text/javascript;

        gzip_comp_level 9;

	gzip_static on;

        access_log      /home/doc/dev/node-final/node-final-project/server/logs/access.log;
        error_log       /home/doc/dev/node-final/node-final-project/server/logs/error.log;

        index index.html index.htm index.nginx-debian.html;

        server_name fast-news.ru www.fast-news.ru;

	location ~* ^.+\.(ogg|ogv|svg|svgz|eot|otf|woff|mp4|ttf|rss|atom|jpg|jpeg|gif|png|ico|zip|tgz|gz|rar|bz2|doc|xls|exe|ppt|tar|mid|midi|wav|bmp|rtf)$ {
		expires max;
		etag on;
	}

        location /api/v1 {
		proxy_pass    http://127.0.0.1:8780;

		limit_req zone=Zone_1rsClient burst=10;
		etag on;
        }

        location /auth {
		proxy_pass      http://127.0.0.1:8780;

		limit_req zone=Zone_1rsClient burst=10;
        }


        location /uploads {
                root  /home/doc/dev/node-final/node-final-project/server;
		expires 1h;
		try_files $uri /$uri =404;

		limit_req zone=Zone_1rsClient burst=10;
        }

        location /images/ {
                rewrite ^/images(.*)$ /uploads/$1;
		etag on;
		limit_req zone=Zone_1rsClient burst=10;
        }

	location /admin {
		rewrite ^/admin(.*)$ /;

		limit_req zone=Zone_1rsClient burst=10;
		etag on;
	}

	location /login {
		rewrite /login /;

		limit_req zone=Zone_1rsClient burst=10;
	}

	location /personal {
		rewrite ^/personal(.*)$ /;
		# rewrite ^/personal / permanent;
		etag on;

		try_files $uri /$uri index.html /index.html =404;

		limit_req zone=Zone_1rsClient burst=10;
	}

        location / {
                root    /home/doc/dev/node-final/node-final-project/server/resources/build;
                try_files $uri /index.html =404;
		etag on;
		limit_req zone=Zone_1rsClient burst=10;
        }

    listen [::]:443 ssl ipv6only=on; # managed by Certbot
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/fast-news.ru/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/fast-news.ru/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot


}


server {
    if ($host = www.fast-news.ru) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


    if ($host = fast-news.ru) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


        listen 80;
        listen [::]:80;

        server_name fast-news.ru www.fast-news.ru;
    return 404; # managed by Certbot




}
