CREATE USER 'fastNewsAdmin'@'localhost' IDENTIFIED BY 'fastNewsAdminPassword';

GRANT SHOW DATABASES  ON *.* TO 'fastNewsAdmin'@'localhost';

GRANT ALL PRIVILEGES  ON `fast\_news`.* TO 'fastNewsAdmin'@'localhost' WITH GRANT OPTION;

FLUSH PRIVILEGES;

SHOW GRANTS FOR 'fastNewsAdmin'@'localhost';
