CREATE TABLE `user_roles` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NOT NULL,
	`privileges` VARCHAR(50) NOT NULL DEFAULT 'u0r0i0n0',
	PRIMARY KEY (`id`)
)
COLLATE='utf8mb4_0900_ai_ci';

/* ALTER TABLE `user_roles`
	CHANGE COLUMN `privileges` `privileges` SMALLINT UNSIGNED NOT NULL DEFAULT 0 AFTER `name`; */

/* ALTER TABLE `user_roles`
	CHANGE COLUMN `privileges` `privileges` VARCHAR(50) NOT NULL DEFAULT 'u0r0i0n0' AFTER `name`; */

UPDATE `user_roles` SET `privileges`='u10r10i10n10' WHERE  `id`=1;

UPDATE `user_roles` SET `privileges`='u0r0i0n0' WHERE  `id`=5;

CREATE TABLE `users` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`login` VARCHAR(50) NOT NULL,
	`email` VARCHAR(50) NOT NULL,
	`password` VARCHAR(100) NOT NULL,
	`role_id` INT(11) NOT NULL DEFAULT 3,
	`is_active` TINYINT(1) NOT NULL DEFAULT 1,
	`fails_count` INT(2) NULL DEFAULT 0,
	`created_ts` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
	`updated_ts` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP(),
	PRIMARY KEY (`id`)
)
COLLATE='utf8mb4_0900_ai_ci';

/* ALTER TABLE `users`
	CHANGE COLUMN `password` `password` VARCHAR(100) NOT NULL AFTER `email`; */

/* ALTER TABLE `users`
	ADD COLUMN `created_ts` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP() AFTER `fails_count`; */

/* ALTER TABLE `users`
	ADD COLUMN `updated_ts` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP() AFTER `created_ts`; */

/* ALTER TABLE `users`
	ADD COLUMN `email` VARCHAR(50) NOT NULL AFTER `login`; */

/* ALTER TABLE `users`
	ADD COLUMN `role_id` INT(11) NOT NULL AFTER `password`; */

/* ALTER TABLE `users` CHANGE COLUMN `password` `password` VARCHAR(50) NOT NULL AFTER `email`; */

ALTER TABLE `users`
	ADD CONSTRAINT `role_id_key` FOREIGN KEY (`role_id`) REFERENCES `user_roles` (`id`);

CREATE TABLE `sessions` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`user_id` INT(11) NOT NULL,
	`token` VARCHAR(300) NOT NULL,
	`created_ts` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
	`updated_ts` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP(),
	`requests_count` INT(11) NOT NULL DEFAULT 0,
	`is_expired` TINYINT(1) NOT NULL DEFAULT 0,
	PRIMARY KEY (`id`)
)
COLLATE='utf8mb4_0900_ai_ci';

ALTER TABLE `sessions`
	ADD CONSTRAINT `user_id_key` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/* ALTER TABLE `sessions`
	ADD COLUMN `is_expired` TINYINT(1) NOT NULL DEFAULT 0 AFTER `datetime`; */

/* ALTER TABLE `sessions`
	CHANGE COLUMN `datetime` `created_ts` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `token`; */

/* ALTER TABLE `sessions`
	ADD COLUMN `updated_ts` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `datetime`; */

/* ALTER TABLE `sessions`
	CHANGE COLUMN `updated_ts` `updated_ts` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP() AFTER `created_ts`; */

/* ALTER TABLE `sessions`
	ADD COLUMN `requests_count` INT(11) NOT NULL DEFAULT 0 AFTER `updated_ts`; */

/* ALTER TABLE `users`
	CHANGE COLUMN `role_id` `role_id` INT(11) NOT NULL DEFAULT 3 AFTER `password`; */

/* ALTER TABLE `users`
	ADD COLUMN `fails_count` INT(2) NULL DEFAULT 0 AFTER `is_active`; */

INSERT INTO `fast_news`.`user_roles` (`name`) VALUES ('admin');

UPDATE `fast_news`.`user_roles` SET `privileges`='15' WHERE  `id`=1;

INSERT INTO `fast_news`.`user_roles` (`name`, `privileges`) VALUES ('user', '1');

INSERT INTO `fast_news`.`user_roles` (`name`) VALUES ('guest');

INSERT INTO `fast_news`.`users` (`login`, `email`, `password`, `role_id`, `is_active`) VALUES ('admin', 'fff@ggg.com', 'null', '1', '1');

INSERT INTO `fast_news`.`users` (`login`, `password`, `role_id`) VALUES ('fff', 'ggg', '1');

/* DELETE FROM `fast_news`.`sessions` WHERE  `id`=1; */

/* SELECT LAST_INSERT_ID(); */

/* UPDATE `fast_news`.`users` SET `is_active`='0' WHERE  `id`=2; */

/* UPDATE `fast_news`.`users` SET `is_active`='1' WHERE  `id`=1; */

CREATE TABLE `images` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(256) NOT NULL DEFAULT '',
	`original_name` VARCHAR(256) NOT NULL DEFAULT '',
    `curr_name` VARCHAR(256) NOT NULL DEFAULT '',
	`size` INT UNSIGNED NOT NULL DEFAULT 0,
	`mimetype` VARCHAR(50) NOT NULL DEFAULT '',
	`encoding` VARCHAR(50) NOT NULL DEFAULT '',
	`title` VARCHAR(256) NOT NULL DEFAULT '',
	`description` VARCHAR(1024) NOT NULL DEFAULT '',
	`created_ts` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
	`updated_ts` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP(),
	`user_id` INT(11) NOT NULL DEFAULT 0,
	`deleted` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	PRIMARY KEY (`id`)
)
COLLATE='utf8mb4_0900_ai_ci';

ALTER TABLE `images`
	ADD CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/* ALTER TABLE `images`
	ADD COLUMN `mimetype` VARCHAR(50) NOT NULL DEFAULT '' AFTER `description`,
	ADD COLUMN `path` VARCHAR(256) NOT NULL DEFAULT '' AFTER `mimetype`; */

/* ALTER TABLE `images`
	CHANGE COLUMN `path` `curr_name` VARCHAR(256) NOT NULL DEFAULT '' AFTER `mimetype`; */

/* ALTER TABLE `images`
	CHANGE COLUMN `mimetype` `mimetype` VARCHAR(50) NOT NULL DEFAULT '' AFTER `size`,
	ADD COLUMN `encoding` VARCHAR(50) NOT NULL DEFAULT '' AFTER `mimetype`; */

CREATE TABLE `news` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`title` VARCHAR(256) NOT NULL DEFAULT '',
	`description` VARCHAR(1024) NOT NULL DEFAULT '',
	`content` INT(11) UNSIGNED NULL DEFAULT 0,
	`user_id` INT(11) UNSIGNED DEFAULT 0,
	`created_ts` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
	`updated_ts` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP(),
	`publishing_ts` TIMESTAMP NULL,
	`is_active` TINYINT(1) NOT NULL DEFAULT 0,
	PRIMARY KEY (`id`)
)
COLLATE='utf8mb4_0900_ai_ci'
;

ALTER TABLE `articles`
	CHANGE COLUMN `user_id` `user_id` INT(11) NOT NULL DEFAULT '0' AFTER `content`,
	ADD CONSTRAINT `user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

ALTER TABLE `news`
	CHANGE COLUMN `content` `content` INT(11) NOT NULL DEFAULT '0' AFTER `description`,
	ADD CONSTRAINT `content_id` FOREIGN KEY (`content`) REFERENCES `news_content` (`id`);

ALTER TABLE `news`
	ADD FULLTEXT INDEX `searchText` (`title`, `description`);

/* RENAME TABLE `articles` TO `news`; */

/* ALTER TABLE `news`
	ADD COLUMN `description` VARCHAR(1024) NOT NULL DEFAULT '' AFTER `title`; */

CREATE TABLE `news_content` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`component` VARCHAR(256) NOT NULL DEFAULT '',
	`props` VARCHAR(7168) NOT NULL DEFAULT '',
	`created_ts` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
	`updated_ts` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP(),
	PRIMARY KEY (`id`)
)
COLLATE='utf8mb4_0900_ai_ci'
;
