const { EOL } = require('os');
const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const serveStatic = require('serve-static');

const { getFromArgv } = require('./src/utils/common-utils');
const savePid = require('./src/logger/savePid');
const log = require('./src/logger/log');
const { default: def, custom } = require('./configs/server');
const requestsLogger = require('./src/middleware/requestsLogger');
const notFound404 = require('./src/finalware/notFound404');
const apiV1Router = require('./src/router/api-v1-router');
const authRouter = require('./src/router/auth-router');

const portFromArgv = getFromArgv(process.argv, 'port', true);
const port = portFromArgv || custom.port || def.port;
const mode = process.env.NODE_ENV || def.NODE_ENV || 'production';
const pid = process.pid;

savePid(pid).catch((err) => { log(`Error: unable to save pid: ${err.message}`) });

log(`${EOL}---------------------------------------------------------`, false);
log(`Application was started in ${mode} mode with pid: ${pid}`);

const server = express();

const pathToBuild = path.resolve(__dirname, 'resources', 'build');
const uploadsDir = custom.uploadsDir || def.uploadsDir || 'uploads';
const pathToUploads = path.resolve(__dirname, uploadsDir);

server.set('etag', 'strong');

server.use(helmet());
server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());
server.use(bodyParser.text());
server.use(requestsLogger);

server.use('/auth', authRouter);
server.use('/api/v1', apiV1Router);
server.use('/', express.static(pathToBuild));
server.use('/admin', express.static(pathToBuild));
server.use('/login', express.static(pathToBuild));
server.use('/registration', express.static(pathToBuild));
server.use('/personal', express.static(pathToBuild));
server.use('/error', express.static(pathToBuild));

server.use(serveStatic(pathToUploads, {
  setHeaders: function(res, path) {
    res.setHeader('Content-type', 'image/*');
    res.setHeader('content-disposition', 'inline');
  }
}));


server.use('/images', express.static(pathToUploads));
server.get('/*', express.static(pathToBuild));

server.use(notFound404);

server.listen(port, () => {
  log(`Server listens on port: ${port}`);
});
