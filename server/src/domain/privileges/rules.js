const READ = 1;
const CREATE = 2;
const UPDATE = 4;
const DELETE = 8;
const CHANGE_PASSWORD = 16;
const USER_UNLOCK = 32;
const ACTIVATE = 64;

const RULES = {
  ['/api/v1/roles']: {
    letter: 'r',
    bitValues: {
      get: READ,
      post: CREATE,
      put: UPDATE,
      delete: DELETE,
    },
  },
  ['/api/v1/users']: {
    letter: 'u',
    bitValues: {
      get: READ,
      post: CREATE,
      put: UPDATE,
      delete: DELETE,
    },
  },
  ['/api/v1/users/change-password']: {
    letter: 'u',
    bitValues: {
      put: CHANGE_PASSWORD,
    },
  },
  ['/api/v1/users/unlock']: {
    letter: 'u',
    bitValues: {
      put: USER_UNLOCK,
    },
  },
  ['/api/v1/personal/account']: {
    letter: 'a',
    bitValues: {
      get: READ,
      post: UPDATE,
    },
  },
  ['/api/v1/personal/account/activate']: {
    letter: 'a',
    bitValues: {
      put: ACTIVATE,
    },
  },
  ['/api/v1/personal/account/change-password']: {
    letter: 'a',
    bitValues: {
      put: UPDATE,
    },
  },
  ['/api/v1/personal/images']: {
    letter: 'i',
    bitValues: {
      get: READ,
      post: CREATE,
      put: UPDATE,
      delete: DELETE,
    },
  },
  ['/api/v1/personal/news']: {
    letter: 'n',
    bitValues: {
      get: READ,
      post: CREATE,
      put: UPDATE,
      delete: DELETE,
    },
  },
  ['/api/v1/home/news']: {
    letter: 'n',
    bitValues: {
      get: READ,
      post: CREATE,
      put: UPDATE,
      delete: DELETE,
    },
  },

  ['/api/v1/home/news/content']: {
    letter: 'n',
    bitValues: {
      get: READ,
      post: CREATE,
      put: UPDATE,
    }
  },
};

module.exports = RULES;
