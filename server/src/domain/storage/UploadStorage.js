const fs = require('fs');
const path = require('path');
const sha256 = require('js-sha256');
const { getUploadsDir } = require('../../utils/personal-utils');
const validators = require('../../requestValidators/fileValidators');

const uploadsDir = getUploadsDir();

function getDestination (req, file, cb) {
  const { id } = req.user;
  const current = new Date().getTime();
  const fileName = `image_${sha256(`${id}${current}`)}`;
  cb(null, path.resolve(uploadsDir, fileName));
}

function UploadStorage (opts = {}) {
  this.getDestination = (opts.destination || getDestination);
}

UploadStorage.prototype._handleFile = function _handleFile (req, file, cb) {
  this.getDestination(req, file, function (err, filePath) {
    if (err) return cb(err);

    const { base } = path.parse(filePath);
    const { title, description, fileName, fileSize } = req.body;
    // console.log('sadfasdf: file: ', req.file);
    // console.log('sadfasdf: body: ', req.body);

    validators.upload({ title, description, fileName, fileSize })
      .then((validation) => {
        if (validation && validation.isValid) {
          const outStream = fs.createWriteStream(filePath);

          file.stream.pipe(outStream);
          outStream.on('error', cb);
          outStream.on('finish', function () {
            cb(null, {
              size: outStream.bytesWritten,
              base,
            });
          });
        } else {
          return cb(JSON.stringify(validation));
        }
      })
      .catch((err) => cb(err));
  });
};

UploadStorage.prototype._removeFile = function _removeFile (req, file, cb) {
  fs.unlink(file.path, cb)
};

module.exports = function (opts) {
  return new UploadStorage(opts)
};
