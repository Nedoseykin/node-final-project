class Image {
  constructor(props) {
    this.id = props.id;
    this.name = props.name;
    this.original_name = props.original_name;
    this.curr_name = props.curr_name;
    this.size = props.size;
    this.mimetype = props.mimetype;
    this.encoding = props.encoding;
    this.title = props.title;
    this.description = props.description;
    this.user_id = props.user_id;

    this.formatForClient = this.formatForClient.bind(this);
  }

  formatForClient() {
    return {
      id: this.id,
      curr_name: this.curr_name,
      size: this.size,
      title: this.title,
      description: this.description,
    };
  }
}

module.exports = Image;
