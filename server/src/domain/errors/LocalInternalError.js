const BasicError = require('./BasicError');

class LocalInternalError extends BasicError {
  constructor() {
    super('Local internal error');
  }
}

module.exports = LocalInternalError;
