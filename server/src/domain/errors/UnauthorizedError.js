const BasicError = require('./BasicError');

class UnauthorizedError extends BasicError{
  constructor() {
    super('Unauthorized error', 401);
  }
}

module.exports = UnauthorizedError;
