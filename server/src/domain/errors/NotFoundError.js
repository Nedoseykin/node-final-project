const BasicError = require('./BasicError');

class NotFoundError extends BasicError{
  constructor(message = 'Resource was not found') {
    super(message, 404);
  }
}

module.exports = NotFoundError;
