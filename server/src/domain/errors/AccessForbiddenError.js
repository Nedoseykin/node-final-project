const BasicError = require('./BasicError');

class AccessForbiddenError extends BasicError{
  constructor() {
    super('Access forbidden error', 403);
  }
}

module.exports = AccessForbiddenError;
