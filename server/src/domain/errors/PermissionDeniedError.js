const BasicError = require('./BasicError');

class PermissionDeniedError extends BasicError{
  constructor() {
    super('Permission denied', 403);
  }
}

module.exports = PermissionDeniedError;
