const sha256 = require('js-sha256');
const BasicError = require('../domain/errors/BasicError');
const log = require('../logger/log');

module.exports = function checkDataAndSendAsJson(req, res, data, serviceName) {
  if (!Array.isArray(data)) {
    new BasicError('Internal server error');
  }
  const etag = sha256(JSON.stringify(data));
  const ifNoneMatch = req.headers['if-none-match'];
  if (etag === ifNoneMatch) {
    log(`304 - ${serviceName} - items found: ${data.length}, not changed`);
    res.sendStatus(304);
  } else {
    log(`200 - ${serviceName} - items found: ${data.length}`);
    res.set('ETag', etag).status(200).json(data);
  }
};
