const log = require('../logger/log');

module.exports = function(req, res) {
  const { originalUrl } = req;
  const message = `404 - not found - ${originalUrl}`;
  const date = new Date();
  const status = 404;
  log(`${date.toUTCString()} - Error: ${message}`, false);
  res
    .status(status)
    .json({
      message,
      status,
      dateTime: date.getTime(),
    });
};
