const BasicError = require('../domain/errors/BasicError');
const LocalInternalError = require('../domain/errors/LocalInternalError');
const log = require('../logger/log');

module.exports = res => function(error) {
  if (!(error instanceof LocalInternalError)) {
    const {
      message,
      status,
      date,
    } = new BasicError(error.message, error.status, error.date);
    log(`${date.toUTCString()} - ${status} - ${message}`, false);
    res.status(status).send({ message, status, dateTime: date.getTime() });
  }
};
