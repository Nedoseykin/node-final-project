const log = require('../logger/log');
const dao = require('../dao/mysql/mysqlDao');
const catchControllerError = require('../finalware/catchControllerError');
const checkDataAndSendAsJson = require('../finalware/checkDataAndSendAsJson');
const { isFilledArray } = require('../utils/common-utils');
const NotFoundError = require('../domain/errors/NotFoundError');
const BasicError = require('../domain/errors/BasicError');
const roleValidators = require('../requestValidators/roleValidators');

exports.getRoles = function(req, res) {
  dao.getRoles()
    .then((data) => {
      checkDataAndSendAsJson(req, res, data, 'getRoles');
    })
    .catch(catchControllerError(res))
};

exports.updateRoles = function(req, res) {
  const { id, ...rest } = req.body;
  dao.getRoleById(id)
    .then((data) => {
      if (!isFilledArray(data)) {
        throw new NotFoundError();
      }
      return dao.updateRole({ ...data[0], ...rest });
    })
    .then(({ result, role }) => {
      const { changedRows } = result;
      if (changedRows < 1) {
        log(`200 - role ${role.name} was not changed`);
      } else {
        log(`200 - role ${role.name} was changed`);
      }
      res.status(200).json(role);
    })
    .catch(catchControllerError(res))
};

exports.createRole = function(req, res) {
  const { body: payload } = req;
  const { name, privileges } = payload;

  roleValidators.create(payload)
    .then((validation) => {
      if (!validation.isValid) {
        log(`200 - Error: Validation is fault (requested name: ${name}): ${JSON.stringify(validation)}`);
        res.status(200).json(validation);
      } else {
        log(`200 - Role creation: validation was passed (requested name: ${name}). Process is in progress...`);
        return dao.createRole({ name, privileges });
      }
    })
    .then((daoResult) => {
      if (daoResult) {
        const { affectedRows, insertId } = daoResult;
        if (affectedRows === 0) {
          throw new BasicError('Role was not created: internal server error');
        }
        log(`200 - Role was created (name: ${name})`);
        res.status(200).json({ name, privileges, id: insertId });
      }
    })
    .catch(catchControllerError(res));
};
