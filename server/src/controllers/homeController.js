const dao = require('../dao/mysql/mysqlDao');
const log = require('../logger/log');
const { isFilledArray } = require('../utils/common-utils');
const NotFoundError = require('../domain/errors/NotFoundError');
const BasicError = require('../domain/errors/BasicError');
const LocalInternalError = require('../domain/errors/LocalInternalError');
const catchControllerError = require('../finalware/catchControllerError');
const checkDataAndSendAsJson = require('../finalware/checkDataAndSendAsJson');
const newsValidators = require('../requestValidators/newsValidators');
const contentValidators = require('../requestValidators/contentValidator');

exports.getNews = function(req, res) {
  dao.getNewsWithLogin()
    .then((news) => {
      checkDataAndSendAsJson(req, res, news, 'getNews');
    })
    .catch(catchControllerError(res));
};

exports.getPublishedNews = function(req, res) {
  const { search } = req.query;

  dao.getPublishedNewsWithLogins(search)
    .then((daoResult) => {
      checkDataAndSendAsJson(req, res, daoResult, 'getPublishedNews')
    })
    .catch(catchControllerError(res));
};

exports.getPersonalNews = function(req, res) {
  const { id } = req.user;
  dao.getNewsWithLoginByUserId(id)
    .then((daoResult) => {
      checkDataAndSendAsJson(req, res, daoResult, 'getPersonalNews');
    })
    .catch(catchControllerError(res));
};

exports.updatePersonalNews = function(req, res) {
  const { body, user } = req;
  newsValidators.update(req.body)
    .then((validation) => {
      if (!validation.isValid) {
        log(`Validation failed: ${JSON.stringify(validation)}`);
        res.status(200).json(validation);
        throw new LocalInternalError();
      }
      return dao.getNewsByIdAndUserId(body.id, user.id)
    })
    .then((daoResult) => {
      if (!isFilledArray(daoResult)) {
        throw new NotFoundError('Publication was not found');
      }
      const news = {
        ...daoResult[0],
        title: body.title,
        description: body.description,
        is_active: body.is_active,
        content: body.content,
        publishing_ts: body.publishing_ts,
      };
      return dao.updateNews(news);
    })
    .then((daoResult) => {
      if (!daoResult) {
        throw new BasicError('Failed news update');
      }
      const { changedRows } = daoResult;
      if (changedRows < 1) {
        log(`200 - No changes were made for publication with id: ${body.id}`);
      } else {
        log(`200 - Publication was changed, id: ${body.id}`);
      }
      res.status(200).json(body);
    })
    .catch(catchControllerError(res));
};

exports.createPersonalNews = function(req, res) {
  const { body, user } = req;

  newsValidators.create(body)
    .then((validation) => {
      if (!validation.isValid) {
        log(`200 - Validation failed: ${JSON.stringify(validation)}`);
        res.status(200).json(validation);
        throw new LocalInternalError();
      }
      const news = { ...body, user_id: user.id };
      return dao.createNews(news);
    })
    .then((daoResult) => {
      const { affectedRows, insertId } = daoResult;
      if (affectedRows < 1) {
        throw new BasicError('Error: can not create news');
      }
      return dao.getNewsById(insertId);
    })
    .then((daoResult) => {
      if (!isFilledArray(daoResult)) {
        throw new BasicError('Internal server error');
      }
      const news = {
        ...daoResult[0],
        login: user.login,
      };
      delete(news.user_id);
      log(`200 - News was created with id: ${news.id}`);

      res.status(200).json(news);
    })
    .catch(catchControllerError(res));
};

exports.getNewsContent = function(req, res) {
  const id = parseInt(req.params.id, 10);
  console.log('getNewsContent: id: ', id);
  dao.getNewsContentItemById(id)
    .then((daoResult) => {
      if (!isFilledArray(daoResult)) {
        throw NotFoundError();
      }
      const item = {
        ...daoResult[0],
        props: JSON.parse(daoResult[0].props)
      };
      res.status(200).json(item);
    })
    .catch(catchControllerError(res));
};

exports.createNewsContent = function(req, res) {
  const { body: payload } = req;
  contentValidators.create(payload)
    .then((validation) => {
      if (!(validation && validation.isValid === true)) {
        log(`200 - Validation failed: ${JSON.stringify(validation)}`);
        res.status(200).json(validation);
        throw LocalInternalError();
      }
      log(`Validation passed: ${validation}`);
      return dao.createNewsContent(payload);
    })
    .then((daoResult) => {
      if (!daoResult || daoResult.affectedRows < 1) {
        throw new BasicError('Can not create news content');
      }
      const { insertId } = daoResult;
      log(`200 - Content was creted with id: ${daoResult.insertId}`);
      const props = payload.props
        ? JSON.parse(payload.props)
        : JSON.parse({ text: '', textPosition: '', imageSrc: '' });
      res.status(200).json({ ...payload, props, id: insertId });
    })
    .catch(catchControllerError(res));
};

exports.updateNewsContent = function(req, res) {
  const { body: payload } = req;
  console.log('updateNewsContent: payload: ', payload);
  contentValidators.update(payload)
    .then((validation) => {
      if (!validation || validation.isValid !== true) {
        log(`200 - Validation failed: ${JSON.stringify(validation)}`);
        res.status(200).json(valdation);
        throw LocalInternalError();
      }
      log(`Validation passed: ${JSON.stringify(validation)}`);
      return dao.updateNewsContent(payload);
    })
    .then((daoResult) => {
      if (daoResult) {
        const { changedRows } = daoResult;
        if (changedRows < 1) {
          log(`200 - content ${req.body.id} was not changed`);
        } else {
          log(`200 - content ${req.body.id} was changed`);
        }
        res.status(200).json({ ...payload });
      }
    })
    .catch(catchControllerError(res));
};
