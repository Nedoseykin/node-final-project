const log = require('../logger/log');
const mysqlDao = require('../dao/mysql/mysqlDao');
const { default: def, custom } = require('../../configs/server');
const { isFilledArray, getToken } = require('../utils/common-utils');
const UnauthorizedError = require('../domain/errors/UnauthorizedError');
const BasicError = require('../domain/errors/BasicError');
const catchControllerError = require('../finalware/catchControllerError');
const userValidators = require('../requestValidators/userValidators');
const { encrypt } = require('../utils/auth-utils');

const failsLimit = custom.authFailsLimit || def.authFailsLimit || 0;

exports.register = function(req, res) {
  const { body: payload } = req;
  const { login, email, password } = payload;

  userValidators.registration(payload)
    .then((validation) => {
      if (!validation.isValid) {
        log(`200 - Error: Validation is fault (requested login: ${login}): ${JSON.stringify(validation)}`);
        res.status(200).json(validation);
      } else {
        log(`200 - Register: validation was passed (requested login: ${login}). User creation is in progress...`);

        return mysqlDao.createUser({ login, email, password: encrypt(password) });
      }
    })
    .then((daoResult) => {
      if (!isFilledArray(daoResult)) {
        throw new BasicError(
          'Registration failed: internal server error. Connect to your system administrator.',
          500,
        );
      }
      const user = daoResult[0];
      log(`User with login ${user.login} was created`);
      const token = getToken();
      return mysqlDao.createSession(user, token);
    })
    .then((daoResult) => {
      if (daoResult) {
        const { user, token } = daoResult;
        log(`200 - Session for new user (login: ${login}) was created with token: ${token}`);
        res.status(200).json({ token, role_id: user.role_id, privileges: user.privileges });
      }
    })
    .catch(catchControllerError(res));
};

exports.login = function(req, res) {
  const { login, password } = req.body;
  const encrypted = encrypt(password);

  mysqlDao.getUserWithPrivilegesByLogin(login)
    .then((daoResult) => {
      if (!isFilledArray(daoResult) || (daoResult[0].is_active !== 1)) {
        throw new UnauthorizedError();
      }
      const user = daoResult[0];
      if (user.password !== encrypted) {
        user.fails_count += 1;
        if ((failsLimit > 0) && (user.fails_count > failsLimit)) {
          user.is_active = 0;
        }
      } else if (user.fails_count > 0) {
        user.fails_count = 0;
      }
      req.user = user;
      return mysqlDao.updateUser(user);
    })
    .then((daoResult) => {
      log(`User data was ${daoResult.changedRows === 0 ? 'not ' : ''}changed`);
      if (req.user.password !== encrypted) {
        throw new UnauthorizedError();
      }
      const token = getToken();
      return mysqlDao.createSession(req.user, token)
    })
    .then(({ user, token }) => {
      log(`200 - Authorized successfully - user: login: ${login}, role ID: ${user.role_id}`);
      res.status(200).send({ token, role_id: user.role_id, privileges: user.privileges });
    })
    .catch(catchControllerError(res));
};

exports.logout = function(req, res) {
  const { token } = req.body;
  mysqlDao.updateSession({ token, is_expired: 1 })
    .then(() => {
      log(`Logout successful - token - ${token}`);
      res.sendStatus(200);
    })
    .catch(catchControllerError(res));
};
