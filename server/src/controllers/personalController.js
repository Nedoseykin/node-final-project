const fs = require('fs');
const path = require('path');
const Jimp = require('jimp');
const catchControllerError = require('../finalware/catchControllerError');
const checkDataAndSendAsJson = require('../finalware/checkDataAndSendAsJson');
const multer = require('multer');
const uploadStorage = require('../domain/storage/UploadStorage');
const log = require('../logger/log');
const dao = require('../dao/mysql/mysqlDao');
const BasicError = require('../domain/errors/BasicError');
const LocalInternalError = require('../domain/errors/LocalInternalError');
const NotFoundError = require('../domain/errors/NotFoundError');
const AccessForbiddenError = require('../domain/errors/AccessForbiddenError');
const Image = require('../domain/image/Image');
const validators = require('../requestValidators/fileValidators');
const accountValidators = require('../requestValidators/accountValidators');
const userValidators = require('../requestValidators/userValidators');
const { getUploadsDir } = require('../utils/personal-utils');
const { isFilledArray } = require('../utils/common-utils');
const { encrypt } = require('../utils/auth-utils');

const storage = uploadStorage();
const upload = multer({ storage }).single('image');

exports.getAccount = function(req, res) {
  const { user } = req;
  if (!user) {
    throw new NotFoundError();
  }
  const { login, role_id, email, role } = user;
  res.status(200).json({ login, role_id, email, privileges: role.privileges });
};

exports.updateAccount = function(req, res) {
  const { user } = req;
  const { login, email } = req.body;
  return accountValidators.update({ id: user.id, login, email })
    .then((validation) => {
      if ((validation.isValid === false)) {
        log(`200 - Validation failed: ${validation}`);
        res.status(200).json(validation);
        throw new LocalInternalError();
      }
      return dao.updateAccount({
        ...user, login, email,
      })
    })
    .then((daoResult) => {
      const { changedRows } = daoResult;
      const payload = { login, email };
      if (!changedRows) {
        log(`No changes performed: ${JSON.stringify(payload)}`);
      } else {
        log(`Account has been changed: ${JSON.stringify(payload)}`);
      }
      res.status(200).json(payload);
    })
    .catch(catchControllerError(res))
};

exports.activateAccount = function(req, res) {
  dao.updateUser({ ...req.user, role_id: 2 })
    .then((daoResult) => {
      if (!daoResult || daoResult.changedRows < 1) {
        throw new BasicError('Error: account activation failed');
      }
      return dao.getUserWithPrivilegesById(req.user.id);
    })
    .then((daoResult) => {
      if (!isFilledArray(daoResult)) {
        throw new BasicError('Internal server error. User not found');
      }
      const user = daoResult[0];
      log(`Account was activated for login: ${user.login}`);
      res.status(200).json({
        login: user.login,
        role_id: user.role_id,
        email: user.email,
        privileges: user.privileges,
      });
    })
    .catch(catchControllerError(res));
};

exports.changePassword = function(req, res) {
  const { login, currentPassword, newPassword, confirmPassword } = req.body;
  console.log('initial: ', req.body);
  const { password: userPassword } = req.user;

  const encryptedCurrent = encrypt(currentPassword);
  const encryptedNew = encrypt(newPassword);
  const encryptedConfirm = encrypt(confirmPassword);

  userValidators.changePassword({
    currentPassword:encryptedCurrent,
    newPassword: encryptedNew,
    confirmPassword: encryptedConfirm,
    userPassword,
  })
    .then((validation) => {
      if (!validation.isValid) {
        log(`200 - Validation failed: ${JSON.stringify(validation)}`);
        res.status(200).json(validation);
        throw new LocalInternalError();
      }
      if (req.user.login !== login) {
        throw new AccessForbiddenError();
      }
      return dao.updateUserPassword({ ...req.user, password: encryptedNew })
    })
    .then((daoResult) => {
      const { changedRows } = daoResult;
      if (changedRows < 1) {
        log('200 - Account password was not changed');
      } else {
        log('200 - Account password was changed');
      }
      res.sendStatus(200);
    })
    .catch(catchControllerError(res));
};

exports.createThumbnail = function(req, res) {
  const { id } = req.body;
  let fileName;
  const uploadsDir = getUploadsDir();

  return dao.getImageById(id)
    .then((daoResult) => {
      if (!isFilledArray(daoResult)) {
        throw new NotFoundError();
      }
      ({ curr_name: fileName } = daoResult[0]);
      const fullName = path.resolve(uploadsDir, fileName);
      const thumbName = path.resolve(uploadsDir, 'thumbnails', fileName);
      if (fs.existsSync(thumbName)) {
        throw new BasicError('Thumbnail is exists', 403);
      }
      log(`Thumbnail is not exists. Start to create for ${fileName}`);

      return Jimp.read(fullName);
    })
    .then((image) => {
      const { width, height } = image.bitmap;
      let resizeWidth;
      let resizeHeight;
      if (width > height) {
        resizeWidth = 200;
        resizeHeight = Jimp.AUTO;
      } else {
        resizeWidth = Jimp.AUTO;
        resizeHeight = 200;
      }

      const thumbName = path.resolve(uploadsDir, 'thumbnails', fileName);

      image
        .resize(resizeWidth, resizeHeight)
        .quality(60)
        .write(thumbName, (err) => {
          if (err) {
            log(err.message);
            throw new BasicError('Unable to create thumbnail');
          }
          log('200 - thumbnail is created');
          res.status(200).send({ id });
        });
    })
    .catch(catchControllerError(res))
};

exports.uploadImage = function(req, res) {
  const uploadsDir = getUploadsDir();
  let fileName;

  new Promise((resolve, reject) => {
    upload(req, res, function(err) {
      if (err instanceof multer.MulterError) {
        reject(err);
      } else if (err) {
        let validation;
        try {
          validation = JSON.parse(err);
          resolve(validation);
        } catch (e) {
          reject(err);
        }
      } else {
        const { body = {}, file = {}, user } = req;

        if (!file.originalname) {
          resolve({ isValid: false, fileSize: 'File is required' });
        } else {
          const payload = {
            name:          body.fileName,
            original_name: file.originalname,
            curr_name:     file.base,
            size:          file.size,
            mimetype:      file.mimetype,
            encoding:      file.encoding,
            title:         body.title,
            description:   body.description,
            user_id:       user.id,
          };
          resolve(payload);
        }
      }
    })
  })
    .then((data) => {
      if (data && data.isValid === false) {
        log(`200 - Validation failed: ${JSON.stringify(data)}`);
        res.status(200).json(data);
        throw new LocalInternalError();
      } else if (data) {
        log('Validation passed');
        return data;
      }
    })
    .then((data) => {
      if (data) {
        return dao.createImage(data);
      }
    })
    .then((daoResult) => {
      if (daoResult) {
        const { id } = daoResult;
        if (!id) {
          throw new BasicError('Image was not created: internal server error');
        }
        log(`200 - Image was created (id: ${id})`);
        res.status(200).json(new Image(daoResult).formatForClient());
        return daoResult;
      }
    })
    .then((image) => {
      // todo creation of a thumbnail
      ({ curr_name: fileName } = image);
      const fullName = path.resolve(uploadsDir, fileName);
      return Jimp.read(fullName);
    })
    .then((image) => {
      const { width, height } = image.bitmap;
      let resizeWidth;
      let resizeHeight;
      if (width > height) {
        resizeWidth = 200;
        resizeHeight = Jimp.AUTO;
      } else {
        resizeWidth = Jimp.AUTO;
        resizeHeight = 200;
      }
      const thumbName = path.resolve(uploadsDir, 'thumbnails', fileName);

      image
        .resize(resizeWidth, resizeHeight)
        .quality(60)
        .write(thumbName, (err) => {
          if (err) {
            throw new LocalInternalError();
          }
        });
    })
    .catch(catchControllerError(res));
};

exports.updateImage = function(req, res) {
  const { id, title, description } = req.body;
  const payload = { id, title, description };
  validators.update(payload)
    .then((validation) => {
      if (validation) {
        if (!validation.isValid) {
          log(`200 - Validation failed: ${JSON.stringify(validation)}`);
          res.status(200).json(validation);
          throw new LocalInternalError();
        }
        log('Validation passed');
        return dao.updateImage(payload);
      } else {
        throw new BasicError('Internal server error');
      }
    })
    .then((daoResult) => {
      if (!daoResult) {
        throw new BasicError('Internal server error');
      }
      const { changedRows } = daoResult;
      if (changedRows > 0) {
        log(`200 - image was updated: ${JSON.stringify(payload)}`);
      } else {
        log(`200 - image was not updated: ${JSON.stringify(payload)}`);
      }
      res.status(200).json(payload);
    })
    .catch(catchControllerError(res));
};

exports.getImages = function(req, res) {
  const { id } = req.user;
  dao.getImagesForClientByUserId(id)
    .then((daoResult) => {
      checkDataAndSendAsJson(req, res, daoResult, 'getImagesForClientByUserId');
    })
    .catch(catchControllerError(res));
};

exports.deleteImage = function(req, res) {
  let { id } = req.params;
  id = parseInt(id, 10);
  let fileName;
  return dao.getImageById(id)
    .then((image) => {
      if (!isFilledArray(image)) {
        throw new NotFoundError();
      }
      ({ curr_name: fileName } = image[0]);

      return dao.deleteImage(id);
    })
    .then((daoResult) => {
      if (!daoResult) {
        throw new BasicError('Internal server error');
      }
      const uploadsDir = getUploadsDir();
      const fullName = path.resolve(uploadsDir, fileName);
      const fullThumbName = path.resolve(uploadsDir, 'thumbnails', fileName);

      if (fs.existsSync(fullName)) {
        fs.unlink(fullName, (err) => {
          if (err) {
            log(`Error: can not delete file: ${fullName}`);
          } else {
            log(`Deleted image: ${fullName}`);
          }
        });
      }
      if (fs.existsSync(fullThumbName)) {
        fs.unlink(fullThumbName, (err) => {
          if (err) {
            log(`Error: can not delete file: ${fullThumbName}`);
          } else {
            log(`Deleted thumbnail: ${fullThumbName}`);
          }
        });
      }

      log(`200 - deleteImage: success: id: ${id}`);
      res.status(200).send({ id });
    })
    .catch(catchControllerError(res));
};
