const { isFilledArray } = require('../utils/common-utils');
const log = require('../logger/log');
const dao = require('../dao/mysql/mysqlDao');
const catchControllerError = require('../finalware/catchControllerError');
const checkDataAndSendAsJson = require('../finalware/checkDataAndSendAsJson');
const AccessForbiddenError = require('../domain/errors/AccessForbiddenError');
const LocalInternalError = require('../domain/errors/LocalInternalError');
const BasicError = require('../domain/errors/BasicError');
const NotFoundError = require('../domain/errors/NotFoundError');
const { encrypt } = require('../utils/auth-utils');
const userValidators = require('../requestValidators/userValidators');

exports.getUsers = function(req, res) {
  dao.getUsersWithoutPassword()
    .then((data) => {
      checkDataAndSendAsJson(req, res, data, 'getUsers');
    })
    .catch(catchControllerError(res))
};

exports.updateUser = function(req, res) {
  userValidators.update(req.body)
    .then((validation) => {
      if (!validation || (validation.isValid !== true)) {
        log(`200 - Validation failed: ${JSON.stringify(validation)}`);
        res.status(200).json(validation);
        throw new LocalInternalError();
      }
      return dao.getUserById(req.body.id)
    })
    .then((daoResult) => {
      if (!isFilledArray(daoResult)) {
        throw new NotFoundError();
      }
      const userItem = daoResult[0];
      log(`User found: ${userItem.login}. Changes applying is started...`);
      req.changedUser = { ...userItem, ...req.body };
      return dao.updateUser({ ...req.changedUser });
    })
    .then((daoResult) => {
      if (daoResult.changedRows < 1) {
        log(`200 - User ${req.changedUser.login} was not changed`);
      } else {
        log(`200 - User ${req.changedUser.login} was changed`);
      }
      delete(req.changedUser.password);
      res.status(200).json({ ...req.changedUser });
    })
    .catch(catchControllerError(res));
};

exports.unlockUser = function(req, res) {
  dao.getUserById(req.body.id)
    .then((daoResult) => {
      if (!isFilledArray(daoResult)) {
        throw new NotFoundError();
      }
      req.unlockedUser = daoResult[0];
      log(`User found: ${req.unlockedUser.login}. Unlocking is started...`);
      req.unlockedUser.is_active = 1;
      req.unlockedUser.fails_count = 0;
      return dao.updateUser({ ...req.unlockedUser });
    })
    .then((daoResult) => {
      if (!daoResult || daoResult.changedRows < 1) {
        throw new BasicError(`Error: fail to unlock user`, 403);
      }
      log(`200 - User ${req.unlockedUser.login} was unlocked`);
      delete(req.unlockedUser.password);
      res.status(200).json({ ...req.unlockedUser });
    })
    .catch(catchControllerError(res));
};

exports.userPasswordChange = function(req, res) {
  const { id } = req.user;
  const { login, currentPassword, newPassword, confirmPassword } = req.body;

  const encryptedCurrent = encrypt(currentPassword);
  const encryptedNew = encrypt(newPassword);
  const encryptedConfirm = encrypt(confirmPassword);

  dao.getUserByLogin(login)
    .then((data) => {
      if (!isFilledArray(data)) {
        throw new NotFoundError();
      }
      const user = data[0];
      if (
        (id === user.id)
        || (user.password !== encryptedCurrent)
      ) {
        throw new AccessForbiddenError();
      }
      if ((encryptedNew !== encryptedConfirm)) {
        throw new BasicError('New password do not matches confirmed', 400);
      }
      log(`User ${user.login} was found. Start to apply changes...`);
      return dao.updateUserPassword({ ...user, password: encryptedNew });
    })
    .then((result) => {
      const { changedRows } = result;
      if (changedRows < 1) {
        log('200 - Password was not changed');
      } else {
        log('200 - Password was changed');
      }
      res.sendStatus(200);
    })
    .catch(catchControllerError(res));
};
