const sha256 = require('js-sha256');
const s = require('../constants/auth');

exports.encrypt = function(p) {
  return sha256(`${p}${s}`);
};
