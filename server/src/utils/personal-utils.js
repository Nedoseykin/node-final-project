const path = require('path');
const { default: def, custom = {} } = require('../../configs/server');

const { getHomeDir } = require('./fs-utils');

const getUploadsDir = function() {
  const homeDir = getHomeDir();
  const uploadsDir = custom.uploadsDir || def.uploadsDir;
  return path.resolve(homeDir, uploadsDir);
};

module.exports = {
  getUploadsDir,
};
