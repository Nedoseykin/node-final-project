const sha256 = require('js-sha256');

function isExists(v) {
  return (v !== undefined) && (v !== null);
}

function isNumber(v) {
  return (typeof v === 'number') && (v === +v);
}

function isFilledString(v) {
  return (typeof v === 'string') && (v.length > 0);
}

function isFilledArray(v) {
  return Array.isArray(v) && (v.length > 0);
}

function getFromArgv(argv, argName, isNumber = false) {
  if (!(isFilledArray(argv)
    && (argv.length > 2)
    && isFilledString(argName))
  ) {
    return null;
  }
  let l = argv.slice(2);
  for (let i of l) {
    let [key, value] = i.split('=');
    if (key === argName) {
      if (!isNumber) { return value; }
      value = parseInt(value, 10);
      return isNaN(value) ? null : value;
    }
  }
  return null;
}

function getToken() {
  let n = '';
  for (let i = 0; i < 20; i++) {
    n = `${n}${Math.round(Math.random() * 1000000)}`;
  }
  return sha256(n);
}

module.exports = {
  isExists,
  isNumber,
  isFilledString,
  isFilledArray,
  getFromArgv,
  getToken,
};
