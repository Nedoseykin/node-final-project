const { isFilledArray } = require('../utils/common-utils');

function validate(payload, validationMode, config) {
  const validation = { isValid: true };
  const fields = Object.keys(config);
  for (let i = 0; i < fields.length; i++) {
    const key = fields[i];
    const validators = config[key];
    if (isFilledArray(validators)) {
      for (let j = 0; j < validators.length; j++) {
        const { validator, message, usage } = validators[j];
        if (usage.includes(validationMode)) {
          const test = validator(payload[key], payload);
          if (!test) {
            validation[key] = message;
            validation.isValid = false;
            break;
          }
        }
      }
    }
  }
  return validation;
}

module.exports = {
  validate,
};
