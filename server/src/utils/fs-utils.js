const fs = require('fs');
const path = require('path');

function getHomeDir() {
  return  path.dirname(process.argv[1]);
}

function mkDirIfNotExists(dirName) {
  return new Promise((resolve, reject) => {
    if (!fs.existsSync(dirName)) {
      fs.mkdir(dirName, { recursive: true }, (err) => {
        if (err) {
          reject({ ...err, message: `Could not make directory ${dirName}: ${err.message}` });
        } else {
          resolve(true);
        }
      });
    } else {
      resolve(false);
    }
  });
}

function writeToFile(fileName, data, options, append = true) {
  return new Promise((resolve, reject) => {
    const action = append && fs.existsSync(fileName) ? 'appendFile' : 'writeFile';
    fs[action](fileName, data, options, (err) => {
      if (err) {
        reject({ ...err, message: `Unable to write to file: ${fileName}: ${err.message}` });
      } else {
        resolve(null);
      }
    });
  });
}

function writeStringToFile(fileName, data, append = true) {
  const dirName = path.dirname(fileName);
  return mkDirIfNotExists(dirName)
    .then(() => writeToFile(fileName, data, options = 'utf-8', append))
}

module.exports = {
  getHomeDir,
  mkDirIfNotExists,
  writeToFile,
  writeStringToFile,
};
