const path = require('path');
const fs = require('fs');
const os = require('os');

const { writeStringToFile } = require('../utils/fs-utils');

const { default: def, custom } = require('../../configs/server');

const logFileName = custom.logFileName || def.logFileName;

const dirName = path.resolve(__dirname, '..', '..', 'logs');

const fileName = path.resolve(dirName, logFileName);

module.exports = function(message, withDate = true) {
  const d = withDate ? new Date().toUTCString() : null;
  const msg = `${d ? `${d} - ` : ''}${message}`;
  console.log(msg);

  return writeStringToFile(fileName, `${msg}${os.EOL}`)
    .catch((err) => {
      console.log(`Error: ${err.message}`);
      return false;
    })
};
