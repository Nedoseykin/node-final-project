const { writeStringToFile } = require('../utils/fs-utils');
const path = require('path');

const { default: def, custom } = require('../../configs/server');

const pidFileName = custom.pidFileName || def.pidFileName;

const fileName = path.resolve(__dirname, '..', '..', 'process', pidFileName);

module.exports = async function(pid) {
  return writeStringToFile(fileName, pid, false);
};
