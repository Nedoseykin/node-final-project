const authRouter = require('express').Router();
const log = require('../logger/log');
const authController = require('../controllers/authController');

authRouter.post('/register', authController.register);

authRouter.post('/login', authController.login);

authRouter.post('/logout', authController.logout);

module.exports = authRouter;
