const apiV1Router = require('express').Router();
const sessionWare = require('../middleware/session');
const userWare = require('../middleware/user');
const privilegesWare = require('../middleware/privileges');
const usersController = require('../controllers/usersController');
const rolesController = require('../controllers/rolesController');
const homeController = require('../controllers/homeController');
const personalController = require('../controllers/personalController');

apiV1Router.use(sessionWare.check);

apiV1Router.use(userWare.check);

apiV1Router.use(privilegesWare.privelegesCheck);

apiV1Router.get('/home/news', homeController.getPublishedNews);

apiV1Router.post('/home/news/content', homeController.createNewsContent);

apiV1Router.put('/home/news/content', homeController.updateNewsContent);

apiV1Router.get('/home/news/content/:id', homeController.getNewsContent);

apiV1Router.put('/personal/account/activate', personalController.activateAccount);

apiV1Router.put('/personal/account/change-password', personalController.changePassword);

apiV1Router.get('/personal/account', personalController.getAccount);

apiV1Router.post('/personal/account', personalController.updateAccount);

apiV1Router.post('/personal/images/create-thumbnail', personalController.createThumbnail);

apiV1Router.post('/personal/images', personalController.uploadImage);

apiV1Router.put('/personal/images', personalController.updateImage);

apiV1Router.get('/personal/images', personalController.getImages);

apiV1Router.delete('/personal/images/:id', personalController.deleteImage);

apiV1Router.get('/personal/news', homeController.getPersonalNews);

apiV1Router.post('/personal/news', homeController.createPersonalNews);

apiV1Router.put('/personal/news', homeController.updatePersonalNews);

apiV1Router.get('/users', usersController.getUsers);

apiV1Router.put('/users', usersController.updateUser);

apiV1Router.put('/users/unlock', usersController.unlockUser);

apiV1Router.put('/users/change-password', usersController.userPasswordChange);

apiV1Router.get('/roles', rolesController.getRoles);

apiV1Router.put('/roles', rolesController.updateRoles);

apiV1Router.post('/roles', rolesController.createRole);

module.exports = apiV1Router;
