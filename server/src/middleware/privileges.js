const AccessForbiddenError = require('../domain/errors/AccessForbiddenError');
const BasicError = require('../domain/errors/BasicError');
const catchControllerError = require('../finalware/catchControllerError');
const log = require('../logger/log');
const dao = require('../dao/mysql/mysqlDao');
const { isFilledArray } = require('../utils/common-utils');
const RULES = require('../domain/privileges/rules');

// exports.adminCheck = function(req, res, next) {
//   new Promise((resolve, reject) => {
//     const { user, method } = req;
//     const adminRequired = (method === 'put') || (method === 'post');
//     const isAdmin = user && (user.role_id === 1);
//     log(`Role check by request type: admin required: ${adminRequired}, user is admin: ${isAdmin}`);
//     if (!adminRequired || (adminRequired && isAdmin)) {
//       log('Success: role check passed');
//       resolve();
//     } else {
//       log('Failed: access is forbidden');
//       reject(new AccessForbiddenError());
//     }
//   })
//     .then(next)
//     .catch(catchControllerError(res))
// };

exports.privelegesCheck = function(req, res, next) {
  const { user, method, originalUrl } = req;
  dao.getRoleById(user.role_id)
    .then((daoResult) => {
      if (!isFilledArray(daoResult)) {
        throw new BasicError('Error: bad role', 400);
      }
      req.user.role = daoResult[0];
      const { privileges } = daoResult[0];

      let cleanUrl = originalUrl.replace(/\/\d*$/, '');
      cleanUrl = cleanUrl.split('?')[0];
      const rule = RULES[cleanUrl];
      if (!rule) {
        throw new BasicError('Rule was not found', 400);
      }
      const { letter, bitValues } = rule;
      const bitValue = bitValues[method.toLowerCase()];
      // console.log('letter: ', letter);
      // console.log('bitValue: ', bitValue);
      let isAllowed = bitValue === null;
      if (!isAllowed) {
        if (!(letter && bitValue)) {
          throw new BasicError('Wrong permission rule details', 400);
        }
        const re = new RegExp(`${letter}(\\d+)`);
        const extracted = re.exec(privileges);
        if (!isFilledArray(extracted) || extracted.length < 2) {
          throw new BasicError('Privilege was not found', 400);
        }
        const parsed = parseInt(extracted[1], 10);
        if (isNaN(parsed)) {
          throw new BasicError('Privilege parsing error', 400);
        }
        isAllowed = Boolean(parsed & bitValue);
      }

      if (!isAllowed) {
        log('Error: privileges is insufficient');
        throw new AccessForbiddenError('');
      }
      req.isAllowed = isAllowed;
      log('Privileges checked: request is allowed');
      next();
    })
    .catch(catchControllerError(res));
};
