const log = require('../logger/log');

module.exports = function(req, res, next) {
  const { originalUrl, method } = req;
  const message = `${method} - ${originalUrl}`;
  log(message);
  next();
};
