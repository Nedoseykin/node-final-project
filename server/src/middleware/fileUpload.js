const validators = require('../requestValidators/fileValidators');
const catchControllerError = require('../finalware/catchControllerError');
const BasicError = require('../domain/errors/BasicError');
const log = require('../logger/log');

exports.fileUploadValidation = function(req, res, next) {
  const { title, description, fileName } = req.body;
  validators.upload({ title, description, fileName })
    .then((data) => {
      if (data) {
        if (data.isValid === false) {
          log(`200 - Validation failed: ${JSON.stringify(data)}`);
          res.status(200).json(data);
        } else {
          log('Validation passed');
          next()
        }
      } else {
        throw new BasicError('Internal server error', 500);
      }
    })
    .catch(catchControllerError(res));
};
