const dao = require('../dao/mysql/mysqlDao');
const catchControllerError = require('../finalware/catchControllerError');
const PermissionDeniedError = require('../domain/errors/PermissionDeniedError');
const { isFilledArray } = require('../utils/common-utils');
const log = require('../logger/log');

// used after session middleware (session in req)
exports.check = function(req, res, next) {
  const { user_id } = req.session;
  dao.getUserById(user_id)
    .then((data) => {
      if (!isFilledArray(data)
        // || (data[0].role_id !== 1)
        || (data[0].is_active !== 1)
      ) {
        const message = isFilledArray(data)
          ? 'Error: permission denied'
          : 'Error: user was not found';
        log(message);
        throw new PermissionDeniedError();
      }
      log('User was found - is active');
      req.user = data[0];
      next();
    })
    .catch(catchControllerError(res));
};
