const dao = require('../dao/mysql/mysqlDao');
const { isFilledArray } = require('../utils/common-utils');
const log = require('../logger/log');
const config = require('../../configs/server');
const catchControllerError = require('../finalware/catchControllerError');
const UnauthorizedError = require('../domain/errors/UnauthorizedError');

const { default: def, custom } = config;

const sessionTimeout = custom.sessionTimeout || def.sessionTimeout || 30;

exports.check = function(req, res, next) {
  const token = req.headers['auth-token'];
  // const { token } = req.body;
  log(`Start session tocken checking: ${token}`);
  return dao.getSession(token)
    .then((data) => {
      if (!isFilledArray(data) || data[0].is_expired === 1) {
        const message = isFilledArray(data)
          ? 'Error: found expired session'
          : 'Error: session was not found';
        log(message);
        throw new UnauthorizedError();
      }
      const session = data[0];
      const { updated_ts } = session;
      const updatedDate = new Date(updated_ts).getTime();
      const currentDate = new Date().getTime();
      const delta = Math.round((currentDate - updatedDate) / 60000);
      if ((sessionTimeout > 0) && (delta > sessionTimeout)) {
        session.is_expired = 1;
      } else {
        session.requests_count += 1;
      }
      return dao.updateSession(session);
    })
    .then((session) => {
      if (session.is_expired === 1) {
        log('Error: found expired session');
        throw new UnauthorizedError();
      }
      log('Success: actual session was found');
      req.session = session;
      next();
    })
    .catch(catchControllerError(res));
};
