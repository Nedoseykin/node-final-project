const { isExists } = require('../../utils/common-utils');
const mysql = require('mysql');
const log = require('../../logger/log');
const BasicError = require('../../domain/errors/BasicError');
const callbacks = require('./callbacks');

const SELECT_NEWS = 'select * from news';

const SELECT_ACTIVE_NEWS_WITH_LOGIN = 'select news.id, news.title, news.description, news.content, users.login,'
  + ' news.created_ts, news.updated_ts, news.publishing_ts, news.is_active'
  + ' from news, users where news.user_id = users.id and news.is_active = 1 and news.publishing_ts <= CURRENT_TIMESTAMP'
  + ' order by news.publishing_ts desc';

const SELECT_FILTERED_ACTIVE_NEWS_WITH_LOGIN = 'select news.id, news.title, news.description, news.content, users.login,'
  + ' news.created_ts, news.updated_ts, news.publishing_ts, news.is_active'
  + ' from news, users where news.user_id = users.id and news.is_active = 1 and news.publishing_ts <= CURRENT_TIMESTAMP'
  + ' and match(news.title, news.description) against (### in BOOLEAN  mode)';

const SELECT_NEWS_BY_ID = 'select * from news where id = ?';

const SELECT_NEWS_BY_ID_AND_USER_ID = 'select * from news where id = ? and user_id = ?';

const SELECT_NEWS_WITH_LOGIN = 'select news.id, news.title, news.description, news.content, users.login,'
  + ' news.created_ts, news.updated_ts, news.publishing_ts, news.is_active'
  + ' from news, users where news.user_id = users.id';

const SELECT_NEWS_BY_USER_ID = 'select * from news where user_id = ?';

const SELECT_NEWS_WITH_LOGIN_BY_USER_ID = 'select news.id, news.title, news.description, news.content, users.login,'
  + ' news.created_ts, news.updated_ts, news.publishing_ts, news.is_active'
  + ' from news, users where news.user_id = ? and news.user_id = users.id';

const INSERT_NEWS = 'insert into news (title, description, content, publishing_ts, is_active, user_id) values (?, ?, ?, ?, ?, ?)';

const UPDATE_NEWS_BY_ID = 'update news set title = ?, description = ?, content = ?, publishing_ts = ?, is_active = ? where id = ?';

const SELECT_USERS = 'select * from users';

const SELECT_USERS_WITHOUT_PASSWORD = 'select id, login, email, role_id, is_active, fails_count,'
  + ' created_ts, updated_ts from users';

const SELECT_USER_BY_ID = 'select * from users where `id` = ? limit 1';

const SELECT_USER_WITH_PRIVILEGES_BY_ID = 'select users.*, user_roles.privileges' +
  ' from users, user_roles where users.id = ? and users.role_id = user_roles.id';

const SELECT_USER_BY_LOGIN = 'select * from users where `login` = ? limit 1';

const SELECT_USER_WITH_PRIVILEGES_BY_LOGIN = 'select users.*, user_roles.privileges' +
  ' from users, user_roles where login = ? and users.role_id = user_roles.id';

const UPDATE_USER = 'update users set login = ?, email = ?, role_id = ?, is_active = ?, fails_count = ? where  `id` = ?';

const UPDATE_ACCOUNT = 'update users set login = ?, email = ?, role_id = ? where `id` = ?';

const UPDATE_USER_PASSWORD = 'update users set password = ? where `id` = ?';

const INSERT_USER = 'insert into users (login, email, password) values (?, ?, ?)';

const INSERT_SESSION = 'insert into sessions (user_id, token) values (?, ?)';

const UPDATE_SESSION = 'update sessions set {FIELDS} where `token` = ?';

const SELECT_SESSION = 'select * from sessions where `token` = ? limit 1';

const SELECT_ROLES = 'select * from user_roles';

const SELECT_ROLE_BY_ID = 'select * from user_roles where `id` = ? limit 1';

const SELECT_ROLE_BY_NAME = 'select * from user_roles where `name` = ? limit 1';

const UPDATE_ROLE = 'update user_roles set name = ?, privileges = ? where `id` = ?';

const CREATE_ROLE = 'insert into user_roles (name, privileges) values (?, ?)';

const CREATE_IMAGE = 'insert into images (name, original_name, curr_name, size, mimetype,'
  + ' encoding, title, description, user_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?)';

const UPDATE_IMAGE = 'update images set title = ?, description = ? where `id` = ?';

const SELECT_IMAGES_FOR_CLIENT = 'select id, curr_name, size, title, description from images where `user_id` = ?';

const SELECT_IMAGE_BY_ID = 'select * from images where `id` = ?';

const DELETE_IMAGE = 'delete from images where `id` = ?';

const SELECT_NEWS_CONTENT_ITEM_BY_ID = 'select * from news_content where id = ?';

const UPDATE_NEWS_CONTENT = 'update news_content set component = ?, props = ? where `id` = ?';

const CREATE_NEWS_CONTENT = 'insert into news_content (component, props) values (?, ?)';

const pool = mysql.createPool({
  connectionLimit: 10,
  host: 'localhost',
  user: 'fn_admin',
  password: 'fn_password',
  database: 'fast_news'
});

exports.getNewsContentItemById = function(id) {
  return new Promise((resolve, reject) => {
    pool.query(
      { sql: SELECT_NEWS_CONTENT_ITEM_BY_ID },
      [id],
      callbacks.cbSelect('getNewsContentItemById', resolve, reject),
    );
  });
};

exports.updateNewsContent = function(data) {
  const { id, component, props } = data;
  return new Promise((resolve, reject) => {
    pool.query(
      { sql: UPDATE_NEWS_CONTENT },
      [component, props, id],
      callbacks.cbUpdate('updateNewsContent', resolve, reject),
    );
  });
};

exports.createNewsContent = function(data) {
  const { component, props } = data;
  return new Promise((resolve, reject) => {
    pool.query(
      { sql: CREATE_NEWS_CONTENT },
      [component, props],
      callbacks.cbInsert('createNewsContent', resolve, reject),
    );
  });
};

exports.getNews = function() {
  return new Promise((resolve, reject) => {
    pool.query(
      { sql: SELECT_NEWS },
      [],
      callbacks.cbSelect('getNews', resolve, reject),
    );
  });
};

exports.getPublishedNewsWithLogins = function(search) {
  return new Promise((resolve, reject) => {
    pool.query(
      {
        sql: search
          ? SELECT_FILTERED_ACTIVE_NEWS_WITH_LOGIN.replace('###', `'*${search}*'`)
          : SELECT_ACTIVE_NEWS_WITH_LOGIN
      },
      [],
      callbacks.cbSelect('getPublishedNewsWithLogins', resolve, reject),
    );
  });
};

exports.getNewsById = function(id) {
  return new Promise((resolve, reject) => {
    pool.query(
      { sql: SELECT_NEWS_BY_ID },
      [id],
      callbacks.cbSelect('getNewsById', resolve, reject),
    );
  });
};

exports.getNewsByIdAndUserId = function(id, user_id) {
  return new Promise((resolve, reject) => {
    pool.query(
      { sql: SELECT_NEWS_BY_ID_AND_USER_ID },
      [id, user_id],
      callbacks.cbSelect('getNewsByIdAndUserId', resolve, reject),
    );
  });
};

exports.getNewsWithLogin = function() {
  return new Promise((resolve, reject) => {
    pool.query(
      { sql: SELECT_NEWS_WITH_LOGIN },
      [],
      callbacks.cbSelect('getNewsWithLogin', resolve, reject),
    );
  });
};

exports.getNewsByUserId = function(userId) {
  return new Promise((resolve, reject) => {
    pool.query(
      { sql: SELECT_NEWS_BY_USER_ID },
      [userId],
      callbacks.cbSelect('getNewsByUserId', resolve, reject),
    );
  });
};

exports.getNewsWithLoginByUserId = function(userId) {
  return new Promise((resolve, reject) => {
    pool.query(
      { sql: SELECT_NEWS_WITH_LOGIN_BY_USER_ID },
      [userId],
      callbacks.cbSelect('getNewsWithLoginByUserId', resolve, reject),
    );
  });
};

exports.createNews = function(news) {
  const { title, description, is_active, content, publishing_ts, user_id } = news;
  return new Promise((resolve, reject) => {
    pool.query(
      { sql: INSERT_NEWS },
      [title, description, content, publishing_ts, is_active, user_id],
      callbacks.cbInsert('createNews', resolve, reject),
    );
  });
};

exports.updateNews = function(news) {
  const { id, title, description, is_active, content, publishing_ts } = news;
  return new Promise((resolve, reject) => {
    pool.query(
      { sql: UPDATE_NEWS_BY_ID },
      [title, description, content, publishing_ts, is_active, id],
      callbacks.cbUpdate('updateNews', resolve, reject),
    );
  });
};

exports.getUsers = async function() {
  return new Promise((resolve, reject) => {
    pool.query(
      { sql: SELECT_USERS },
      [],
      callbacks.cbSelect('getUsers', resolve, reject),
    );
  });
};

exports.getUsersWithoutPassword = async function() {
  return new Promise((resolve, reject) => {
    pool.query(
      { sql: SELECT_USERS_WITHOUT_PASSWORD },
      [],
      callbacks.cbSelect('getUsersWithoutPassword', resolve, reject),
    );
  });
};

exports.getUserById = async function(id) {
  return new Promise((resolve, reject) => {
    pool.query(
      { sql: SELECT_USER_BY_ID },
      [id],
      callbacks.cbSelect('getUserById', resolve, reject),
    );
  });
};

exports.getUserWithPrivilegesById = async function(id) {
  return new Promise((resolve, reject) => {
    pool.query(
      { sql: SELECT_USER_WITH_PRIVILEGES_BY_ID },
      [id],
      callbacks.cbSelect('getUserWithPrivilegesById', resolve, reject),
    );
  });
};

exports.getUserWithPrivilegesByLogin = async function(login) {
  return new Promise((resolve, reject) => {
    pool.query(
      { sql: SELECT_USER_WITH_PRIVILEGES_BY_LOGIN },
      [login],
      callbacks.cbSelect('getUserWithPrivilegesByLogin', resolve, reject),
    );
  });
};

exports.getUserByLogin = async function(login) {
  return new Promise((resolve, reject) => {
    pool.query(
      { sql: SELECT_USER_BY_LOGIN },
      [login],
      callbacks.cbSelect('getUserByLogin', resolve, reject),
    );
  });
};

exports.updateUser = async function(user) {
  return new Promise((resolve, reject) => {
    const { id, login, email, role_id, is_active, fails_count } = user;
    pool.query(
      { sql: UPDATE_USER },
      [login, email, role_id, is_active, fails_count, id],
      callbacks.cbUpdate('updateUser', resolve, reject),
    );
  });
};

exports.updateAccount = function(user) {
  return new Promise((resolve, reject) => {
    const { id, login, email, role_id } = user;
    pool.query(
      { sql: UPDATE_ACCOUNT },
      [login, email, role_id, id],
      callbacks.cbUpdate('updateAccount', resolve, reject),
    );
  });
};

exports.updateUserPassword = async function(user) {
  return new Promise((resolve, reject) => {
    const { id, password } = user;
    pool.query(
      { sql: UPDATE_USER_PASSWORD },
      [password, id],
      callbacks.cbUpdate('updateUserPassword', resolve, reject),
    );
  });
};

exports.createUser = async function(user) {
  const { login, email, password } = user;
  return new Promise((resolve, reject) => {
    pool.query(
      { sql: INSERT_USER },
      [login, email, password],
      callbacks.cbInsert('createUser', resolve, reject),
    );
  })
    .then((result) => {
      if (!result || result.affectedRows < 1) {
        throw new BasicError('Error: user was not created');
      }
      const { insertId } = result;
      return exports.getUserWithPrivilegesById(insertId);
    })
};

// todo
exports.createSession = async function(user, token) {
  return new Promise((resolve, reject) => {
    pool.query(
      { sql: INSERT_SESSION },
      [user.id, token],
      (error, result) => {
        if (error) {
          const date = new Date();
          log(`${date.toUTCString()} - 500 - Error: ${error.message}`, false);
          reject({ message: error.message, date });
        } else {
          log(`mysql DAO - createSession - result: affected rows: ${result.affectedRows}`);
          resolve({ user, token });
        }
      },
    );
  });
};

// todo
exports.updateSession = async function (session) {
  const { token, is_expired, requests_count } = session;
  let fields = 'is_expired = ?';
  let values = [is_expired, token];

  if (isExists(requests_count)) {
    fields = `requests_count = ?, ${fields}`;
    values = [requests_count, ...values];
  }
  const sqlString = UPDATE_SESSION.replace('{FIELDS}', fields);

  return new Promise((resolve, reject) => {
    pool.query(
      { sql: sqlString },
      values,
      (error, result) => {
        if (error) {
          const date = new Date();
          log(`${date.toUTCString()} - 500 - Error: ${error.message}`, false);
          reject({ message: error.message, date });
        } else {
          log(`mysql DAO - updateSession - result: changed rows: ${result.changedRows}`);
          resolve(session);
        }
      },
    );
  });
};

exports.getSession = async function(token) {
  return new Promise((resolve, reject) => {
    pool.query(
      { sql: SELECT_SESSION },
      [token],
      callbacks.cbSelect('getSession', resolve, reject),
    );
  });
};

exports.getRoles = function() {
  return new Promise((resolve, reject) => {
    pool.query(
      { sql: SELECT_ROLES },
      [],
      callbacks.cbSelect('getRoles', resolve, reject),
    );
  });
};

exports.getRoleById = function(id) {
  return new Promise((resolve, reject) => {
    pool.query(
      { sql: SELECT_ROLE_BY_ID },
      [id],
      callbacks.cbSelect('getRoleById', resolve, reject),
    );
  })
};

exports.getRoleByName = function(name) {
  return new Promise((resolve, reject) => {
    pool.query(
      { sql: SELECT_ROLE_BY_NAME },
      [name],
      callbacks.cbSelect('getRoleByName', resolve, reject),
    );
  })
};

// todo
exports.updateRole = function(role) {
  const { name, privileges, id } = role;
  return new Promise((resolve, reject) => {
    pool.query(
      { sql: UPDATE_ROLE },
      [name, privileges, id],
      (error, result) => {
        if (error) {
          const date = new Date();
          log(`${date.toUTCString()} - 500 - Error: ${error.message}`, false);
          reject({ message: error.message, date });
        } else {
          log(`mysql DAO - updateRole - result: changed rows: ${result.changedRows}`);
          resolve({ result, role });
        }
      },
    );
  })
};

exports.createRole = function(role) {
  const { name, privileges } = role;
  return new Promise((resolve, reject) => {
    pool.query(
      { sql: CREATE_ROLE },
      [name, privileges],
      callbacks.cbInsert('createRole', resolve, reject),
    );
  });
};

// todo
exports.createImage = function(image) {
  return new Promise((resolve, reject) => {
    pool.query(
      { sql: CREATE_IMAGE },
      [
        image.name,
        image.original_name,
        image.curr_name,
        image.size,
        image.mimetype,
        image.encoding,
        image.title,
        image.description,
        image.user_id,
      ],
      (error, result) => {
        if (error) {
          const date = new Date();
          log(`${date.toUTCString()} - 500 - Error: ${error.message}`, false);
          reject({ message: error.message, date });
        } else {
          log(`mysql DAO - createFile - result: affected rows: ${result.affectedRows}`);
          resolve({ ...image, id: result.insertId });
        }
      },
    );
  });
};

exports.updateImage = function(image) {
  const { id, title, description } = image;
  return new Promise((resolve, reject) => {
    pool.query(
      { sql: UPDATE_IMAGE },
      [title, description, id],
      callbacks.cbUpdate('updateImage', resolve, reject),
    );
  });
};

exports.getImagesForClientByUserId = function(userId) {
  return new Promise((resolve, reject) => {
    pool.query(
      { sql: SELECT_IMAGES_FOR_CLIENT },
      [userId],
      callbacks.cbSelect('getImagesForClientByUserId', resolve, reject),
    );
  })
};

exports.getImageById = function(id) {
  return new Promise((resolve, reject) => {
    pool.query(
      { sql: SELECT_IMAGE_BY_ID },
      [id],
      callbacks.cbSelect('getImageById', resolve, reject),
    );
  })
};

exports.deleteImage = function(id) {
  return new Promise((resolve, reject) => {
    pool.query(
      { sql: DELETE_IMAGE },
      [id],
      callbacks.cbDelete('deleteImage', resolve, reject),
    );
  });
};
