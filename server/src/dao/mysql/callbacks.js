const log = require('../../logger/log');

const RESULT_FIELDS = {
  AFFECTED_ROWS: 'affectedRows',
  CHANGED_ROWS: 'changedRows',
  LENGTH: 'length',
  INSERT_ID: 'insertId',
};

const ACTION_TYPES = {
  FOUND: 'found',
  AFFECTED: 'affected',
  CHANGED: 'changed',
};

const createGetSuccessMessage = actionType => (method, value) => (
  `mysql DAO - ${method} - result: ${value} rows ${actionType}`
);

const createGetErrorMessage = (method, error, date) => isDetailed => (
  `${date.toUTCString()} - 500 - ${
    isDetailed
      ? `Error: ${method}: ${error.message}`
      : 'Internal server error. Connect to your service administrator'
  }`
);

const DB_OPERATIONS = {
  SELECT: {
    prefix: 'get',
    field: RESULT_FIELDS.LENGTH,
    getSuccessMessage: createGetSuccessMessage(ACTION_TYPES.FOUND),
    createGetErrorMessage,
  },
  INSERT: {
    prefix: 'create',
    field: RESULT_FIELDS.AFFECTED_ROWS,
    getSuccessMessage: createGetSuccessMessage(ACTION_TYPES.AFFECTED),
    createGetErrorMessage,
  },
  UPDATE: {
    prefix: 'update',
    field: RESULT_FIELDS.CHANGED_ROWS,
    getSuccessMessage: createGetSuccessMessage(ACTION_TYPES.CHANGED),
    createGetErrorMessage,
  },
  DELETE: {
    prefix: 'delete',
    field: RESULT_FIELDS.AFFECTED_ROWS,
    getSuccessMessage: createGetSuccessMessage(ACTION_TYPES.AFFECTED),
    createGetErrorMessage,
  },
};

function createDaoCallback(method, dbOperation, resolve, reject) {
  const {
    field,
    getSuccessMessage,
    createGetErrorMessage,
  } = dbOperation;
  return (error, result) => {
    if (error) {
      const date = new Date();
      const getErrorMessage = createGetErrorMessage(method, error, date);
      log(getErrorMessage(true), false); // for logs
      reject({ message: getErrorMessage(false), date }); // for response to client
    } else {
      log(getSuccessMessage(method, result[field]));
      resolve(result);
    }
  };
}

module.exports = {
  cbSelect: (method, resolve, reject) => createDaoCallback(method, DB_OPERATIONS.SELECT, resolve, reject),
  cbInsert: (method, resolve, reject) => createDaoCallback(method, DB_OPERATIONS.INSERT, resolve, reject),
  cbUpdate: (method, resolve, reject) => createDaoCallback(method, DB_OPERATIONS.UPDATE, resolve, reject),
  cbDelete: (method, resolve, reject) => createDaoCallback(method, DB_OPERATIONS.DELETE, resolve, reject),
};
