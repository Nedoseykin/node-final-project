const { isFilledString, isFilledArray, isNumber } = require('../utils/common-utils');
const BasicError = require('../domain/errors/BasicError');

const FIELDS = {
  title: [
    {
      validator: isFilledString,
      message: 'Title is required',
      usage: ['upload', 'update'],
    },
  ],

  fileSize: [
    {
      validator: value => {
        const parsed = parseInt(value, 10);
        return isNumber(parsed) && (parsed > 0);
      },
      message: 'File is required',
      usage: ['upload'],
    },
  ],
};

exports.upload = async function (payload) {
  if (!payload) {
    throw new BasicError('No data were provided', 400);
  }
  const validation = { isValid: true };
  const fields = Object.keys(FIELDS);
  for (let i = 0; i < fields.length; i++) {
    const key = fields[i];
    const validators = FIELDS[key];
    if (isFilledArray(validators)) {
      for (let j = 0; j < validators.length; j++) {
        const { validator, message, usage } = validators[j];
        if (usage.includes('upload')) {
          const test = validator(payload[key], payload);
          if (!test) {
            validation[key] = message;
            validation.isValid = false;
            break;
          }
        }
      }
    }
  }
  return validation;
};

exports.update = async function (payload) {
  if (!payload) {
    throw new BasicError('No data were provided', 400);
  }
  const validation = { isValid: true };
  const fields = Object.keys(FIELDS);
  for (let i = 0; i < fields.length; i++) {
    const key = fields[i];
    const validators = FIELDS[key];
    if (isFilledArray(validators)) {
      for (let j = 0; j < validators.length; j++) {
        const { validator, message, usage } = validators[j];
        if (usage.includes('update')) {
          const test = validator(payload[key], payload);
          if (!test) {
            validation[key] = message;
            validation.isValid = false;
            break;
          }
        }
      }
    }
  }
  return validation;
};
