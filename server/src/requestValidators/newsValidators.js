const { isExists, isFilledString } = require('../utils/common-utils');
const BasicError = require('../domain/errors/BasicError');
const { validate } = require('../utils/validation-utils');

const FIELDS = {
  title: [
    {
      validator: isFilledString,
      message: 'Title is required',
      usage: ['create', 'update'],
    },
  ],
  description: [
    {
      validator: isFilledString,
      message: 'Description is required',
      usage: ['create', 'update'],
    },
  ],
  is_active: [
    {
      validator: isExists,
      message: 'Is active field is required',
      usage: ['create', 'update'],
    },
  ],
  publishing_ts: [
    {
      validator: isFilledString,
      message: 'Publication date is required',
      usage: ['create', 'update'],
    },
  ],
  content: [],
};

exports.update = async function (payload) {
  if (!payload) {
    throw new BasicError('No data were provided', 400);
  }
  return validate(payload, 'update', FIELDS);
};

exports.create = async function (payload) {
  if (!payload) {
    throw new BasicError('No data were provided', 400);
  }
  return validate(payload, 'create', FIELDS);
};
