const { isFilledString, isFilledArray, isNumber } = require('../utils/common-utils');
const BasicError = require('../domain/errors/BasicError');
const dao = require('../dao/mysql/mysqlDao');
const { validate } = require('../utils/validation-utils');

const FIELDS = {
  login: [
    {
      validator: isFilledString,
      message: 'Login is required',
      usage: ['update'],
    }
  ],
  email: [
    {
      validator: isFilledString,
      message: 'E-mail is required',
      usage: ['update'],
    }
  ],
};

exports.update = async function (payload) {
  if (!payload) {
    throw new BasicError('No data were provided', 400);
  }
  const validation = validate(payload,'update', FIELDS);
  if (!validation.isValid && isFilledString(validation.login)) {
    return validation;
  }
  const data = await dao.getUserByLogin(payload.login);
  if (isFilledArray(data) && (data[0].id !== payload.id)) {
    validation.isValid = false;
    validation.login = 'Login should be unique';
  }
  return validation;
};
