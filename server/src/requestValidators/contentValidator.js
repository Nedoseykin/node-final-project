const { isExists, isFilledString } = require('../utils/common-utils');
const BasicError = require('../domain/errors/BasicError');
const { validate } = require('../utils/validation-utils');

const FIELDS = {
  id: [
    {
      validator: isExists,
      message: 'Id is required',
      usage: ['update'],
    },
    {
      validator: v => v > 0,
      message: 'Id should be greater than 0',
      usage: ['update'],
    },
  ],
  component: [
    {
      validator: isFilledString,
      message: 'Component is required',
      usage: ['create', 'update'],
    },
  ],
  props: [],
};

exports.update = async function (payload) {
  if (!payload) {
    throw new BasicError('No data were provided', 400);
  }
  return validate(payload, 'update', FIELDS);
};

exports.create = async function (payload) {
  if (!payload) {
    throw new BasicError('No data were provided', 400);
  }
  return validate(payload, 'create', FIELDS);
};
