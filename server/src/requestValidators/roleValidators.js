const { isFilledString, isFilledArray, isNumber } = require('../utils/common-utils');
const BasicError = require('../domain/errors/BasicError');
const dao = require('../dao/mysql/mysqlDao');

const FIELDS = {
  name: [
    {
      validator: isFilledString,
      message: 'Name is required',
    }
  ],
  privileges: [
    {
      validator: isNumber,
      message: 'Privileges are required',
    }
  ],
};

exports.create = async function (payload) {
  if (!payload) {
    throw new BasicError('No data were provided', 400);
  }
  const validation = { isValid: true };
  const fields = Object.keys(FIELDS);
  for (let i = 0; i < fields.length; i++) {
    const key = fields[i];
    const validators = FIELDS[key];
    if (isFilledArray(validators)) {
      for (let j = 0; j < validators.length; j++) {
        const { validator, message } = validators[j];
        const test = validator(payload[key], payload);
        if (!test) {
          validation[key] = message;
          validation.isValid = false;
          break;
        }
      }
    }
  }
  if (!validation.isValid && isFilledString(payload.name)) {
    return validation;
  }
  const data = await dao.getRoleByName(payload.name);
  if(isFilledArray(data)) {
    validation.isValid = false;
    validation.login = 'Name should be unique';
  }
  return validation;
};
