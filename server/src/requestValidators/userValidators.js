const { isFilledString, isFilledArray } = require('../utils/common-utils');
const BasicError = require('../domain/errors/BasicError');
const dao = require('../dao/mysql/mysqlDao');
const { validate } = require('../utils/validation-utils');

// usage of validator: validator(fieldValue, payload) where payload is { field1: someValue, field2: fieldValue, ...}
const FIELDS = {
  login: [
    {
      validator: isFilledString,
      message: 'Login is required',
      usage: ['registration', 'update'],
    },
  ],
  email: [
    {
      validator: isFilledString,
      message: 'E-mail is required',
      usage: ['registration', 'update'],
    },
  ],
  password: [
    {
      validator: isFilledString,
      message: 'Password is required',
      usage: ['registration'],
    },
  ],
  currentPassword: [
    {
      validator: isFilledString,
      message: 'Current password is required',
      usage: ['changePassword'],
    },
    {
      validator: (currentPassword, payload) => currentPassword === payload.userPassword,
      message: 'Current password is wrong',
      usage: ['changePassword'],
    },
  ],
  newPassword: [
    {
      validator: isFilledString,
      message: 'Password is required',
      usage: ['changePassword'],
    },
  ],
  confirmPassword: [
    {
      validator: isFilledString,
      message: 'Password confirmation is required',
      usage: ['registration', 'changePassword'],
    },
    {
      validator: (confirmPassword, payload) => confirmPassword === payload.password,
      message: 'Confirmation should be equal to password',
      usage: ['registration'],
    },
    {
      validator: (confirmPassword, payload) => confirmPassword === payload.newPassword,
      message: 'Confirmation should be equal to new password',
      usage: ['changePassword'],
    }
  ],
};

exports.update = async function(payload) {
  if(!payload) {
    throw new BasicError('No data were provided', 400);
  }
  const validation = validate(payload, 'update', FIELDS);
  if (!validation.isValid) { return validation; }
  const data = await dao.getUserByLogin(payload.login);
  if (isFilledArray(data) && (data[0].id !== payload.id)) {
    validation.isValid = false;
    validation.login = 'Login should be unique';
  }
  return validation;
};

exports.registration = async function(payload) {
  if (!payload) {
    throw new BasicError('No data were provided', 400);
  }
  const validation = validate(payload, 'registration', FIELDS);
  if (!validation.isValid) { return validation; }
  const data = await dao.getUserByLogin(payload.login);
  if (isFilledArray(data)) {
    validation.isValid = false;
    validation.login = 'Login should be unique';
  }
  return validation;
};

exports.changePassword = async function (payload) {
  console.log('payload validation: ', payload);
  if (!payload) {
    throw new BasicError('No data were provided', 400);
  }
  return validate(payload, 'changePassword', FIELDS);
};
