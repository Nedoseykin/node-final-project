const path = require('path');
const fs = require('fs');


const fileName = path.resolve(__dirname, 'process', 'server.pid');
const isExist = fs.existsSync(fileName);

new Promise((resolve, reject) => {
  if (!isExist) {
    reject({ message: 'File is not found' });
  }
  fs.readFile(fileName, 'utf-8', (error, result) => {
    if (error) {
      reject(error);
    }
    resolve(result);
  });
})
  .then((result) => {
    const pid = Number(result);
    if (!pid || isNaN(pid)) {
      console.log('Error: wrong PID: ', pid);
      process.exit(1);
    }
    console.log('Kill process with PID', pid);
    process.kill(pid);
    process.exit(0);
  })
  .catch((error) => {
    console.log('Error: ', error.message);
    process.exit(1);
  });
