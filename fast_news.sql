-- MySQL dump 10.13  Distrib 5.7.29, for Linux (x86_64)
--
-- Host: localhost    Database: fast_news
-- ------------------------------------------------------
-- Server version	5.7.29-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL DEFAULT '',
  `original_name` varchar(256) NOT NULL DEFAULT '',
  `curr_name` varchar(256) NOT NULL DEFAULT '',
  `size` int(10) unsigned NOT NULL DEFAULT '0',
  `mimetype` varchar(50) NOT NULL DEFAULT '',
  `encoding` varchar(50) NOT NULL DEFAULT '',
  `title` varchar(256) NOT NULL DEFAULT '',
  `description` varchar(1024) NOT NULL DEFAULT '',
  `created_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT INTO `images` VALUES (1,'castle.jpeg','1336065246_krasivie_zamki_na_bygaga.com.ua-55.jpg','image_3d462048b89eda6abdd15cab6efdd883b2bf8b43b30603033470f9dfe3d5bf77',35936,'image/jpeg','7bit','Castle from stone','In the water','2020-02-06 00:09:59','2020-02-06 00:10:27',1,0),(4,'zamok_mansanares-ehl-real_vorota.jpg','zamok_mansanares-ehl-real_vorota.jpg','image_c1ace2a7353cadabf642d6f42e5db181beb88eff5fbeb52368b7ebd69054e1e0',871710,'image/jpeg','7bit','Castle','Front view','2020-02-07 14:15:45','2020-02-07 14:15:45',2,0),(5,'zamok_del_monte.jpg','zamok_del_monte.jpg','image_11d0881819bf0c7e6d0f12f08eed30d919cecb3050d616592d5c967d1e1a4730',1019128,'image/jpeg','7bit','Castle with towers','On a hill','2020-02-07 14:16:54','2020-02-07 14:16:54',2,0),(6,'curia_by_antoniodeluca-d63voy4.jpg','curia_by_antoniodeluca-d63voy4.jpg','image_03d8fb9508e60f4199ef961ba3b1ac460e0a39c146a9f178cbaaeaa883dfb8b9',2956593,'image/jpeg','7bit','Picture in zala','test','2020-02-07 23:06:54','2020-02-07 23:06:54',2,0),(7,'desert_mecha_concept_by_antoniodeluca-d66la20.jpg','desert_mecha_concept_by_antoniodeluca-d66la20.jpg','image_4c87da5716e9b79e1a3b398a80c1afcd42b6fdc5ec88436739215740ef45103f',2128735,'image/jpeg','7bit','Spider','Antonio De Luca','2020-02-08 11:32:46','2020-02-08 11:32:46',2,0),(8,'jealousy_by_antoniodeluca-d63vtgo.jpg','jealousy_by_antoniodeluca-d63vtgo.jpg','image_395bf57a925e7c70f1791dc165c2b9caa552c26ea7c824d1cf71dc4e49b0daac',3388074,'image/jpeg','7bit','Night visit','Tsss','2020-02-08 11:34:32','2020-02-08 11:34:32',2,0),(9,'979824_629457870399586_618756696_o.jpg','979824_629457870399586_618756696_o.jpg','image_725a4cf3925a0f2537ecd2156122875c7a3db9c6231ae9291534faab5c75b487',202252,'image/jpeg','7bit','Man octopus','Horrow','2020-02-09 15:05:02','2020-02-09 15:05:02',4,0),(10,'1005757_657442254267814_567061598_n.jpg','1005757_657442254267814_567061598_n.jpg','image_ec753a72dd35915b22cc1c30eb7b5da91bf429f13887ea8010d919f6eaef926d',60949,'image/jpeg','7bit','Face','With tortoise','2020-02-09 22:10:31','2020-02-09 22:11:05',5,0),(11,'6-11-750x545.jpg','6-11-750x545.jpg','image_98bd7023e464375bba4a11fc2d3b53aa040904fa98d434c2506520a641b6d082',75428,'image/jpeg','7bit','Romantic cat','Cat with flower','2020-02-22 21:36:10','2020-02-22 21:36:10',6,0);
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(256) NOT NULL DEFAULT '',
  `description` varchar(1024) NOT NULL DEFAULT '',
  `content` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) unsigned DEFAULT '0',
  `created_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `publishing_ts` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  FULLTEXT KEY `searchText` (`title`,`description`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (1,'Spider Techno','fantastic',1,2,'2020-02-14 20:14:23','2020-02-22 20:33:53','2020-02-14 00:00:00',1),(2,'Night visit of woman-killer','In the old castle at night',2,2,'2020-02-14 20:30:18','2020-02-22 20:36:22','2020-02-23 00:00:00',1),(3,'Кот - романтик','Все, как у людей)',3,6,'2020-02-15 15:27:14','2020-02-23 16:23:56','2020-02-15 00:00:00',1),(4,'Русский язык на сайте!','Таки смог правильно написать конфигурацию для русского языка',4,2,'2020-02-23 16:04:17','2020-02-23 16:18:21','2020-02-22 21:00:00',1);
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news_content`
--

DROP TABLE IF EXISTS `news_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `component` varchar(256) NOT NULL DEFAULT '',
  `props` varchar(7168) NOT NULL DEFAULT '',
  `created_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news_content`
--

LOCK TABLES `news_content` WRITE;
/*!40000 ALTER TABLE `news_content` DISABLE KEYS */;
INSERT INTO `news_content` VALUES (1,'BasicNewsLayout','{\"textPosition\":\"POSITION_LEFT\",\"imageSrc\":\"/images/image_4c87da5716e9b79e1a3b398a80c1afcd42b6fdc5ec88436739215740ef45103f\",\"text\":\"Spider Techno\"}','2020-02-22 20:33:27','2020-02-23 12:19:16'),(2,'BasicNewsLayout','{\"imageSrc\":\"/images/image_395bf57a925e7c70f1791dc165c2b9caa552c26ea7c824d1cf71dc4e49b0daac\",\"textPosition\":\"POSITION_LEFT\",\"text\":\"Night visit of woman-killer\"}','2020-02-22 20:35:49','2020-02-22 20:35:49'),(3,'BasicNewsLayout','{\"imageSrc\":\"/images/image_98bd7023e464375bba4a11fc2d3b53aa040904fa98d434c2506520a641b6d082\",\"textPosition\":\"POSITION_RIGHT\",\"text\":\"Любит-не любит, мяукнет, или там еще чего...\"}','2020-02-22 21:37:01','2020-02-23 16:23:54'),(4,'BasicNewsLayout','{\"textPosition\":\"POSITION_RIGHT\",\"text\":\"Русский язык в приложении\",\"imageSrc\":\"/images/image_c1ace2a7353cadabf642d6f42e5db181beb88eff5fbeb52368b7ebd69054e1e0\"}','2020-02-23 16:18:18','2020-02-23 16:18:18');
/*!40000 ALTER TABLE `news_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` varchar(300) NOT NULL,
  `created_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `requests_count` int(11) NOT NULL DEFAULT '0',
  `is_expired` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id_key` (`user_id`),
  CONSTRAINT `user_id_key` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES (1,1,'625839d9806d2df30dd2ae4186581060906ddd390c0e317c1745f31624a8bfd0','2020-02-06 00:08:22','2020-02-06 00:11:30',11,1),(2,2,'b42163aa83eca04dfa04e10378941f1bbfe856fd0d63a320cbd07f4239b794a1','2020-02-06 00:11:55','2020-02-06 00:12:15',2,1),(3,1,'5702df8d4694120c866d30d0f62ed6ab4829df2adccb4dc17805a3f36399f969','2020-02-06 00:12:21','2020-02-06 00:12:40',4,1),(4,2,'acc1c9e79c7aec6a5a5cbedd54f278858e19cdaae5b077f221ebb515dd61b471','2020-02-06 00:12:45','2020-02-06 00:16:02',8,1),(5,2,'a7e4cfe51d3b71356ad4a6947303553ccf00abf04d43a657b4cea9e177d1acff','2020-02-06 00:16:43','2020-02-06 00:16:57',2,1),(6,3,'e2e78cdbe1cec3b54e4a64480175091da66258f99060819f3ababf3ba36e64a8','2020-02-06 00:17:15','2020-02-06 00:18:27',2,1),(7,2,'bfade275eb7adeee9681d7fd92d6cb1f8fb3c35e3dc455b055bfe3418df7530e','2020-02-06 00:18:31','2020-02-06 00:18:47',3,1),(8,3,'cc0a7b2341f278c597ce42a7a8a6de170f282853e8ddeb424854ce634a28a218','2020-02-06 00:18:55','2020-02-06 00:19:53',3,1),(9,2,'5d51e30f53411b7ea668674a3e89e027987da777b2e0cfd81e688992e268ba54','2020-02-06 17:35:22','2020-02-06 17:35:48',2,1),(10,2,'c985fc2007e073970a0f1e259a2a0159421c9f1d4e5df7dde3ccd59f019eafa9','2020-02-06 17:35:51','2020-02-06 17:37:04',4,1),(11,1,'bd429a1d46428a7ba8acf1ffc91ace9ce2ad7b941b65a7ed8f5f79abbdcd3cdc','2020-02-06 17:37:11','2020-02-06 17:37:14',0,1),(12,3,'7baf3c9c7ac3b0e49933b23b4b5eb8d5233cb0f7eabb362e351cf1aa836b781b','2020-02-06 17:37:19','2020-02-06 17:37:26',1,1),(13,2,'67d0f1d4838034bca24b4714ad07f279ce661539a1548a878a0f426acc9bbdb1','2020-02-06 17:37:30','2020-02-06 17:38:21',8,0),(14,2,'9a3442ed4805306231b169af790373b27bf5ea0faca3cb0a8df3f1b9a42cf578','2020-02-06 17:38:57','2020-02-06 17:39:03',3,0),(15,2,'b345385fcb357d87e7309478558af007f9a50ab0ac5d6d2a5fc566a66ecb3e66','2020-02-07 14:13:53','2020-02-07 14:18:14',11,1),(16,2,'2590bb3eae6b4e880e8b9084922fad7e94cb5e017cfcf4d765007dffa4eb4608','2020-02-07 14:39:09','2020-02-07 14:39:11',1,0),(17,2,'d2e225947cadafebea3b71af77059570640c64c83ed244ee1e5c3f910b4e4332','2020-02-07 14:41:31','2020-02-07 14:41:41',3,0),(18,2,'3f574e834c9b99dec54e76676f884d3f8d958b7fc49ab4b06e59c1e685a6f661','2020-02-07 19:01:35','2020-02-07 19:02:47',3,0),(19,2,'202f0bdcda49d1e2a95c7630ee7febe374848c8fb2070d72bdc4bb533b77ce7d','2020-02-07 19:44:06','2020-02-07 19:50:53',4,0),(20,2,'0a04cce404a355f61ab4ac5bd7d9d24be9a9caa08a0003e2f959fcea62b799ed','2020-02-07 20:01:06','2020-02-07 20:03:39',5,0),(21,2,'1c3b260eafd67d27bdcc767898133ac412ea6c740e1ade9566addf366b2a8a45','2020-02-07 20:14:49','2020-02-07 20:46:04',1,1),(22,2,'c9cd6108d5b68cd41fa3b008e27ec7e59fc29bf8a38f38755db3af50e26ba0a5','2020-02-07 20:38:01','2020-02-07 23:21:20',0,1),(23,2,'ec538bc239a7431706e8a9524361a8932229e3ac249ba32242234db3ad556704','2020-02-07 20:46:13','2020-02-07 21:46:34',8,1),(24,2,'e486ed846ede0ae2289d8048fbdc4a8c8c84fc918a59deb639bfce2614e2d59c','2020-02-07 21:46:43','2020-02-07 22:14:31',3,1),(25,2,'55df86f6529f4338655e6ab956c6d1e31e0eac2e1862b373755cda333e8f3197','2020-02-07 22:14:34','2020-02-07 22:58:47',3,1),(26,2,'e58a84270f52fd028e237d9bccfe65774e95382cdf607ea25cb0305405b3664f','2020-02-07 22:58:50','2020-02-07 23:01:30',3,1),(27,2,'f233ebbf6d4859b83ba78787c170f66cbeaeb03d9144fa33de06d834f00b2f7d','2020-02-07 23:01:36','2020-02-07 23:06:54',2,0),(28,2,'6b74392dc2400ad64bb7d461bfa578e09c20f88975e19f246fafe25740a1478b','2020-02-07 23:21:24','2020-02-07 23:31:16',4,1),(29,2,'9b54eb3a67c4f593f1dc6da62bf7089941a477fff74d921b88d4d64faf481f76','2020-02-07 23:31:30','2020-02-07 23:39:25',10,0),(30,2,'0184d2f0b15bcc03be3eb2e6649f5edcd394fb76c3a16ff497b549a4de0a579f','2020-02-08 08:51:42','2020-02-08 12:08:20',1,1),(31,2,'8601e6531cd1556339ddd9964777e70a6e05bec1116013d10ebfdae07bbefca5','2020-02-08 10:33:55','2020-02-08 11:22:05',2,1),(32,2,'a4e10ffc9df9811b9d4bd6049eab7694619a5a8d302a3b6b9cd5531bcf65ec20','2020-02-08 11:22:10','2020-02-08 11:34:32',6,0),(33,2,'05af71d1f8fc962000f19082ee247c7a921ec07a8a194e637b17c56d07523df2','2020-02-08 12:08:39','2020-02-08 17:41:54',32,1),(34,2,'18df03b5fe4e5ba3c9106aa4fcc1479c11cde9bcb9ab72dc680d1b220abeeb4b','2020-02-08 17:43:49','2020-02-08 17:46:09',4,0),(35,2,'f4fbfa6f1a71787b0dcb95911eb85da334bd52ddea40d79d79c59fa7ed9575c9','2020-02-08 17:58:17','2020-02-08 17:58:17',0,0),(36,2,'414d29744e8c7fa8e40e925eaa603f8d84b2d1444959f9589df49ff8b5a18050','2020-02-08 18:34:26','2020-02-08 20:11:31',1,1),(37,2,'b27ae40f2daa0709a403a34809628c3d21144a8336db1e1f97882e4c6d51d13f','2020-02-08 20:11:36','2020-02-08 20:12:54',3,0),(38,2,'ebf878931e8ad58fb205a7c75acaaa300c45fe2a53757011ca228d15c36c71ca','2020-02-09 14:56:54','2020-02-09 15:01:34',7,1),(39,4,'e21f474433ad574ba41e6688faa9e5dc6e5a760a2678d8e21c2db871ed7582a0','2020-02-09 15:02:09','2020-02-09 15:03:05',2,1),(40,2,'a28f766c06ff58e1af820be8913809f7cdd548314c18a651c4a2745811a865be','2020-02-09 15:03:12','2020-02-09 15:04:09',5,1),(41,4,'3900b1b4157e8cb4d6bb7fa6ee55ec7d68aad3aa57029bfe23eed3eb01eb00d7','2020-02-09 15:04:19','2020-02-09 15:44:34',6,1),(42,2,'e858b9abe7865d11db23e948e03af0870acc5d45b297e98153c0556f2859f49d','2020-02-09 15:46:05','2020-02-09 22:05:27',0,1),(43,2,'2ecf7086b351cfa2c42360bea4849b3e66ccdfee5451c3bb8e3e15ccf83f9921','2020-02-09 22:05:36','2020-02-09 22:08:19',11,1),(44,5,'458e7300aa9b1f6a70650417461ef725f8eec7844d4b36f25cdf7e1cabb3635c','2020-02-09 22:08:43','2020-02-09 22:31:57',12,1),(45,5,'e81b8e0b33a85a06067254f6b7679899f128be47a2492a222fdd92b5ff70c0be','2020-02-09 22:32:13','2020-02-09 22:55:58',4,1),(46,2,'6d48616ec4adbfa37cf1076826c49d05762417e02fb157e605f2fb86708a2ee8','2020-02-09 22:56:06','2020-02-09 22:56:06',0,0),(47,2,'406e6d68af39f1ec52579baa436085030ef2067608689be4d6d105a9335d5e4c','2020-02-09 23:18:11','2020-02-09 23:20:12',5,1),(48,2,'48c2d379c89c81f2d5fd0c807246f3af900777022c33dddbe6129622b5c5b595','2020-02-10 07:27:03','2020-02-10 07:28:14',5,1),(49,2,'24e4793f8e5e8f48ecf472fe7ee54c92009cb3ccafc2d73843018e5b42900682','2020-02-10 19:04:29','2020-02-10 19:04:38',2,0),(50,2,'183778ca4b2fad831f00f64e5133c5377b39871263a7af6064dce85312e14749','2020-02-11 19:42:31','2020-02-11 19:42:31',0,0),(51,2,'fcacd124ff399723aecf2c070951bc21556c35e3b1604a86d7540bb4267af49f','2020-02-12 23:19:35','2020-02-12 23:19:50',3,0),(52,2,'4c9133d3fb6c0033e3588d6276a7fd3793efc1f3437a7b57763b992fa70c2db5','2020-02-13 19:04:17','2020-02-13 19:06:14',9,1),(53,2,'82db90a788120017b98b458f37a0af1500841018d87c4dad835c45048cb7be83','2020-02-13 22:46:45','2020-02-13 22:48:43',8,1),(54,2,'488189f9d7ab7d80ede45929342c30e0191d4f7856c53839a7032c1708b3bb86','2020-02-14 20:13:48','2020-02-14 21:07:44',10,1),(55,2,'a400403f0ffd665f423145f0ecfa5bd26a51a1eda8bb899db4c111ba234954e7','2020-02-14 20:59:53','2020-02-14 21:18:46',15,0),(56,2,'0c10a8a0f47b61c20ff8ff7a1f35414f3910cb8784282fcf5d57ff292ee89ae1','2020-02-14 21:07:48','2020-02-14 22:19:31',3,1),(57,2,'b6819434b84332366cf767a8ba9dd0e612330e8dc1a188626be2e24d06eac48d','2020-02-14 22:19:38','2020-02-14 22:19:38',0,0),(58,2,'6b27382f93b845a819b3bb34ed477dcded640c03e4cce7a898f2f8b3c97af5c7','2020-02-15 15:23:35','2020-02-15 15:24:56',7,1),(59,6,'a612b9dc3c96e2bf8d032eb4843ebb85119a34a41deb3910415c228d346c6b0f','2020-02-15 15:25:33','2020-02-15 15:27:33',8,1),(60,6,'eb3fd922b572c264e3a2146d3391b44e6e0dd3ac889a56a0367b8beeedfe31ae','2020-02-15 15:27:43','2020-02-15 17:24:12',1,1),(61,6,'aafec5b48edbfd0de617a9b9aa8133f72747cf8eeeb9d470cfb474e4b2b39b89','2020-02-15 17:24:17','2020-02-15 20:48:56',1,1),(62,2,'fd7fa9d1fca82b9d6976e34b26cc12e6cfd27348c18619304016a9c41be53350','2020-02-15 20:49:00','2020-02-15 22:22:50',1,1),(63,2,'8d03856343a5129764006c49f00268d2a11b4a80f0def3148731128ee88d2930','2020-02-15 22:23:00','2020-02-15 23:17:51',12,1),(64,2,'d608843156581edaba91ab265888db2d31f56dcdbd1c17cef87b291a5422d5a5','2020-02-15 23:17:56','2020-02-15 23:18:01',2,0),(65,2,'fae02a6d607a07ffe2d64b4d7b70f748c1b732cf73fd37a906b0bc89aa9a34ef','2020-02-16 17:24:48','2020-02-16 17:28:27',11,1),(66,2,'be4c04305fa9071b4990417f60785f00bccf9f3e9665bf4655b04849be380c7a','2020-02-16 17:29:30','2020-02-16 17:29:41',2,1),(67,6,'889b21751e1d092d25bc741e735c939cb8e354482562138f3056252f1bcab502','2020-02-16 17:29:48','2020-02-16 17:32:23',9,1),(68,2,'6c30ab4af05043c073a0063de7b42b0aab7d9fc2e499a3add1e53dca500dfbdc','2020-02-16 17:32:36','2020-02-16 17:32:50',2,1),(69,6,'e504384c1f84bad3db86445ac0cb65da417314009bc51ad2ee45fe0fd0a3c623','2020-02-16 17:33:03','2020-02-16 17:33:24',0,1),(70,6,'e9f2a7c67a4ea7b46312b33e7fdab5eac3256f058760f363b2b22d80cd5f765c','2020-02-16 17:33:35','2020-02-16 18:24:31',0,1),(71,2,'e235b50cf897b186e3e65ab9deb6ab37ed8202b189a24ba3645320e2280be670','2020-02-16 18:24:37','2020-02-16 19:30:03',0,1),(72,6,'6a998cab89ed58fe07b8f7aad6b857950970e4370f4e41bc344d712c82d48c83','2020-02-16 19:30:13','2020-02-16 22:03:34',4,1),(73,2,'ea2a7ecc9e1cdbef540da8f3ad8036a7ef46979b43e28d33b08b91692fff86f6','2020-02-16 22:03:41','2020-02-16 23:28:27',8,1),(74,2,'3a83725db75cec9ca991155e323e1a27eebecbd4e0eddefa4f1b8d36b3d97b9e','2020-02-16 22:05:52','2020-02-16 22:06:37',9,0),(75,2,'4f5e4d0165846804e3a2b5d7ba265b99b3134026fecd743b01fb4bb264416577','2020-02-16 23:28:35','2020-02-16 23:38:21',15,0),(76,2,'fbe3e011ae915a1c1002db463498326d3ea0306ec9d2b27abf646c27493c5623','2020-02-16 23:38:50','2020-02-17 00:01:11',12,1),(77,2,'1b3782d97836c3a3aa40c3256a0869015682a34315fa7827dfe5fe6426326d48','2020-02-17 00:01:20','2020-02-17 00:23:56',30,0),(78,2,'559ed4952dc409312a1dbcf3ef51073e1b29957e971f4a1974e5ca2aa31ef699','2020-02-17 00:12:14','2020-02-17 00:22:09',17,0),(79,2,'af48ae17c66f4edb48c56e436932c9c696223f548dcd7821fc67bb29027406fd','2020-02-17 12:38:53','2020-02-17 12:39:42',8,1),(80,2,'5ccfaf62b25d9c1e713be39c7ddd5c05baa86b09f866573bd4c4ae6bb36b8f12','2020-02-17 20:03:03','2020-02-17 23:11:23',1,1),(81,2,'5693a2a4d081b36bcb330c07200fb0b405c614e24b96916cf2295d6efd138182','2020-02-17 23:11:32','2020-02-17 23:12:22',6,0),(82,2,'aa812ce2dada9ce2ffffe5f69bfb7fcf7365d1aea49e4d31fd72f9184094324c','2020-02-22 20:23:20','2020-02-22 20:37:42',20,1),(83,2,'230e24aabde0027f3fc71772cf6cd38a6d81071cee67bdc4e0f18204b60b8e44','2020-02-22 20:38:29','2020-02-22 20:39:05',2,1),(84,2,'4d44c51b28ad686c27d3d475448166c676f80112c418931dbff0e314ccff2bb0','2020-02-22 21:33:00','2020-02-22 21:33:18',2,1),(85,6,'ea1d4ac4859196717163a6d3c1c8135d53bc7ae64519053092c1d39b05c8e3c5','2020-02-22 21:33:27','2020-02-22 21:38:02',14,1),(86,2,'1438cce9926029c332ccc6fbc17d867bcb16c66dfca49147bb8fbee4c6e58e5e','2020-02-22 22:30:49','2020-02-22 22:34:25',9,0),(87,2,'513058233cb28eb5ec628b93ce212faca377453cfec27847d113ee4a4e0b825b','2020-02-23 12:16:11','2020-02-23 16:15:58',23,1),(88,2,'cbe187ef3d3c53b921d6fd8f327e90535e9d55382f2b871f20f8cc8b0dbe9c90','2020-02-23 16:16:05','2020-02-23 16:21:45',9,1),(89,6,'a355cd8ebc51f128eaf46662b4d3ac28994d540f6707e44a3fb51f8fcdff7623','2020-02-23 16:21:58','2020-02-23 17:06:04',12,1),(90,2,'f0cfa563452c9611a458929f257c60e520fda3ef9eaeaa5141b9c228623d7ae1','2020-02-25 23:16:51','2020-02-25 23:20:05',9,0),(91,2,'6bc7eea13b4066c1c4f47c33a32e29de7b5f22721f6f5f55fdc3c368728904a6','2020-02-25 23:20:46','2020-02-25 23:20:50',2,0),(92,2,'11bd4161f5034bcd25d7d480b4b285c0d54ec418f03f7719fccb8f6debdf8798','2020-02-25 23:31:47','2020-02-25 23:33:52',16,0),(93,2,'ef0121f693accbb2b6f8c70109b51ed8ae38ba022263981f11b84836a8bf5b79','2020-02-27 23:28:48','2020-02-27 23:29:56',9,0),(94,2,'e521f18317ca2cd28ff4b52aa2ae670d4643337fed1ac12469a645330dbb299d','2020-02-27 23:30:48','2020-02-27 23:32:01',6,0),(95,2,'0d4105478f8c1cbf332ac83e1725fe2e3a1953fbb8dca5a0d0c00b3442c2cfcd','2020-02-28 20:30:01','2020-02-28 20:31:28',5,0),(96,2,'555232ef1b199faabf36ce7316e5e613499babb1441697e131bfcb03147815cb','2020-02-28 20:32:38','2020-02-28 20:33:34',5,0);
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `privileges` varchar(50) NOT NULL DEFAULT 'u0r0i0n0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES (1,'admin','u63r15i15n15a5'),(2,'user','u0r0i15n15a5'),(3,'guest','u0r0i0n1a65');
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT '3',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `fails_count` int(2) DEFAULT '0',
  `created_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `role_id_key` (`role_id`),
  CONSTRAINT `role_id_key` FOREIGN KEY (`role_id`) REFERENCES `user_roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','fff@ggg.com','fa9cf178697bc1429b2b96c1b2a900eb89c7cbb83e92063983d79d50e8299d81',1,1,0,'2020-02-05 23:46:08','2020-02-09 22:07:03'),(2,'fff','fff@ggg.com','200e11b2a5e8837a9a3fa478b8ce7d2be7e7deb90aab7f5cededf786981615ea',1,1,0,'2020-02-06 00:11:55','2020-02-09 22:05:53'),(3,'user','user@ggg.com','338b0c4bdf9984c4f8fd7c8da20cb1cbb12eb3aeccb4a8252d616d422b0fbbc1',2,1,0,'2020-02-06 00:17:15','2020-02-06 17:37:00'),(4,'vasia','vasia@mmm.com','61d84ee4d9bc3f018ab6508a07ac55acb344f2ec92fec0c6ce630df6858a50ac',2,1,0,'2020-02-09 15:02:09','2020-02-09 15:03:24'),(5,'grisha','grisha@ggg.com','61d84ee4d9bc3f018ab6508a07ac55acb344f2ec92fec0c6ce630df6858a50ac',2,1,0,'2020-02-09 22:08:43','2020-02-09 22:09:58'),(6,'someUser','someUser@gmail.com','61d84ee4d9bc3f018ab6508a07ac55acb344f2ec92fec0c6ce630df6858a50ac',2,1,0,'2020-02-15 15:25:33','2020-02-16 17:30:56');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-02-28 21:15:39
